/**
 * This program finds maximum likelihood estimates of
 * haplotype frequencies from a sample of genotyped individuals.
 * <ul>
 * Usage : <b> java GCHap input.par input.ped </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree fil. Only the
 * genotype data from this file are used, the individuals are
 * assumed to be unrelated. </li>
 *
 * <p>
 * The program uses the gene counting method, or EM algorithm,
 * to iterate between reconstructing the haplotypes and estimating
 * haplotype frequencies.
 *
 * <p>
 * The results are written to the standard output file.
 * There is a line for each haplotype with a positive frequency MLE,
 * the frequency is given first in the line followed by the alleles
 * of the haplotype.
 * These are followed by two lines for each individual giving the
 * reconstructed haplotypes, one haplotype on each line.
 */
public class GCHap extends alun.gchap.GCHap {
}
