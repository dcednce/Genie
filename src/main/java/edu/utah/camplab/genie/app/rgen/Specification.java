//******************************************************************************
// Specification.java
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

import edu.utah.camplab.genie.gm.*;
import edu.utah.camplab.genie.io.ResourceResolver;
import edu.utah.camplab.genie.io.XLoader;
import edu.utah.camplab.genie.io.XUt;
import edu.utah.camplab.genie.ped.PedData;
import edu.utah.camplab.genie.ped.Study;
import edu.utah.camplab.genie.sim.GSimulator;
import edu.utah.camplab.genie.sim.XDropSim;
import edu.utah.camplab.genie.sim.XTopSim;
import edu.utah.camplab.genie.stat.CCAnalysis;
import edu.utah.camplab.genie.stat.CCStat;
import edu.utah.camplab.genie.util.GEException;
import edu.utah.camplab.genie.util.Randy;
import edu.utah.camplab.genie.util.Ut;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

//import java.lang.Integer;

//==============================================================================

/**
 * Encapsulates the contents of an *.rgen specification file (an XML
 * application).  It creates the objects needed by {@link PedGenie},
 * according to the info found in the file.
 */
public class Specification {

  // change DTD_RESOLVER from private to public
  public static final EntityResolver DTD_RESOLVER = new ResourceResolver(
    "ge-rgen.dtd"
  );
  private static final String SPKG_GM = Ut.pkgOf(Repeat.class);
  private static final String SPKG_SIM = Ut.pkgOf(GSimulator.class);
  private static final String SPKG_STAT = Ut.pkgOf(CCStat.class);
  private static final String SPKG_IO = Ut.pkgOf(XLoader.class);
  private static final String OPT_DISABLER = ".";
  public static NumberFormat resultFormat = NumberFormat.getInstance();
  public static NumberFormat scientificFormat = NumberFormat.getInstance();
  private static String scientificNotation = "0.##E0";
  private static boolean intervalcheck = true;
  private static int maxFractionDigits = 2;
  public final int percent;
  private final String nsURI;
  private final String specName;
  private final Map<String, String> mParams = new TreeMap<String, String>();
  private final GSimulator.Top topSim;
  private final GSimulator.Drop dropSim;
  private final GDef gDef;
  private final AlleleFormat alleleformat;
  private final List<CCStat> lStats = new ArrayList<CCStat>();
  private final List<CCStat> lMetas = new ArrayList<CCStat>();
  private final List<String> lcovarIDs = new ArrayList<String>();
  private final List<String> ltabletype = new ArrayList<String>();
  private final PedData.Printer dataDumper;
  private final String app_id;
  public int[] covarIDs = null;
  public String[] tabletype = null;
  public int newSeed;
  private List<CCAnalysis> lAnalyses = new ArrayList<CCAnalysis>();
  private Study[] study;
  private Map<String, Object> hapC_params = new HashMap<String, Object>();
  //public static File   	    quantfile;
  private Boolean header = null;
  private String missingdata;
  private String distribution;
  private String distSuffix;
  private int nSims;
  private int nWeightedCycle = 0;
  private String decode;
  private boolean decodeflag = false;
  private String decodefile = null;
  private boolean printFreqflag = false;
  //----------------------------------------------------------------------------
  Specification(File f, String appid, Map<String, String> prior_globals)
    throws GEException {
    app_id = appid;
    specName = f.getPath();

    //Ryan added to setup default params for hapBuilder
    hapC_params.put("hapc_sigtesting", false);
    hapC_params.put("hapc_sigtesting_only", "false,0");
    hapC_params.put("hapc_loadnulls", false);
    hapC_params.put("hapc_backsets", true);
    //hapC_params.put("hapc_threshold", "0.1,0.05,0.01,0.005,0.001");
    hapC_params.put("hapc_threshold", "0.1,0.05,0.005,0.0005");
    hapC_params.put("hapc_models", "HAdd,HDom,HRec,MG");
    hapC_params.put("hapc_check_mostsignificant", "true");

    Element edoc = new XLoader().loadDocumentElement(f, DTD_RESOLVER);
    nsURI = edoc.getNamespaceURI();

    mParams.putAll(getGlobalAttributes(edoc));
    mParams.putAll(getOptionalParameters(kidsOf(edoc, Tag.PARAM)));
    mParams.putAll(getLocusParameters(kidsOf(edoc, Tag.LOCUS)));
    mParams.putAll(prior_globals);

    String nsimStr = mParams.get(Att.NSIMS);
    nSims = Integer.parseInt(nsimStr);
    //int simLen = nsimStr.length() + 1;
    //if ( simLen > maxFractionDigits )
    //  maxFractionDigits = simLen;
    // modified for random seed
    String rseed = mParams.get(Att.RSEED);
    if ("random".equals(rseed.toLowerCase())) {
      newSeed = new Random().nextInt(nSims * 100);
    }
    else {
      newSeed = Integer.parseInt(rseed);
    }
    Randy.create((long) newSeed);
    //Randy.lcreate((long) Integer.parseInt((String) mParams.get(Att.RSEED)));

    missingdata = mParams.get(Att.MISSINGDATA);
    alleleformat = new AlleleFormat(mParams.get(Att.ALLELEFORMAT));
    percent = new Integer(mParams.get(Att.PERCENT)).intValue();
    distribution = mParams.get(Att.DISTRIBUTION).toLowerCase();
    nWeightedCycle = nSims;

    if (distribution.equals("weightedindex")) {
      distSuffix = "Wt";
      nSims = 0;
      mParams.put(Att.TOP, "IndivWtTopSim");
      mParams.put(Att.DROP, "IndivWtDropSim");
    }

    Object top = null, drop = null, dumper = null,
      topAtt = null;
    int percent = 0;

    for (Iterator i = mParams.keySet().iterator(); i.hasNext(); ) {
      String name = ((String) i.next()).toLowerCase();
      //if (name.equals(Att.TOP) && !(app_id.toLowerCase().equals("hapmc")))
      if (name.equals(Att.TOP)) {
        top = Ut.newModule(SPKG_SIM, mParams.get(name));
      }
      //else if (name.equals(Att.DROP) && !(app_id.toLowerCase().equals("hapmc")))
      else if (name.equals(Att.DROP)) {
        drop = Ut.newModule(SPKG_SIM, mParams.get(name));
      }
      // attach ResultImp to each statistic and metastat
      else if (name.startsWith(PName.CCSTAT)) {
        lStats.add((CCStat) Ut.newModule(SPKG_STAT, mParams.get(name), distSuffix));
      }
      else if (name.startsWith(PName.METASTAT)) {
        lMetas.add((CCStat) Ut.newModule(SPKG_STAT, mParams.get(name), distSuffix));
      }
      else if (name.equals(PName.DUMPER)) {
        dumper = Ut.newModule(SPKG_IO, mParams.get(name));
      }
      //Ryan added 08-16-07 to extract parameters for HapBuilder
      //1. Boolean value to do complete significant testing - default is no (0).
      //2. Boolean value to perform backsets - default is yes (1).
      //3. Threshold list - default is 0.1
      else if (name.startsWith(PName.HAPC)) {
        if (name.equals("hapc_threshold")) {
          hapC_params.put(name, mParams.get(name));
        }
        else {
          hapC_params.put(name, mParams.get(name));
        }
      }
      else if (name.startsWith(PName.COVAR)) {
        lcovarIDs.add(mParams.get(name));
      }
      else if (name.startsWith(PName.TABLETYPE)) {
        ltabletype.add(mParams.get(name));
      }
      else if (name.equals(Att.DECODE)) {
        String decodeValue = mParams.get(name).toLowerCase();
        if (decodeValue.startsWith("y") || decodeValue.startsWith("true")) {
          decodeflag = true;
        }
      }
      else if (name.equals(Att.DECODEFILE)) {
        decodefile = mParams.get(name);
      }
      else if (name.equals(Att.PRINTFREQ)) {
        String printValue = mParams.get(name).toLowerCase();
        if (printValue.startsWith("y") || printValue.startsWith("true")) {
          printFreqflag = true;
        }
        //} else if ( name.equals(Att.SCIENTIFICNOTATION)) {
        //String inscientificNotation = mParams.get(name).toLowerCase();
        //if (inscientificNotation.startsWith("y")
        // scientificNotation = inscientificNotation;
      }
      else if (name.equals(Att.MAXFRACTIONDIGITS)) {
        maxFractionDigits = new Integer(mParams.get(Att.MAXFRACTIONDIGITS)).intValue();
      }
      else if (name.equals(Att.INTERVALCHECK)) {
        String inIntervalCheck = mParams.get(name).toLowerCase();
        if (inIntervalCheck.startsWith("n") ||
          inIntervalCheck.startsWith("false")) {
          intervalcheck = false;
        }
      }
    }
    ((DecimalFormat) scientificFormat).applyPattern(scientificNotation);
    resultFormat.setMaximumFractionDigits(maxFractionDigits);

    // use maxFractionDigits to ResultImp to all ccstat
    //for ( CCStat ccstat : lStats )
    //  ccstat.setMaxFractionDigits(maxFractionDigits);
    //for ( CCStat metastat : lMetas )
    //  metastat.setMaxFractionDigits(maxFractionDigits);

    topSim = (GSimulator.Top) top;
    dropSim = (GSimulator.Drop) drop;
    //alleleformat = (AlleleFormat) alformat;
    dataDumper = (PedData.Printer) dumper;

    String[] covarids = lcovarIDs.toArray(new String[0]);
    covarIDs = new int[covarids.length];
    for (int i = 0; i < covarids.length; i++) {
      covarIDs[i] = Integer.parseInt(covarids[i]);
    }

    if ((topSim instanceof XTopSim) && (dropSim instanceof XDropSim)) {
      if (!ltabletype.contains("x-chr")) {
        ltabletype.add("x-chr");
      }
    }
    if (ltabletype.size() > 0) {
      tabletype = ltabletype.toArray(new String[0]);
    }
    else {
      tabletype = new String[1];
      tabletype[0] = new String("original");
    }
    //covarIDs = getCovarParameters(kidsOf(edoc, Tag.COVAR));
    NodeList datafilelist = kidsOf(edoc, Tag.DATAFILE);
    buildDataFile(datafilelist);
    gDef = buildGDef(kidsOf(edoc, Tag.LOCUS));
    gDef.setAlleleFormat(alleleformat);
    gDef.setDecoderFlag(decodeflag);
    alleleformat.setMissingData(missingdata);
    buildAnalysisDefs(kidsOf(edoc, Tag.CCTABLE));
  }

  //----------------------------------------------------------------------------
  public static int getMaxFractionDigits() {
    return maxFractionDigits;
  }

  //----------------------------------------------------------------------------
  public static NumberFormat getScientificFormat() {
    return scientificFormat;
  }

  //----------------------------------------------------------------------------
  public static boolean getIntervalCheck() {
    return intervalcheck;
  }

  //----------------------------------------------------------------------------
  public static NumberFormat getResultFormat() {
    return resultFormat;
  }

  //----------------------------------------------------------------------------
  String getName() {
    return specName;
  }

  //----------------------------------------------------------------------------
  GSimulator.Top getTopSim() {
    return topSim;
  }

  //----------------------------------------------------------------------------
  GSimulator.Drop getDropSim() {
    return dropSim;
  }

  //----------------------------------------------------------------------------
  public GDef getGDef() {
    return gDef;
  }

  //----------------------------------------------------------------------------
  CCAnalysis[] getCCAnalyses() {
    return lAnalyses.toArray(new CCAnalysis[0]);
  }

  //----------------------------------------------------------------------------
  PedData.Printer getDataDumper() {
    return dataDumper;
  }

  //----------------------------------------------------------------------------
  public int getNumberOfSimulations() {
    //return Integer.parseInt(mParams.get(Att.NSIMS));
    return nSims;
  }

  //----------------------------------------------------------------------------
  public Map getAllGlobalParameters() {
    return Collections.unmodifiableMap(mParams);
  }

  //----------------------------------------------------------------------------
  //Ryan 06-12-07 changed to public for analysisSet to access this method.
  public CCStat[] getCCStats() {
    return lStats.toArray(new CCStat[0]);
  }

  //----------------------------------------------------------------------------
  //Ryan 1-04-08 changed to public for analysisSet to access this method.
  public String[] gethapB_models() {
    String[] models = ((String) hapC_params.get("hapc_models")).split(",");
    return models;
  }

  //----------------------------------------------------------------------------
  public CCStat[] getMetaStats() {
    return lMetas.toArray(new CCStat[0]);
  }

  //----------------------------------------------------------------------------
  public int getRseed() {
    return newSeed;
  }

  //----------------------------------------------------------------------------
  // change the following methods from private to public
  public Map<String, String> getGlobalAttributes(Element doc_elem) {
    Map<String, String> m = new HashMap<String, String>();
    NamedNodeMap atts = doc_elem.getAttributes();

    for (int i = 0; i < atts.getLength(); ++i) {
      m.put(atts.item(i).getNodeName(), atts.item(i).getNodeValue());
    }
    m.remove("xmlns" + ":" + doc_elem.getPrefix());

    return m;
  }

  //----------------------------------------------------------------------------
  public Map<String, String> getOptionalParameters(NodeList nlparams) {
    Map<String, String> m = new HashMap<String, String>();
    for (int i = 0; i < nlparams.getLength(); ++i) {
      Element e = (Element) nlparams.item(i);
      String value = e.getFirstChild().getNodeValue().trim();
      if (!value.startsWith(OPT_DISABLER)) {
        m.put(e.getAttribute(Att.NAME), value);
      }
    }
    return m;
  }

  //----------------------------------------------------------------------------
  public int[] getCovarParameters(NodeList covarparams) {
    int[] covars = new int[covarparams.getLength()];
    for (int i = 0; i < covarparams.getLength(); i++) {
      Element e = (Element) covarparams.item(i);
      covars[i] = Integer.parseInt(e.getAttribute(Att.ID));
    }
    return covars;
  }

  //----------------------------------------------------------------------------
  public Map<String, String> getLocusParameters(NodeList nlparams) {
    Map<String, String> m = new HashMap<String, String>();
    for (int i = 0; i < nlparams.getLength(); ++i) {
      Element e = (Element) nlparams.item(i);
      if (e.getAttribute(Att.MARKER).length() == 0) {
        m.put("locus " + (i + 1), e.getAttribute(Att.ID));
      }
      else {
        m.put("locus " + (i + 1), e.getAttribute(Att.MARKER));
      }
    }
    return m;
  }

  //----------------------------------------------------------------------------
  public void buildDataFile(NodeList ndatafile) throws GEException {
    ArrayList<Study> studyList = new ArrayList<Study>();
    //study = new Study[ndatafile.getLength()];
    for (int i = 0; i < ndatafile.getLength(); i++) {
      String stdname, genodata, hap, quant, par = null;
      Element edatafile = (Element) ndatafile.item(i);
      stdname = XUt.stringAtt(edatafile, Att.STUDYNAME);
      header = new Boolean(XUt.stringAtt(edatafile, Att.HEADER));
      genodata = XUt.stringAtt(edatafile, Att.GENOTYPEDATA);
      hap = XUt.stringAtt(edatafile, Att.HAPLOTYPE);
      quant = XUt.stringAtt(edatafile, Att.QUANTITATIVE);
      par = XUt.stringAtt(edatafile, Att.LINKAGEPARAMETER);
      //decodef     = XUt.stringAtt(edatafile, Att.DECODEFILE);
      if (genodata != null) {
        studyList.add(new Study(stdname, genodata, quant, hap, par));
      }
      //if ( decodef != null ) {
      //decodefile = decodef;
      //decodeflag = true;
      //}
    }
    if (studyList.size() == 0) {
      new GEException("Genotype data file is missing in .rgen parameter file");
    }
    study = (Study[]) studyList.toArray(new Study[0]);
  }

  //----------------------------------------------------------------------------
  private GDef buildGDef(NodeList nlloci) throws GEException {
    GDefBuilder gdb = new GDefBuilder();
    for (int i = 0; i < nlloci.getLength(); ++i) {
      Element elocus = (Element) nlloci.item(i);
      //  XUt.intAtt(elocus, Att.ID), XUt.doubleAtt(elocus, Att.DIST), true
      gdb.addLocus(
        XUt.intAtt(elocus, Att.ID), XUt.stringAtt(elocus, Att.MARKER),
        XUt.doubleAtt(elocus, Att.DIST), true,
        XUt.intAtt(elocus, Att.GENE)
      );
    }

    return gdb.build();
  }

  //----------------------------------------------------------------------------
  private void buildAnalysisDefs(NodeList nltables) throws GEException {
    CCStat[] stats = getCCStats();
    CCStat[] metas = getMetaStats();
    int nstats = stats.length;
    int nmetas = metas.length;
    GDef gdef = getGDef();
    int nloci = gdef.getLocusCount();
    int ntabletype = (getTableType()).length;

    for (int itable = 0; itable < nltables.getLength(); ++itable) {
      Element etable = (Element) nltables.item(itable);
      int[] itabletype = parseRefs(etable, Att.TABLES, ntabletype);
      int[] iloci = parseRefs(etable, Att.LOCI, nloci);
      int[] istats = parseRefs(etable, Att.STATS, nstats);
      int[] imetas = parseRefs(etable, Att.METAS, nmetas);
      String imodel = etable.getAttribute(Att.MODEL);
      String itype = etable.getAttribute(Att.TYPE).toLowerCase();
      NodeList nlcols = kidsOf(etable, Tag.COL);
      int ncols = nlcols.getLength();
      CCAnalysis.Table.Column[] cdefs = new CCAnalysis.Table.Column[ncols];
      //List<String>        	lmarker = new ArrayList<String>();
      String irepeat = etable.getAttribute(Att.REPEAT);
      int[][] repeatGroup;
      int nrepeat;
      int nlociGroup;

      if (itype.length() == 0) {
        itype = "genotype";
      }

      //check # loci in the table
      Element colAtt = (Element) nlcols.item(0);
      NodeList gAtt = kidsOf(colAtt, Tag.G);
      NodeList aAtt = kidsOf((Element) gAtt.item(0), Tag.A);
      int groupSize = aAtt.getLength();
      Object repeat = Ut.newModule(SPKG_GM, irepeat);
      //RepeatLoci rp = new RepeatLoci();
      Repeat rp = (Repeat) repeat;
      repeatGroup = rp.getGroup(iloci, groupSize);
      nrepeat = repeatGroup.length;
      //iloci = repeatGroup[0];
      nlociGroup = repeatGroup[0].length;

      //System.out.println("Building CCAnalysis");
      //for ( int re = 0; re < nrepeat; re++ ) {
      //System.out.println("    for repeat group " + re );
      //for ( int rr = 0 ; rr < repeatGroup[re].length; rr++)
      //System.out.println("has loci " + repeatGroup[re][rr]);
      //}

      for (int icol = 0; icol < ncols; ++icol) {
        //System.out.println(" for Column " + icol );
        Element ecol = (Element) nlcols.item(icol);
        int wt = XUt.intAtt(ecol, Att.WT);
        NodeList nlg = kidsOf(ecol, Tag.G);
        GtypeMatcher[][] repeatgtm = new GtypeMatcher[nrepeat][];

        for (int r = 0; r < nrepeat; r++) {
          List<GtypeMatcher> lgtmatchers =
            new ArrayList<GtypeMatcher>();
          // add new dimension for repeat
          //System.out.println(" for repeat " + r );
          for (int ig = 0, ng = nlg.getLength(); ig < ng; ++ig) {
            NodeList nla = kidsOf((Element) nlg.item(ig), Tag.A);
            // added gdef to constructor
            //GtypeMatcher gtm[] = new GtypeMatcher(iloci, gdef);
            //System.out.println(" for each re:g has " + repeatGroup[r].length + " loci");
            GtypeMatcher gtm = new GtypeMatcher(repeatGroup[r], gdef);

            //if (nla.getLength() != iloci.length)
            if (nla.getLength() != nlociGroup) {
              throw new GEException(
                elCountMsg(
                  new String[]{Tag.CCTABLE, Tag.COL, Tag.G, Tag.A},
                  new int[]{itable, icol, ig, nla.getLength()},
                  nlociGroup
                  //iloci.length
                )
              );
            }

            //for (int ia = 0; ia < iloci.length; ++ia)
            for (int ia = 0; ia < nlociGroup; ++ia) {
              gtm.setRegex(ia,
                nla.item(ia).getFirstChild().getNodeValue().trim());
            }

            lgtmatchers.add(gtm);
          }
          repeatgtm[r] = (GtypeMatcher[])
            lgtmatchers.toArray(new GtypeMatcher[0]);
          //cdefs[icol] = new ColumnImp(lgtmatchers, wt, itype);
        }
        cdefs[icol] = new ColumnImp(repeatgtm, wt);
      }
      List<CCStat> newStats = new ArrayList<CCStat>();
      for (int iistat = 0; iistat < istats.length; ++iistat) {
        newStats.add(stats[istats[iistat]]);
      }
      CCStat[] selected_stats = newStats.toArray(new CCStat[0]);

      List<CCStat> newMetas = new ArrayList<CCStat>();
      for (int iimeta = 0; iimeta < imetas.length; ++iimeta) {
        newMetas.add(metas[imetas[iimeta]]);
      }
      CCStat[] selected_metas = newMetas.toArray(new CCStat[0]);

      List<String> newTabletype = new ArrayList<String>();
      for (int iitabletype = 0; iitabletype < itabletype.length; iitabletype++) {
        newTabletype.add(tabletype[itabletype[iitabletype]]);
      }
      String[] selected_tabletype = newTabletype.toArray(new String[0]);

      //lAnalyses.add(new CCAnalysis(itable, cdefs, markers, imodel,
      //lAnalyses.add(new CCAnalysis(cdefs, markers, imodel, selected_stats,
      //11-28-07 removed markers, no fix markername for repeat process
      lAnalyses.add(new CCAnalysis(cdefs, imodel, selected_stats,
        selected_metas, itype, covarIDs,
        percent, repeatGroup, selected_tabletype,
        app_id));
    }
  }

  //----------------------------------------------------------------------------
  public void updateAnalyses(List<CCAnalysis> newAnalyses) {
    lAnalyses.clear();
    for (int i = 0; i < newAnalyses.size(); i++) {
      lAnalyses.add(newAnalyses.get(i));
    }
  }

  //----------------------------------------------------------------------------
  private int[] parseRefs(Element e, String attr_name, int nobjs)
    throws GEException {
    int[] ordinals = XUt.intsAtt(e, attr_name);
    if (ordinals != null) // if attribute specifying object subset present
    {
      return Ut.mapAdd(ordinals, -1); // change indexing base from 1 to 0
    }
    return Ut.identityArray(nobjs);   // otherwise reference all objects
  }

  //----------------------------------------------------------------------------
  private String elCountMsg(String[] names, int[] indices, int expect) {
    List<String> l = new ArrayList<String>();
    for (int i = 0; i < names.length; ++i) {
      l.add(names[i] + "[" + (indices[i] + 1));
    }
    return expect + " elements expected ( " + Ut.join(l, " > ") + " )";
  }

  //----------------------------------------------------------------------------
  private NodeList kidsOf(Element e, String kid_elements_name) {
    return XUt.kidsOf(e, kid_elements_name, nsURI);
  }

  //----------------------------------------------------------------------------
  //public File getQuantfile ()
  //{
  //   return quantfile;
  //}
  //----------------------------------------------------------------------------
  public String getDecodeFile() {
    return decodefile;
  }

  //----------------------------------------------------------------------------
  public boolean getDecodeFlag() {
    return decodeflag;
  }

  //----------------------------------------------------------------------------
  //moved this method to util.Ut
  //private static Object newModule(String sdefpkg, String classname)
  //public static Object newModule(String sdefpkg, String classname)
  //throws GEException
  // {
  //  try {
  //    return Class.forName(
  //      classname.indexOf('.') == -1 ? (sdefpkg + "." + classname) : classname
  //    ).newInstance();
  //  } catch (Exception e) {
  //    throw new GEException("Can't get class instance: ", e);
  //  }
  //}

  //----------------------------------------------------------------------------
  public boolean getPrintFreqFlag() {
    return printFreqflag;
  }

  //----------------------------------------------------------------------------
  public Study[] getStudy() {
    if (study.length > 0) {
      return study;
    }
    else {
      return null;
    }
  }

  //----------------------------------------------------------------------------
  public int[] getCovarIds() {
    return covarIDs;
  }

  //----------------------------------------------------------------------------
  public String[] getTableType() {
    return tabletype;
  }

  //----------------------------------------------------------------------------
  public boolean hasHeader() {
    return header;
  }

  //----------------------------------------------------------------------------
  public String getDistribution() {
    return distribution;
  }

  //----------------------------------------------------------------------------
  public int getWeightedCycle() {
    return nWeightedCycle;
  }

  //----------------------------------------------------------------------------
  //Ryan 06-12-07 created to create a new table column in analysisSet class
  //public ColumnImp createNewColumn(List<GtypeMatcher> lgtmatchers, int wt, String itype){
  //return new ColumnImp(lgtmatchers, wt, itype);
  public ColumnImp createNewColumn(List<GtypeMatcher> lgtmatchers, int wt) {
    GtypeMatcher[][] gtm = new GtypeMatcher[1][];
    gtm[0] = (GtypeMatcher[]) lgtmatchers.toArray(new GtypeMatcher[0]);
    //return new ColumnImp(lgtmatchers, wt);
    //System.out.println("createNewColumn is called");
    return new ColumnImp(gtm, wt);
  }

  //----------------------------------------------------------------------------
  //Ryan 06-12-07 created to obtain percent variable for analysisSet class
  public int getPercent() {
    return percent;
  }

  //----------------------------------------------------------------------------
  //Ryan 08-16-07, added gethapB functions to allow user specified parameters
  //for hapBuilder package. The parameters are:
  //1. significance testing - establish the significance of the loci sets from hapBuilder
  //2. backset testing - go foward and backward or just forward
  //3. threshold - the different thresholds for the different steps.
  public boolean gethapC_sigtesting() {
    if (hapC_params.get("hapc_sigtesting").equals("true")) {
      return true;
    }
    else {
      return false;
    }
  }

  //----------------------------------------------------------------------------
  public boolean gethapC_backsets() {
    if (hapC_params.get("hapc_backsets").equals("true")) {
      return true;
    }
    else {
      return false;
    }
  }

  //----------------------------------------------------------------------------
  public double[] gethapC_threshold() {
    String[] thold = ((String) hapC_params.get("hapc_threshold")).split(",");
    double[] m = {};
    for (int i = 0; i < thold.length; i++) {
      m[i] = Double.parseDouble(thold[i]);
    }
    return m;
  }

  //----------------------------------------------------------------------------
  // added 2-25-08
  public double[] gethapC_screenthreshold() {
    String[] thold = ((String) hapC_params.get("hapc_screenthreshold")).split(",");
    double[] m = {};
    for (int i = 0; i < thold.length; i++) {
      m[i] = Double.parseDouble(thold[i]);
    }
    return m;
  }

  //----------------------------------------------------------------------------
  public String[] gethapC_models() {
    String[] models = ((String) hapC_params.get("hapc_models")).split(",");
    return models;
  }

  //----------------------------------------------------------------------------
  public int gethapC_screen() {
    if (hapC_params.containsKey("hapc_screen")) {
      if (hapC_params.get("hapc_screen").equals("true")) {
        return 1;
      }
      else {
        return -1;
      }
    }
    else {
      return 0;
    }
  }

  //----------------------------------------------------------------------------
  public boolean gethapC_sigtesting_only() {
    if (((String) hapC_params.get("hapc_sigtesting_only")).split(",")[0].equals("true")) {
      return true;
    }
    else {
      return false;
    }
  }

  //----------------------------------------------------------------------------
  public int gethapC_sigtesting_start() {
    int sigrun = 0;
    sigrun = (int) Integer.parseInt(((String) hapC_params.get("hapc_sigtesting_only")).split(",")[1]);
    return sigrun;
  }

  //----------------------------------------------------------------------------
  public boolean gethapC_loadnulls() {
    if (hapC_params.get("hapc_loadnulls").equals("true")) {
      return true;
    }
    else {
      return false;
    }
  }

  //----------------------------------------------------------------------------
  public boolean gethapC_check_mostsignificant() {
    if (hapC_params.get("hapc_check_mostsignificant").equals("true")) {
      return true;
    }
    else {
      return false;
    }
  }

  //----------------------------------------------------------------------------
  public AlleleFormat getAlleleFormat() {
    return alleleformat;
  }

  public interface Tag {
    String DATAFILE = "datafile";
    String LOCUS = "locus";
    String PARAM = "param";
    String CCTABLE = "cctable";
    String COL = "col";
    String G = "g";
    String A = "a";
  }

  public interface Att {
    String NS = "xmlns:ge";
    String NSIMS = "nsims";
    String RSEED = "rseed";
    String TOP = "top";
    String DROP = "drop";
    String REPORT = "report";
    String PERCENT = "percent";
    String NAME = "name";
    String ID = "id";
    String DIST = "dist";
    String MARKER = "marker";
    String ORDERED = "ordered";
    String GENE = "gene";
    String LOCI = "loci";
    String REPEAT = "repeat";
    String STATS = "stats";
    String METAS = "metas";
    String MODEL = "model";
    String TABLES = "tables";
    String TYPE = "type";
    String WT = "wt";
    String STUDYNAME = "studyname";
    String HEADER = "header";
    String GENOTYPEDATA = "genotypedata";
    String QUANTITATIVE = "quantitative";
    String HAPLOTYPE = "haplotype";
    String LINKAGEPARAMETER = "linkageparameter";
    String ALLELEFORMAT = "alleleformat";
    String MISSINGDATA = "missingdata";
    String DISTRIBUTION = "distribution";
    String DECODEFILE = "decodefile";
    String DECODE = "decode";
    String PRINTFREQ = "printfreq";
    //String SCIENTIFICNOTATION = "scientificnotation";
    String MAXFRACTIONDIGITS = "maxfractiondigits";
    String INTERVALCHECK = "intervalcheck";
  }

  public interface PName {
    String CCSTAT = "ccstat";
    String DUMPER = "dumper";
    String METASTAT = "metastat";
    //String QUANTFILE = "quantfile";
    String COVAR = "covar";
    String TABLETYPE = "tabletype";
    String HAPC = "hapc";
  }

  private static class ColumnImp implements CCAnalysis.Table.Column {
    private final GtypeMatcher[][] gtMatchers;
    private final int theWt;

    //private final String 	   theType;
    //private ColumnImp( List<GtypeMatcher> gt_matchers, int weight)
    private ColumnImp(GtypeMatcher[][] gt_matchers, int weight) {
      gtMatchers = gt_matchers;
      theWt = weight;
    }

    // pass each allele separately for type = Allele
    public int subsumesAtype(Gtype gt, boolean first, int repeatIndex) {
      GtypeMatcher[] gtm = gtMatchers[repeatIndex];
      for (int j = 0; j < gtm.length; j++) {
        if (gtm[j].matchesGtype(gt, first)) {
          return 1;
        }
      }
      return 0;
    }

    public int subsumesGtype(Gtype gt, int repeatIndex) {
      GtypeMatcher[] gtm = gtMatchers[repeatIndex];
      for (int j = 0; j < gtm.length; ++j) {
        if (gtm[j].matchesGtype(gt)) {
          return 1;
        }
      }
      return 0;
    }

    public int getWeight() {
      return theWt;
    }

    public GtypeMatcher[][] getGtypeMatcher() {
      return gtMatchers;
    }
    //public String getType() { return theType; }
  }
}

