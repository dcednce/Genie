//******************************************************************************
// Main.java
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

//==============================================================================

/**
 * Contains the main() method that runs a {@link PedGenie} object from the
 * console.
 */
public class Main {

  public static void main(String[] args) {
    // allow GUI 
    //if ( args.length == 0 ) {
    //  GenieGUI.main(args);
    //} else {
    //  MainManager mm = new MainManager();
    //  mm.executeGenie(args);
    //}
    MainManager mm = new MainManager(args);

    try {
      Runtime.getRuntime().addShutdownHook(new ShutdownThread(mm));
    } catch (Throwable t) {
      System.out.println("Failed to add ShutdownHook");
    }
  }
}

