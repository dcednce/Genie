//******************************************************************************
// IOManager.java
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

import alun.genio.LinkageFormatter;
import alun.genio.LinkageParameterData;
import edu.utah.camplab.genie.gm.AlleleFormat;
import edu.utah.camplab.genie.gm.FreqDataSet;
import edu.utah.camplab.genie.gm.GDef;
import edu.utah.camplab.genie.gm.GtypeMatcher;
import edu.utah.camplab.genie.io.HapDataLoader;
import edu.utah.camplab.genie.io.LpedParser;
import edu.utah.camplab.genie.ped.*;
import edu.utah.camplab.genie.prep.AlleleDecoder;
import edu.utah.camplab.genie.sim.GSimulator;
import edu.utah.camplab.genie.stat.CCAnalysis;
import edu.utah.camplab.genie.util.GEException;
import edu.utah.camplab.genie.util.Ut;

import java.io.*;
import java.util.Calendar;
import java.util.Map;

//import edu.utah.camplab.genie.prep.PhaseData;

//==============================================================================
public class IOManager {

  private static File specfile;
  private static GDef gdef;
  private static AlleleFormat alleleformat;
  private static LinkageParameterData lpd;
  private static Study[] study;
  private static int numGDef;
  private final Specification theSpec;
  private final String app_id, app_ver, outpathStem;
  private final PedData.Printer dataDumper;
  private int[] covar_ids;
  private int numSimData = 1;
  private Calendar indate;
  private boolean hasHeader;
  private boolean hasError;
  private boolean reportCompleted = false;

  //----------------------------------------------------------------------------
  IOManager(String appid,
            String appver,
            File fspec,
            Map<String, String> prior_spec_params,
            Calendar inDate)
    throws GEException {
    app_id = appid;
    app_ver = appver;
    specfile = fspec;
    theSpec = loadSpec(specfile, prior_spec_params);
    study = theSpec.getStudy();
    gdef = theSpec.getGDef();
    covar_ids = theSpec.getCovarIds();
    GSimulator.Top tSim = theSpec.getTopSim();
    hasHeader = theSpec.hasHeader();
    alleleformat = gdef.getAlleleFormat();
    //outpathStem = Ut.stemOf(specfile);
    outpathStem = Ut.fullstemOf(specfile);
    dataDumper = theSpec.getDataDumper();
    numGDef = gdef.getLocusCount();
    indate = inDate;

    //Ryan 08-19-07 commented out for compressGtype.
//    if ( app_id.equals("hapBuilder") )
//      numSimData = theSpec.getNumberOfSimulations();

    for (int i = 0; i < study.length; i++) {
      File genofile = study[i].getGenotypeFile();
      File quantfile = study[i].getQuantitativeFile();
      File hapfile = study[i].getHaplotypeFile();
      File parfile = study[i].getLinkageParFile();

      study[i].setPedData(loadPedData(genofile, quantfile));
      //Indiv[] indiv = study[i].getPedData().getIndividuals(PedQuery.IS_ANY);

      if (parfile != null) {
        study[i].setLinkageParameterData(loadLinkageParameterData(parfile));
      }

      //load haplotype or genotype frequency files
      if (hapfile != null) {
        HapDataLoader haploader = new HapDataLoader();
        try {
          haploader.parse(hapfile, alleleformat);
        } catch (IOException e) {
          throw new GEException(e.getMessage());
        }
        FreqDataSet[] freqdata = haploader.getHapFreq();
        study[i].setFreqDataSet(freqdata);
      }
    }
    if (hasError) {
      throw new GEException("Please check genotype data");
    }

    // add decoderMap to locus
    if (theSpec.getDecodeFlag()) {
      String decodefreqfile = theSpec.getDecodeFile();
      System.out.println("Decoder file is " + decodefreqfile);
      if (decodefreqfile != null) {
        File dFile = new File(decodefreqfile);
        try {
          AlleleDecoder.parse(gdef, dFile);
        } catch (Exception e) {
          e.printStackTrace();
          throw new GEException("failed to parse decode freq file " + decodefreqfile);
        }
      }
      else {
        try {
          AlleleDecoder.build(gdef, study);
        } catch (Exception e) {
          e.printStackTrace();
          throw new GEException("Failed to build decoder map from study genotype data.");
        }
      }
      // update all gtypematcher with decoder map
      CCAnalysis[] analyses = theSpec.getCCAnalyses();
      int a = 0;
      //System.out.println(" === update analysis's column gtypematcher");
      for (CCAnalysis ccA : analyses) {
        //System.out.println("Analysis " + a );
        CCAnalysis.Table.Column[] col = ccA.getColDefs();
        for (int c = 0; c < col.length; c++) {
          //System.out.println("For Column " + c );
          GtypeMatcher[][] gtm = col[c].getGtypeMatcher();
          for (int i = 0; i < gtm.length; i++) {
            //System.out.println("Gtypematcher for repeat " + i );
            for (int j = 0; j < gtm[i].length; j++) {
              //System.out.println("GtypeMatche # " + j );
              gtm[i][j].changeRegex();
            }
          }
        }
        a++;
      }
    }
  }

  //----------------------------------------------------------------------------
  Specification getSpecification() {
    return theSpec;
  }

  //----------------------------------------------------------------------------
  Study[] getStudy() // Why we need this method?
  {
    return study;
  }

  //----------------------------------------------------------------------------
  boolean dumpingEnabled() {
    return dataDumper != null;
  }

  //----------------------------------------------------------------------------
  void dumpData(int idump, int index) throws GEException, IOException {
    if (!dumpingEnabled()) {
      return;
    }
    int nsims = theSpec.getNumberOfSimulations();
    if (Ut.uqNameOf(dataDumper).equals("IndivDumper") && idump < nsims) {
      return;
    }
    String[] dumppathStem = new String[study.length];

    for (int i = 0; i < study.length; i++) {
      dumppathStem[i] = Ut.stemOf(study[i].getGenotypeFile()) + '.' + Ut.stemOf(specfile);
      File f = Ut.fExtended(
        dumppathStem[i] + "_" + idump, dataDumper.getType());
      PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(f)));

      System.out.println("Dumping to '" + f + "'...");

      dataDumper.print(study[i].getPedData(),
        idump == 0 ? Indiv.GtSelector.OBS : Indiv.GtSelector.SIM,
        alleleformat, numGDef, pw, index, hasHeader);
      pw.close();
    }
  }

  //----------------------------------------------------------------------------
  void dumpData(int idump) throws IOException, GEException {
    if (!dumpingEnabled()) {
      return;
    }
    //if ( Ut.uqNameOf(dataDumper).equals("IndivDumper") )
    //  return;

    String[] dumppathStem = new String[study.length];

    for (int i = 0; i < study.length; i++) {
      dumppathStem[i] = Ut.stemOf(study[i].getGenotypeFile()) + '.' + Ut.stemOf(specfile);
      File f = Ut.fExtended(
        dumppathStem[i] + "_" + idump, dataDumper.getType());
      PrintWriter pw = new PrintWriter(new BufferedWriter(
        new FileWriter(f)), true);

      System.out.println("Dumping to '" + f + "'...");

      dataDumper.print(study[i].getPedData(),
        idump == 0 ? Indiv.GtSelector.OBS : Indiv.GtSelector.SIM,
        alleleformat, numGDef, pw);
      pw.close();
    }
  }

  //----------------------------------------------------------------------------
  void writeReport(int cycleIndex) throws IOException {
    CCAnalysis[] analyses = theSpec.getCCAnalyses();
    /**
     String[] filesource   = new String[study.length];
     String[] studyname    = new String[study.length];

     for ( int i = 0; i < study.length; i++ )
     {
     filesource[i] = study[i].getPedData().getID();
     studyname[i]  = study[i].getStudyName();
     }

     Map m = theSpec.getAllGlobalParameters();

     if ( "full".equals(m.get("report")) )
     {
     String[] ext = new String[2];
     ext[0]       = "report";
     ext[1]       = "summary";
     for ( int i = 0; i < ext.length; kkkkkkkkkkk
     File f = Ut.fExtended (outpathStem, "report");
     Reporter r   
     reportext[0] = "report";
     reportext[1] = "summary";
     }
     else if ( "summary".equals(m.get("report");

     File f = Ut.fExtended(outpathStem, reportext[i])
     Reporter r = new Reporter();
     r.reportHeaderInfo(appID, theSpec, filesource,
     studyname, indate);

     for (int i = 0; i < analyses.length; ++i)
     r.reportCCAnalysis(analyses[i]);

     */
    Reporter r = new Reporter(app_id, app_ver, theSpec, study, indate,
      analyses, outpathStem, cycleIndex);
    reportCompleted = true;
    //r.closeReport();

    //System.out.println("Report written to '" + f + "'." + Ut.N);
  }

  //----------------------------------------------------------------------------
  private Specification loadSpec(File f, Map<String, String> prior_params)
    throws GEException {
    System.out.println("Loading specification from '" + f + "'...");

    return new Specification(f, app_id, prior_params);
  }

  //----------------------------------------------------------------------------
  private PedData loadPedData(File pedf, File quantf) throws GEException {
    System.out.println("Loading data from '" + pedf + "'...");

    PedDocument doc = new PedDocument(gdef);
    QuantIndiv[] quantIndiv = null;
    if (quantf != null) {
      quantIndiv = loadQuantData(quantf);
    }

    //System.out.println(hasHeader ? "true" : "false");
    System.out.println("Loading genotype file " + pedf.getName());
    doc.read(pedf, quantIndiv, covar_ids,
      numSimData, new LpedParser(), hasHeader);
    System.out.println("Loaded " + doc.getPedigrees().length + " pedigrees.");
    if (doc.getError()) {
      hasError = true;
    }

    return doc;
  }

  //----------------------------------------------------------------------------
  private QuantIndiv[] loadQuantData(File f) throws GEException {
    System.out.println("Loading quantative data from '" + f + "'...");
    QuantDocument qdoc = new QuantDocument();
    try {
      qdoc.read(f, covar_ids);
    } catch (Exception e) {
      throw new GEException("failed to read file : '" + f + "', " + e);
    }

    return qdoc.getQuantIndiv();
  }

  //----------------------------------------------------------------------------
  public LinkageParameterData loadLinkageParameterData(File parf) {
    try {
      LinkageFormatter f = new LinkageFormatter(new BufferedReader(
        new FileReader(parf)), "Par file");
      LinkageParameterData pd = new LinkageParameterData(f);
      return pd;
    } catch (Exception e) {
      System.out.println("Failed to read Linkage Parameter file : " + parf.getName());
      System.out.println(e.getMessage());
      return null;
    }
  }

  //---------------------------------------------------------------------------
  public GDef getGDef() { //return theSpec.getGDef(); }
    return gdef;
  }

  //Ryan 06-21-07 Temporary function to get inDate
  public Calendar getinDate() {
    return indate;
  }

  //Ryan added 09/03/07 added function to get outpathstem variable to use as the file stem
  //for hapBuilder output files. The report function for hapBuilder should probably 
  //be placed in this file or Reporter.java.
  public String getoutpathStem() {
    return outpathStem;
  }

  //---------------------------------------------------------------------------
  public boolean reportCompleted() {
    return reportCompleted;
  }
}
