//******************************************************************************
// Reporter.java
//******************************************************************************
package edu.utah.camplab.genie.app.rgen;

import edu.utah.camplab.genie.gm.AlleleFormat;
import edu.utah.camplab.genie.gm.FreqDataSet;
import edu.utah.camplab.genie.gm.GDef;
import edu.utah.camplab.genie.gm.GtypeMatcher;
import edu.utah.camplab.genie.ped.Study;
import edu.utah.camplab.genie.stat.*;
import edu.utah.camplab.genie.util.Ut;

import java.io.*;
import java.text.NumberFormat;
import java.util.*;

//import edu.utah.camplab.genie.stat.ResultImp;

//==============================================================================
public class Reporter {

  private static final int TABLE_WIDTH = 95;
  private static final int LABEL_FWIDTH = 15;
  private static final int COL_FWIDTH = 20;
  private static final int DIV_FWIDTH = 3;
  private static int numSim;
  private static Calendar inDate;
  private static String pathStem;
  private static NumberFormat resultFormat, scientificFormat;
  private PrintWriter pwReport, pwSummary, pwSimulated, pwDecode;
  private String[] studyname, peddatasrc;
  private GDef gdef;
  private Study[] study;
  private Map mparams;
  private int seed;
  private String distribution;
  private AlleleFormat alleleformat;
  private boolean decodeFlag;
  private boolean printFreqFlag = false;
  private int cycleIndex;
  //private int defaultFractionDigits = 8;

  //----------------------------------------------------------------------------
  Reporter(String app_id, String app_ver, Specification spec,
           Study[] study, Calendar inCal,
           CCAnalysis[] a, String pathStem, int cycleIndex)
    throws IOException {
    mparams = spec.getAllGlobalParameters();
    gdef = spec.getGDef();
    String reporttype = ((String) mparams.get("report")).toLowerCase();
    seed = spec.getRseed();
    numSim = spec.getNumberOfSimulations();
    distribution = spec.getDistribution();
    alleleformat = spec.getAlleleFormat();
    decodeFlag = spec.getDecodeFlag();
    printFreqFlag = spec.getPrintFreqFlag();
    this.study = study;
    this.pathStem = pathStem;
    this.cycleIndex = cycleIndex;
    studyname = new String[study.length];
    peddatasrc = new String[study.length];
    for (int i = 0; i < study.length; i++) {
      studyname[i] = study[i].getStudyName();
      peddatasrc[i] = study[i].getPedData().getID();
    }
    scientificFormat = Specification.getScientificFormat();
    resultFormat = spec.getResultFormat();

    if ("summary".equals(reporttype)) {
      File fsummary = Ut.fExtended(pathStem, "summary");
      pwSummary = new PrintWriter(
        new BufferedWriter(new FileWriter(fsummary)), true);
      for (int i = 0; i < a.length; i++) {
        summaryCCAnalysis(a[i]);
      }
      closeReport(pwSummary, fsummary);
    }
    else if ("simulated".equals(reporttype)) {
      File fsimulated = Ut.fExtended(pathStem, "simulated");
      pwSimulated = new PrintWriter(
        new BufferedWriter(new FileWriter(fsimulated)), true);
      for (int i = 0; i < a.length; i++) {
        simulatedCCAnalysis(a[i]);
      }
      closeReport(pwSimulated, fsimulated);
    }
    else if ("both".equals(reporttype)) {
      File fsummary = Ut.fExtended(pathStem, "summary");
      File freport = Ut.fExtended(pathStem, "report");
      pwSummary = new PrintWriter(
        new BufferedWriter(new FileWriter(fsummary)), true);
      pwReport = new PrintWriter(
        new BufferedWriter(new FileWriter(freport)), true);
      reportHeaderInfo(app_id, app_ver, spec, inCal);
      for (int i = 0; i < a.length; i++) {
        summaryCCAnalysis(a[i]);
        reportCCAnalysis(a[i], i + 1);
      }
      putElapseTime();
      closeReport(pwSummary, fsummary);
      closeReport(pwReport, freport);
    }
    else {
      File freport = Ut.fExtended(pathStem, "report");
      pwReport = new PrintWriter(
        new BufferedWriter(new FileWriter(freport)), true);
      reportHeaderInfo(app_id, app_ver, spec, inCal);
      for (int i = 0; i < a.length; i++) {
        reportCCAnalysis(a[i], i + 1);
      }
      putElapseTime();
      closeReport(pwReport, freport);
    }
    if (printFreqFlag) {
      printHapFreqToFile();
    }

    //System.out.println("Analyses Completed Successfully");
  }

  //----------------------------------------------------------------------------
  void reportHeaderInfo(String app_id, String app_ver, Specification spec,
                        Calendar inCal) throws IOException {
    System.out.println("Writing Report.....");
    pwReport.println("********** " + app_id + " " + app_ver + " Report **********");
    pwReport.println("Created: " + inCal.getTime());
    pwReport.println("Specification from: " + spec.getName());
    pwReport.println("Data from: " + peddatasrc[0]);
    for (int i = 1; i < peddatasrc.length; i++) {
      pwReport.println("           " + peddatasrc[i]);
    }
    boolean printDecodeToFile = false;
    if (decodeFlag) {
      String decodeFreqFile = spec.getDecodeFile();
      if (decodeFreqFile != null) {
        pwReport.println("Input Decode Allele Frequency File: " + decodeFreqFile);
      }
      else {
        printDecodeToFile = true;
      }
      //printDecodeInfo(printDecodeToFile);
    }

    //numSim = spec.getNumberOfSimulations();
    inDate = inCal;
    String[] pairs = new String[mparams.size()];
    int ipair = 0;

    for (Iterator i = mparams.entrySet().iterator(); i.hasNext(); ) {
      Map.Entry e = (Map.Entry) i.next();
      pairs[ipair++] = e.getKey() + " = " + e.getValue();
    }
    pwReport.println("Runtime global parameters: ");
    pwReport.print("  " + Ut.join(pairs, Ut.N + "  "));
    pwReport.println();
    pwReport.println("Number of simulation executed: " + cycleIndex);
    pwReport.println();
    if (decodeFlag) {
      printDecodeInfo(printDecodeToFile);
    }
  }

  //----------------------------------------------------------------------------

  /**
   * void reportCCAnalysis(CCAnalysis a)
   * {
   * if ( "both".equals(mparams.get("report")) )
   * bothCCAnalysis(a);
   * else
   * summaryCCAnalysis(a);
   * }
   */
  //----------------------------------------------------------------------------
  public void reportCCAnalysis(CCAnalysis a, int index) {
    CCAnalysis.Table[][][][] tables = a.getObservedTable();
    CCAnalysis.Table[][][] metaTDTtables = a.getObsMetaTDTTable();
    // work from here
    //CCAnalysis.Table[]
    int nMetaReports = a.getMetaReports(0, 0).length;
    int nMetaTDTReports = a.getMetaTDTReports(0, 0).length;
    int nrepeat = a.getRepeat();
    String analysisType = a.getType();
    ;
    String[] tabletype = a.getTableType();
    int ntabletype = tabletype.length;
    CCAnalysis.Table.Column[] column = a.getColDefs();
    int nLoci = a.getLoci().length;
    pwReport.println();
    pwReport.println(Ut.fit(" ", TABLE_WIDTH + 6, '_', false));
    pwReport.println(
      Ut.fit(" ", DIV_FWIDTH, '*', false)
        + "Analysis " + index
        + Ut.fit(" ", DIV_FWIDTH, '*', true));
    pwReport.println("   Table Pattern ");
    for (int i = 0, n = column.length; i < n; ++i) {
      GtypeMatcher[] gtMatcher = column[i].getGtypeMatcher()[0];
// for Interaction OR table does  not match pattern table
      String wtStr = "[" + column[i].getWeight() + "]";
      pwReport.print(Ut.rjust("Column: " + (i + 1) + wtStr, COL_FWIDTH));
      //pwReport.println();
      for (int k = 0; k < gtMatcher.length; k++) {
        if (k > 0) {
          pwReport.println(Ut.rjust(" or ", COL_FWIDTH));
        }
        for (int j = 0; j < nLoci; j++) {
          int nSubGroup = gtMatcher[k].originalPatterns[j].length;
          String allelesPattern = "";
          int l = 0;
          String tmpPattern;
          while (l < nSubGroup) {
            byte tmp1 = gtMatcher[k].originalPatterns[j][l];
            if (tmp1 == alleleformat.getMissingData()) {
              tmpPattern = ".";
            }
            else {
              tmpPattern = Byte.toString(tmp1);
              if (!decodeFlag) {
                if (alleleformat.format.startsWith("char")) {
                  System.out.println("format char " + tmp1);
                  tmpPattern = "" + (char) (tmp1 & 0xFF);
                }
              }
            }
            allelesPattern += tmpPattern;
            if (!analysisType.equals("allele")) {
              byte tmp2 = gtMatcher[k].originalPatterns[j][l + 1];
              if (tmp2 == alleleformat.getMissingData()) {
                tmpPattern = "/.";
              }
              else {
                tmpPattern = "/" + Byte.toString(tmp2);
                if (!decodeFlag) {
                  if (alleleformat.format.startsWith("char")) {
                    tmpPattern = "/" + (char) (tmp2 & 0xFF);
                  }
                }
              }
              l += 2;
              allelesPattern += tmpPattern;
            }
            else {
              l++;
            }
            if (l < nSubGroup) {
              allelesPattern += " or ";
            }
          }
          if (j > 0 || k > 0) {
            pwReport.print(Ut.rjust(" ", COL_FWIDTH));
          }
          pwReport.println(Ut.rjust(allelesPattern, COL_FWIDTH));
        }
      }
      pwReport.println();
    }
    for (int r = 0; r < nrepeat; r++) {
      String markers = new String();
      int[] loci_id = a.getLoci(r);
      for (int i = 0; i < loci_id.length; i++) {
        String markerName = gdef.getLocus(loci_id[i]).getMarker();
        if (markerName.length() == 0) {
          markerName = "Marker " + loci_id[i];
        }
        markers += markerName + " ";
      }

      for (int t = 0; t < ntabletype; t++) {
        String tableName = "";
        String modelName = "";
        if (tabletype[t].equals("psuedo")) {
          tableName = "   Table Type : Psuedo";
        }
        if (a.getModel() != null) {
          modelName = "   Model : " + a.getModel();
        }
        //pwReport.println();
        pwReport.println(Ut.fit(" ", TABLE_WIDTH + 6, '_', false));
        //pwReport.print(
        //  Ut.fit(" ", DIV_FWIDTH, '*', false)
        //  + "Analysis " + index
        //  + Ut.fit(" ", DIV_FWIDTH, '*', true) 
        pwReport.println(
          tableName
            + "   Loci : " + markers
            + "   Type : " + a.getType()
            + modelName);
        pwReport.println();

        for (int s = 0; s < tables[r][t].length; s++) {
          pwReport.println();
          pwReport.println("Study : " + studyname[s]);
          for (int j = 0; j < tables[r][t][s].length; j++) {
            if (tables[r][t][s][j] != null &&
              !(tables[r][t][s][j] instanceof SibTDTTable)) {
              int tablenamesize = (tables[r][t][s][j].getTableName()).length();
              int tabpadsize = (TABLE_WIDTH - tablenamesize + 2) / 2;
              pwReport.println(Ut.fit(" ", tabpadsize, '=', false) +
                tables[r][t][s][j].getTableName() +
                Ut.fit(" ", tabpadsize, '=', true));
              putColumnDefinitions(tables[r][t][s][j]);
              putTableContents(tables[r][t][s][j]);
              pwReport.println();
            }
          }
          putFullStatReports(a.getStatReports(r, t, s));
        }
        if (nMetaReports > 0) {
          pwReport.println();
          String header = "Meta / CMH Analysis";
          int headersize = header.length();
          int headerpadsize = (TABLE_WIDTH - headersize + 2) / 2;
          pwReport.println(Ut.fit(" ", headerpadsize, '_', false) +
            header + Ut.fit(" ", headerpadsize, '_', true));
          putFullStatReports(a.getMetaReports(r, t));
        }
        if (nMetaTDTReports > 0) {
          pwReport.println();
          String header = "Meta TDT Analysis";
          int headersize = header.length();
          int headerpadsize = (TABLE_WIDTH - headersize + 2) / 2;
          pwReport.println(Ut.fit(" ", headerpadsize, '_', false) +
            header + Ut.fit(" ", headerpadsize, '_', true));
          pwReport.println();
          for (int m = 0; m < metaTDTtables[r][t].length; m++) {
            if (metaTDTtables[r][t][m] != null &&
              !(metaTDTtables[r][t][m] instanceof SibTDTTable)) {
              String tName = metaTDTtables[r][t][m].getTableName();
              int tablenamesize = tName.length();
              int tabpadsize = (TABLE_WIDTH - tablenamesize + 2) / 2;
              pwReport.println(Ut.fit(" ", tabpadsize, '=', false) +
                "Meta " + tName +
                Ut.fit(" ", tabpadsize, '=', true));
              putColumnDefinitions(metaTDTtables[r][t][m]);
              putTableContents(metaTDTtables[r][t][m]);
              pwReport.println();
            }
          }
          putFullStatReports(a.getMetaTDTReports(r, t));
        }
      }
    }
  }

  //----------------------------------------------------------------------------
  public void summaryCCAnalysis(CCAnalysis a) {
    CCAnalysis.Table[][][][] tables = a.getObservedTable();
    CCAnalysis.Table[][][] metaTDTtables = a.getObsMetaTDTTable();
    int nMetaReports = a.getMetaReports(0, 0).length;
    int nMetaTDTReports = a.getMetaTDTReports(0, 0).length;
    int nrepeat = a.getRepeat();
    for (int r = 0; r < nrepeat; r++) {
      String markers = new String();
      int[] loci_id = a.getLoci(r);
      for (int i = 0; i < loci_id.length; i++) {
        String markerName = gdef.getLocus(loci_id[i]).getMarker();
        if (markerName.length() == 0) {
          markerName = "Marker " + loci_id[i];
        }
        markers += markerName + " ";
      }
      pwSummary.print(markers + "  " + a.getModel() + "  " + seed + "  ");

      for (int t = 0; t < tables[r].length; t++) {
        for (int s = 0; s < tables[r][t].length; s++) {
          pwSummary.print(" " + studyname[s] + " ");
          putSummaryStatReports(a.getStatReports(r, t, s));
        }

        if (nMetaReports > 0) {
          putSummaryStatReports(a.getMetaReports(r, t));
        }
        if (nMetaTDTReports > 0) {
          putSummaryStatReports(a.getMetaTDTReports(r, t));
        }
        pwSummary.println(" ");
      }
    }
  }

  //----------------------------------------------------------------------------
  public void simulatedCCAnalysis(CCAnalysis a) {
    CCAnalysis.Table[][][][] tables = a.getObservedTable();
    CCAnalysis.Table[][][] metaTDTtables = a.getObsMetaTDTTable();
    int nMetaReports = a.getMetaReports(0, 0).length;
    int nMetaTDTReports = a.getMetaTDTReports(0, 0).length;

    int nrepeat = a.getRepeat();
    String markers = new String();
    for (int r = 0; r < nrepeat; r++) {
      int[] loci_id = a.getLoci(r);
      for (int i = 0; i < loci_id.length; i++) {
        String markerName = gdef.getLocus(loci_id[i]).getMarker();
        if (markerName.length() == 0) {
          markerName = "Marker " + loci_id[i];
        }
        markers += markerName + " ";
      }
      pwSimulated.print(markers + "  " + a.getModel() + "  " + seed + "  ");

      for (int t = 0; t < tables[r].length; t++) {
        for (int s = 0; s < tables[r][t].length; s++) {
          putSimulatedStatReports(a.getStatReports(r, t, s));
        }

        if (nMetaReports > 0) {
          putSummaryStatReports(a.getMetaReports(r, t));
        }
        if (nMetaTDTReports > 0) {
          putSummaryStatReports(a.getMetaTDTReports(r, t));
        }
        pwSummary.println(" ");
      }
    }
  }

  //----------------------------------------------------------------------------
  void putElapseTime() {
    long[] times = getElapsedTime();
    pwReport.println();
    pwReport.println("Elapse Time : " + times[0] + " h : " +
      times[1] + " m : " +
      times[2] + " s");
  }

  //----------------------------------------------------------------------------
  void closeReport(PrintWriter pw, File file) {
    pw.close();
    System.out.println("Report written to '" + file + "'." + Ut.N);
  }

  //----------------------------------------------------------------------------
  private void putColumnDefinitions(CCAnalysis.Table t) {
    int colheadsize = (t.getColumnHeading()).length();
    int colpadsize = (TABLE_WIDTH - colheadsize - LABEL_FWIDTH + 2) / 2;
    //int colpadsize = ( TABLE_WIDTH - colheadsize - LABEL_FWIDTH - 2 ) / 2 ;

    //pwReport.println();
    pwReport.println(Ut.ljust(t.getRowHeading(), LABEL_FWIDTH) + " " +
      Ut.fit(" ", colpadsize, '-', false) + t.getColumnHeading() +
      Ut.fit(" ", colpadsize, '-', true));
    pwReport.print(Ut.ljust(" ", LABEL_FWIDTH) + " ");

    for (int i = 0, n = t.getColumnCount(); i < n; ++i) {
      pwReport.print(
        Ut.rjust(t.getColumnLabelAt(i), COL_FWIDTH)
      );
    }
    String tempTotal = "TOTAL";
    if (t instanceof QuantitativeTable) {
      tempTotal = "Row Total";
    }
    pwReport.println(Ut.rjust(tempTotal, COL_FWIDTH));
    pwReport.println();
  }

  //----------------------------------------------------------------------------
  private void putTableContents(CCAnalysis.Table t) {
    int nRow = t.getRowCount();
    if (nRow > 0) {
      for (int irow = 0; irow < nRow; ++irow) {
        CCAnalysis.Table.Row row = t.getRowAt(irow);
        Number rowtotal = new Double(0.0);
        if (row != null) {
          Number[] rowCells = row.getCells();
          String[] extraInfo = new String[rowCells.length];
          if (t instanceof CombTDTTable) {
            rowtotal = null;
          }
          else if (t instanceof QuantitativeTable) {
            Number[] tempCells = row.getCells();
            int[] tempCellsN = row.getCellsN();

            for (int i = 0; i < rowCells.length; i++) {
              rowCells[i] = new Double(tempCells[i].doubleValue() / (double) tempCellsN[i]);
              if (Double.isNaN(tempCells[i].doubleValue())) {
                rowCells[i] = new Double(0.0);
              }
              rowtotal = new Double(rowtotal.doubleValue() + rowCells[i].doubleValue());
              extraInfo[i] = "(" + tempCellsN[i] + ")";
            }
          }
          else {
            rowtotal = t.getTotals().forRows()[irow];
            for (int i = 0; i < rowCells.length; i++) {
              //Number[] tempCells = row.getCells();
              //String percentage = toString(tempCells[i].doubleValue()/rowtotal.doubleValue());
              //System.out.println("cell["+i+"]" + rowCells[i].doubleValue() + " row total " + rowtotal.doubleValue());
              extraInfo[i] = "(" +
                toString((rowCells[i].doubleValue() / rowtotal.doubleValue()) *
                  100) + ")";
            }
          }
          putTableContentLine(
            //row.getPtype().toString(), row.getCells(), t.getTotals().forRows()[irow]
            row.getLabel(), rowCells, extraInfo, rowtotal);
        }
      }

      if (t.getRowAt(0) != null) {
        Number[] cTotals = t.getTotals().forColumns();
        String[] extraTotalInfo = new String[cTotals.length];
        Number gTotal = t.getTotals().forTable();
        String tempTotal = "TOTAL";
        if (t instanceof SibTDTTable || t instanceof CombTDTTable) {
          gTotal = null;
        }
        // Quantitative Table special case!
        if (t instanceof QuantitativeTable) {
          //Number[] colTotals = cTotals;
          Number[] colTotals = new Double[cTotals.length];
          ;
          gTotal = new Double(0.0);
          tempTotal = "Col TOTAL";
          QuantitativeTable q = (QuantitativeTable) t;
          QuantitativeTable.TotalsExt qtotals = (QuantitativeTable.TotalsExt) q.getTotals();
          int[] colcount = qtotals.forCol_counts();
          for (int i = 0; i < q.getColumnCount(); i++) {
            colTotals[i] = new Double(0.0);
            if (!Double.isNaN(cTotals[i].doubleValue()) &&
              colcount[i] != 0) {
              colTotals[i] = new Double(cTotals[i].doubleValue() / (double) colcount[i]);
            }

            //System.out.println("cTotls["+i+"] " + cTotals[i] + " colcount " + colcount[i] +
            //" colTotals " + colTotals[i]);
            cTotals[i] = colTotals[i];
            gTotal = new Double(gTotal.doubleValue() + colTotals[i].doubleValue());
          }
        }
        //pwReport.println("--");
        putTableContentLine(tempTotal, cTotals, extraTotalInfo, gTotal);
      }

      if (t.getAttachMessage() != null) {
        pwReport.println(t.getAttachMessage());
      }
      //pwReport.println();
    }
  }

  //----------------------------------------------------------------------------
  //private void putTableContentLine(String label, String[] vals, int total)
  // Change vals to String[] so we can pass cell value (quant) + count
  private void putTableContentLine(String label, Number[] vals,
                                   String[] extraInfo, Number total) {
    pwReport.print(Ut.ljust(label, LABEL_FWIDTH) + ":");
    for (int i = 0; i < vals.length; ++i) {
      //  pwReport.print(Ut.rjust(vals[i], COL_FWIDTH));
      String printval;
      if (vals[i] instanceof Double) {
        printval = toString(vals[i].doubleValue());
      }
      else {
        printval = String.valueOf(vals[i]);
      }

      if (extraInfo[i] != null) {
        printval = printval + extraInfo[i];
      }

      pwReport.print(Ut.rjust(printval, COL_FWIDTH));
    }
    String printtotal = " ";
    if (total != null) {
      // need work !! 
      if (total instanceof Double) {
        printtotal = toString(total.doubleValue());
      }
      else {
        printtotal = String.valueOf(total);
      }
    }
    pwReport.println(Ut.rjust(printtotal, COL_FWIDTH));
  }

  //----------------------------------------------------------------------------
  private void putFullStatReports(CCStatRun.Report[] reports) {
    for (int i = 0; i < reports.length; ++i) {
      pwReport.println(reports[i].getTitle() + " : ");
      // replaced by resultMap in compTracker
      //int[] validSim = reports[i].getNumSimulationReport(); 
      //if ( validSim.length > 0  && !distribution.equals("weightedindex") )
      //{
      //  for ( int j = 0; j < validSim.length; j++ ) 
      //  {
      //    pwReport.print( validSim[j] + " / " + numSim );
      //    if ( validSim.length > 1 && j != (validSim.length - 1) )
      //      pwReport.print(", ");
      //  }
      //  pwReport.println(" total statistics calculated");
      //}
      //else 
      //  pwReport.println();
      //pwReport.println("   Observed statistic : " + 
      //                 doubleToString(reports[i].getObservationalReport()));
      //if ( reports[i].getObsExtraReport() != null )
      //  pwReport.println("   " + doubleToString(reports[i].getObsExtraReport()));
      //pwReport.println("   " + reports[i].getCompStatTitle() + 
      //                 doubleToString(reports[i].getInferentialReport()));
      //if ( reports[i].getInfExtraReport() != null )
      //  pwReport.println("   " + reports[i].getInfExtraStatTitle() +
      //                   " : " + doubleToString(reports[i].getInfExtraReport()));
      pwReport.println("   Observed statistic : " +
        (reports[i].getObservationalReport()).toString());
      //if ( reports[i].getObsExtraReport() != null ) 
      //pwReport.println("   " + reports[i].getObsExtraReport());
      pwReport.println("   " + reports[i].getCompStatTitle() +
        (reports[i].getInferentialReport()).toScientificFormat());
      if (reports[i].getInfExtraReport() != null) {
        pwReport.println("   " + reports[i].getInfExtraStatTitle() +
          (reports[i].getInfExtraReport().toString()));
      }
      if (reports[i].getWarning() != null) {
        pwReport.println("   " + reports[i].getWarning());
      }
    }
  }

  //----------------------------------------------------------------------------
  private void putSummaryStatReports(CCStatRun.Report[] reports) {
    for (int i = 0; i < reports.length; ++i) {
      //pwSummary.print(" " + doubleToString(reports[i].getObservationalReport()));
      //pwSummary.print(" " + doubleToString(reports[i].getInferentialReport()));
      pwSummary.print(" " + reports[i].getObservationalReport().toString());
      pwSummary.print(" " + reports[i].getInferentialReport().toScientificFormat());
      if (reports[i].getInfExtraReport() != null)
      //pwSummary.print(" " + doubleToString(reports[i].getInfExtraReport()));
      {
        pwSummary.print(" " + reports[i].getInfExtraReport().toString());
      }
    }
  }

  //----------------------------------------------------------------------------
  private void putSimulatedStatReports(CCStatRun.Report[] reports) {
    for (int i = 0; i < reports.length; ++i) {
      pwSimulated.print(" obs " +
        reports[i].getObservationalReport());
      //doubleToString(reports[i].getObservationalReport()));
      //pwSimulated.print(" inf " + reports[i].getInferentialReport());
      //if ( reports[i].getInfExtraReport() != null )
      //  pwSummary.print(" " + reports[i].getInfExtraReport());
      pwSimulated.println();

      if (reports[i].getInfSimReport() != null) {
        Vector<Vector<Double>> v = reports[i].getInfSimReport();
        int nv = v.size();
        int ncv = v.get(0).size();
        for (int k = 0; k < ncv; k++) {
          pwSimulated.println();
          //pwSimulated.print("sim : " + k + " " );
          for (int j = 0; j < nv; j++) {
            double value = v.get(j).get(k);
            pwSimulated.print(" " + value);
          }
        }
      }
      pwSimulated.println();
    }
  }

  //----------------------------------------------------------------------------
  public long[] getElapsedTime() {
    long outSecs = Calendar.getInstance().getTimeInMillis();
    long inSecs = inDate.getTimeInMillis();
    long diffSecs = outSecs - inSecs;
    long hours = diffSecs / (1000 * 60 * 60);
    long mins = diffSecs / (1000 * 60) - hours * 60;
    long secs = diffSecs / 1000 - mins * 60;

    return new long[]{hours, mins, secs};
  }

  //----------------------------------------------------------------------------
  public String toString(String str) {
    return str;
  }

  public String toString(double val) {
    if (val == 0.0) {
      return "0.0";
    }
    else if (Double.isNaN(val) || Double.isInfinite(val)) {
      return "-";
    }
    else {
      return resultFormat.format(val);
    }
  }

  //----------------------------------------------------------------------------
  public String toScientificFormat(String str) {
    return str;
  }

  public String toScientificFormat(double val) {
    if (val == 0.0) {
      return "0.0";
    }
    else if (Double.isNaN(val) || Double.isInfinite(val)) {
      return "-";
    }
    else {
      return scientificFormat.format(val);
    }
  }

  //----------------------------------------------------------------------------
  public void printHapFreqToFile() throws IOException {
    for (int i = 0; i < study.length; i++) {
      File freq = Ut.fExtended(pathStem, studyname[i] + ".freq");
      PrintWriter pwFreq = new PrintWriter(
        new BufferedWriter(new FileWriter(freq)), true);
      FreqDataSet[][] fds = study[i].getFreqDataSet();
      if (fds != null && fds.length > 0) {
        for (int j = 0; j < fds.length; j++) {
          for (int k = 0; k < fds[j].length; k++) {
            double frequency = fds[j][k].getFrequency();
            byte[] code = fds[j][k].getCode();
            String convertedCode = "";
            String separator = "-";
            int ncode = code.length;
            for (int c = 0; c < ncode; c++) {
              if ((c + 1) == ncode) {
                separator = "";
              }
              if (alleleformat.format.startsWith("char")) {
                convertedCode += (((char) (code[c] & 0xFF)) + separator);
              }
              else {
                convertedCode += ((code[c] & 0xFF) + separator);
              }
            }
            if (convertedCode != null) {
              pwFreq.println(frequency + " " + convertedCode);
            }
          }
        }
      }
      System.out.println(" Study : " + studyname[i] + "'s Allele / Haplotype frequency table was written to " + freq.toString());
      closeReport(pwFreq, freq);
    }
  }

  //----------------------------------------------------------------------------
  public void printDecodeInfo(boolean printDecodeToFile) throws IOException {
    File decodeFreqFile = null;
    if (printDecodeToFile) {
      decodeFreqFile = Ut.fExtended(pathStem, "decodefreq");
      System.out.println(" Decoded Allele Frequency written to " + decodeFreqFile);
      pwReport.println("Decoded Allele Frequency File : " + decodeFreqFile);
      pwDecode = new PrintWriter(
        new BufferedWriter(new FileWriter(decodeFreqFile)), true);
      pwDecode.println(Ut.ljust("Locus", COL_FWIDTH) +
        Ut.ljust("Input Data", COL_FWIDTH) +
        Ut.ljust("Freq", COL_FWIDTH) +
        Ut.ljust("Represented in Analysis", COL_FWIDTH));
    }
    int nLocus = gdef.getLocusCount();
    for (int i = 0; i < nLocus; i++) {
      GDef.Locus locus = gdef.getLocus(i);
      TreeMap<Byte, FreqDataSet> decoderMap = locus.getDecoderMap();
      for (Iterator it = decoderMap.keySet().iterator(); it.hasNext(); ) {
        Byte key = (Byte) it.next();
        FreqDataSet value = decoderMap.get(key);
        byte code = (value.getCode())[0];
        String convertedValue = "";
        if (alleleformat.format.startsWith("char")) {
          convertedValue += (char) (code & 0xFF);
        }
        else {
          convertedValue += (code & 0xFF);
        }
        if (key != null && convertedValue != null) {
          String keyValue = "" + (key & 0xFF);
          pwReport.println("Locus " + locus.getMarker() + ", allele " +
            convertedValue + ", frequency " +
            toString(value.getFrequency()) +
            ", is represented by allele " +
            keyValue + " in results");
          if (printDecodeToFile) {
            pwDecode.println(Ut.ljust(locus.getMarker(), COL_FWIDTH) +
              Ut.ljust(convertedValue, COL_FWIDTH) +
              Ut.ljust(toString(value.getFrequency()), COL_FWIDTH) +
              Ut.ljust(keyValue, COL_FWIDTH));
          }
        }
      }
    }
    if (printDecodeToFile) {
      closeReport(pwDecode, decodeFreqFile);
    }
  }
}
