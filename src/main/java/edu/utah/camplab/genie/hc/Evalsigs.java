package edu.utah.camplab.genie.hc;

import java.io.*;
import java.util.*;

public class Evalsigs {
  public Map obs_levelps = new HashMap();
  public Map sims_levelps = new HashMap();
  public Map ps2model = new HashMap();
  public double[] obs_cdf;
  public double[] sims_cdf;

  //----------------------------------------------------------------------------
  public static double round(double val, int places) {
    long factor = (long) Math.pow(10, places);

    // Shift the decimal the correct number of places
    // to the right.
    val = val * factor;

    // Round to the nearest integer.
    long tmp = Math.round(val);

    // Shift the decimal the correct number of places
    // back to the left.
    return (double) tmp / factor;
  }

  //----------------------------------------------------------------------------
  public void readfile(String filename) {
    String type = "obs";
    if (filename.equals("all_sims.final")) {
      type = "sims";
    }
    StringBuffer contents = new StringBuffer();

    //declared here only to make visible to finally clause
    BufferedReader input = null;
    try {
      //use buffering, reading one line at a time
      //FileReader always assumes default encoding is OK!
      input = new BufferedReader(new FileReader(filename));
      String line = null; //not declared within while loop
      while ((line = input.readLine()) != null) {
        //System.out.println(line);
        process_line(line, type);
      }
    } catch (FileNotFoundException ex) {
      ex.printStackTrace();
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  //----------------------------------------------------------------------------
  private void store_models(String model, Double p, int step) {
    Set s = new HashSet();
    String key = step + ":" + p;
    if (ps2model.containsKey(key)) {
      s = (Set) ps2model.get(key);
      s.add(model);
    }
    else {
      s.add(model);
      ps2model.put(key, s);
    }
  }

  //----------------------------------------------------------------------------
  public String retrieve_models(Double p, int lvl) {
    String models_out = "";
    if (lvl == -1) {
      for (int i = 1; i < obs_levelps.size(); i++) {
        Set s = (Set) ps2model.get(i + ":" + p);
        if (s != null) {
          models_out = models_out + " " + s.toString();
        }
      }
    }
    else {
      Set s = (Set) ps2model.get(lvl + ":" + p);
      if (s != null) {
        models_out = s.toString();
      }
    }
    return models_out;
  }

  //----------------------------------------------------------------------------
  public void process_line(String line, String type) {
    Map m = obs_levelps;
    if (type.equals("sims")) {
      m = sims_levelps;
    }
    String[] split_line = line.split(":");
    String lociset = split_line[1];
    int step = lociset.split("-").length;
    List map_ps = new ArrayList();

    if (m.containsKey(step)) {
      map_ps = (List) m.get(step);
    }

    String model = split_line[2];
    String stat = split_line[3];
//    System.out.println(line);
    if (stat.indexOf("Odd") > -1) {
      //Get the number of columns
      int numcols = (split_line.length - 4) / 3;
      for (int i = 0; i < numcols; i++) {
        int lineloc = 3 + (3 * (i + 1));
        double p = Double.parseDouble(split_line[lineloc]);
//        System.out.println("p-val:" + split_line[lineloc]);
//        System.out.println("p-val:" + p);
        map_ps.add(p);
        if (type.equals("obs")) {
          String key = line.substring(3, line.lastIndexOf(":"));
//          System.out.println(key);
          store_models(key, p, step);
        }
      }
    }
    else if (stat.indexOf("Chi-squared") > -1) {
      int lineloc = split_line.length - 1;
      double p = Double.parseDouble(split_line[lineloc]);
      map_ps.add(p);
      if (type.equals("obs")) {
        String key = line.substring(3, line.lastIndexOf(":"));
//        System.out.println(key);
        store_models(key, p, step);
        //store_models(model,p);
      }
    }
    m.put(step, map_ps);
  }

  //----------------------------------------------------------------------------
  public List get_levelps(int level, String type) {
    Map m = obs_levelps;
    if (type.equals("sims")) {
      m = sims_levelps;
    }

    if (level == -1) {
      level = m.keySet().size();
    }

    List aggps = new ArrayList();
    for (int i = 1; i <= level; i++) {
      List ss = (List) m.get(i);
      if (ss != null) {
        aggps.addAll(ss);
      }
    }
    Collections.sort(aggps);
    return aggps;
  }

  //----------------------------------------------------------------------------
  private int[] rank(List<Double> l, String type) {
    Double[] ll = l.toArray(new Double[0]);
    int len = ll.length;
    double[] cdf = new double[len];
    int[] ranks = new int[len];

    // Put the first rank in, which is the last item. This takes the highest rank.
    double previous_pv = ll[len - 1];
    // Start from the back and go forward. If the next item is the same as the previous
    // p-value, then set its rank to the same as the previous item.
    for (int i = (len - 1); i > -1; i--) {
      double pv = ll[i];
      int pos = i;
      if (pv == previous_pv && i != (len - 1)) {
        ranks[i] = ranks[i + 1];
      }
      else {
        ranks[i] = i + 1;
        previous_pv = pv;
      }
      double r = ranks[i];
      double dl = len;
      double res = r / dl;
      cdf[i] = res;
    }

    if (type.equals("obs")) {
      obs_cdf = cdf;
    }
    else {
      sims_cdf = cdf;
    }

    return ranks;
  }

  //----------------------------------------------------------------------------
  public double[][] level_efdr(int lvl) {
    List obs_lvl = get_levelps(lvl, "obs");
    List sims_lvl = get_levelps(lvl, "sims");
    return efdr(obs_lvl, sims_lvl);
  }

  //----------------------------------------------------------------------------
  public void all_efdr() throws IOException {
    int levels = obs_levelps.size();
    write_efdr(level_efdr(-1), -1);
//    for( int i = 1; i <= levels; i++ )
//    {
//      write_efdr(level_efdr(i),i);
//    }
  }

//  //Deprecated
//  private double empirical_p ( double p, List sims )
//  {
//    int count = sims.size();
//    double sp = 1.0;
//    while(p < sp && count > 0)
//    {
////      if(count == 10){
////        int ppp = 0;
////      }
//      sp = (Double) sims.get(count-1);
//      count = count - 1;
//    }
//    if(count == 0)
//    {
//      count = 1;
//    }
//    double ss = sims.size();
//    double ep = (Double) (count / ss);
//    return ep;
//  }

  //----------------------------------------------------------------------------
  private double[][] efdr(List obs, List sims) {
    int[] rank_obs = rank(obs, "obs");
    int[] rank_sims = rank(sims, "sims");
    List scdf_list = Arrays.asList(sims_cdf);
    double[][] eFDRs = new double[obs.size()][3];
    double previous_q = 1.0;
    for (int i = (obs_cdf.length - 1); i > -1; i--) {
      double obs_val = obs_cdf[i];
      double sim_pv = 0.0;
      int indx = 0;
      if (scdf_list.contains(obs_val)) {
        indx = scdf_list.indexOf(obs_val);
        sim_pv = (Double) sims.get(indx);
      }
      else {
        int count = 0;
        while (sims_cdf[count] < obs_val) {
          if (count == (sims_cdf.length - 1)) {
            break;
          }
          else {
            count++;
          }
        }
        indx = count;

        double sim_cdf_lower = 0.0;
        double sim_cdf_upper = 0.0;
        double sim_pval_lower = 0.0;
        double sim_pval_upper = 0.0;
        double ratio = 0.0;
        double sim_pval = 0.0;

        if (indx == 0) {
          sim_cdf_lower = 0.0;
          sim_cdf_upper = sims_cdf[indx];
          sim_pval_lower = 0.0;
          sim_pval_upper = (Double) sims.get(indx);
          ratio = (obs_val - sim_cdf_lower) / (sim_cdf_upper - sim_cdf_lower);
          sim_pval = ratio * (sim_pval_upper - sim_pval_lower) + sim_pval_lower;
        }
        else if (indx == sims_cdf.length - 1) {
          sim_pval = (Double) sims.get(indx);
        }
        else {
          sim_cdf_lower = sims_cdf[indx - 1];
          sim_cdf_upper = sims_cdf[indx];
          sim_pval_lower = (Double) sims.get(indx - 1);
          sim_pval_upper = (Double) sims.get(indx);
          ratio = (obs_val - sim_cdf_lower) / (sim_cdf_upper - sim_cdf_lower);
          sim_pval = ratio * (sim_pval_upper - sim_pval_lower) + sim_pval_lower;
        }
        double q = (Double) obs.get(i) / sim_pval;
        if (q > previous_q) {
          q = previous_q;
        }
        else {
          previous_q = q;
        }
        eFDRs[i][0] = q;
        //double ep = empirical_p((Double) obs.get(i),sims);
        eFDRs[i][1] = sim_pval;
        eFDRs[i][2] = (Double) obs.get(i);
      }
    }
    return eFDRs;
  }

  //----------------------------------------------------------------------------
  public void write_efdr(double[][] qs, int lvl) throws IOException {
    BufferedWriter out = null;
    String lvl_text = "" + lvl;
    if (lvl == -1) {
      lvl_text = "" + obs_levelps.size();
    }
    if (!new File("significance.out").exists()) {
      out = new BufferedWriter(new FileWriter("significance.out"));
      out.write("Step " + lvl_text + "\neFDR value\tEmpirical P-value\tObserved P-value\n");//\tModels\n");
    }
    else {
      out = new BufferedWriter(new FileWriter("significance.out", true));
      out.write("\nStep " + lvl_text + "\neFDR value\tEmpirical P-value\tObserved P-value\n");//\tModels\n");
    }

    for (int j = 0; j < qs.length; j++) {
      //System.out.println(eFDRs[j] + "\n");
      Double qval = round(qs[j][0], 6);
      Double epval = round(qs[j][1], 6);
      Double obsval = round(qs[j][2], 6);
//      String models = retrieve_models(qs[j][2],lvl);
//      System.out.println(models);
//      if(models.length() > 1){
//        models = models.substring(1,models.length()-1);
//      }
      out.write(qval + "\t\t" + epval + "\t\t\t" + obsval + "\n");// "\t\t\t" + models + "\n");
    }
    out.close();
  }
}

