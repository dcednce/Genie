//*****************************************************************************
// OddsRatiosWithCI.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.TreeMap;
import java.util.TreeSet;

//=============================================================================
public class OddsRatiosWithCI extends OddsRatios {

  //---------------------------------------------------------------------------
  public OddsRatiosWithCI() {
    super();
    infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //return new ORCompTrackerCI(); 
    return new RatiosComparisonTracker(true, true, 1.0);
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //----------------------------------------------------------------------------
  public Result getInfExtraStat(ComparisonTracker compTracker) {
    //ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
    //ORCompTrackerCI ORct = (ORCompTrackerCI) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    return rct.getConfidenceIntervals();
  }

  //----------------------------------------------------------------------------
  //public Vector<Vector<Double>> getInfSimStat(ComparisonTracker compTracker)
  public TreeMap<String, TreeSet<Double>> getInfSimStat() {
    //ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
    //ORCompTrackerCI ORct = (ORCompTrackerCI) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    return rct.getSimulatedResults();
  }
}
