//*****************************************************************************
// ChiSquared.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
//public class ChiSquared implements CCStat {

public class ChiSquared extends CCStatImp {
  //---------------------------------------------------------------------------
  public ChiSquared() {
    title = "Chi-squared";
  }

  //---------------------------------------------------------------------------
  //public String getName() { return "Chi-squared"; }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    Result res = null;
    CCAnalysis.Table.Totals totals = t.getTotals();
    // Check Row and Column Totals before calculation.
    if (StatUt.checkRowColSum(totals.forRows()[0],
      totals.forColumns(),
      totals.forTable()
    )) {
      //Number[] caser = t.getRowFor(Ptype.CASE).getCells();
      //Number[] contr = t.getRowFor(Ptype.CONTROL).getCells();
      // add ResultImp to CCStatImp
      //resultAt0 = new ResultImp.Real(
      //resultAt0 =  resultImp.new Real(
      res = StatUt.chiSquared(t.getRowFor(Ptype.CASE).getCells(),
        totals.forRows()[0],
        totals.forColumns(),
        totals.forTable());
    }
    else {
      //resultAt0 = new ResultImp.StringResult
      //resultAt0 =  resultImp.new StringResult
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING: row or column sum equals zero, **test** has not been performed.");
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  //Ryan 06-24-06 passed in thread.
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
