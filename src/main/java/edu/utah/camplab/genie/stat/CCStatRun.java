//*****************************************************************************
// CCStatRun.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import java.util.Vector;

//=============================================================================
public class CCStatRun {

  private static ResultManager resultManager = new ResultManager();
  private static CCStat.Result nullStr = resultManager.new StringResult("null");
  protected final CCStat myStat;
  private final CCAnalysis myOwner;
  private final CCStat.ComparisonTracker compTracker;
  private final Probe myProbe;
  protected CCStat.Result observedResult;
  private CCStat.Result r0Result;
  private String quantStatF;
  private CCStat.Result pValue;
  private int myRefColIndex;
  //---------------------------------------------------------------------------
  CCStatRun(CCAnalysis owner, CCStat stat, Probe p) {
    myOwner = owner;
    myStat = stat;
    int nCol = owner.getColumnWeights().length;
    compTracker = myStat.newComparisonTracker(myStat.getDegreeOfFreedom(nCol));
    myProbe = p;
    myRefColIndex = myOwner.getReferenceColumnIndex();
    observedResult = null;
  }

  //---------------------------------------------------------------------------
  void computeObserved(CCAnalysis.Table[] t) {
    //if (myOwner.checkDistinctWeights())
    //{
    r0Result = myStat.computeAt0(myOwner, t);
    //}
    //else
    //{
    // r0Result = new ResultImp.StringResult
    //  ("WARNING : weights are not distinct in the table defined, **test** has not been performed");
    //}
    //int df = myStat.getDegreeOfFreedom(t[0].getColumnCount());
    //compTracker.setDegreeOfFreedom(df);
    //observedResult = myStat.getObservedResult( r0Result,
    //                                           myRefColIndex );
    compTracker.setStartingResult(r0Result);
    observedResult = r0Result;
  }

  //---------------------------------------------------------------------------
  void computeObserved(CCAnalysis.Table t) {
    //if (myOwner.checkDistinctWeights())
    //{
    r0Result = myStat.computeAt0(myOwner, t);
    //}
    //else
    //{
    // r0Result = new ResultImp.StringResult
    //  ("WARNING : weights are not distinct in the table defined, **test** has not been performed");
    //}
    //observedResult = myStat.getObservedResult( r0Result,
    //                                           myRefColIndex );
    compTracker.setStartingResult(r0Result);
    observedResult = r0Result;
  }

  //---------------------------------------------------------------------------
  void computeSimulated(CCAnalysis.Table[] t) {
    //if ( !(r0Result instanceof ResultImp.StringResult) )
    if (r0Result != null) {
      CCStat.Result r = myStat.computeAtX(myOwner, t);
      compTracker.compareResultAtX(r);

      if (myProbe != null) {
        myProbe.logTableContents(t);
        myProbe.logStatResult(r);
      }
    }
  }

  //---------------------------------------------------------------------------
  void computeSimulated(CCAnalysis.Table t) {
    //if ( !(r0Result instanceof ResultImp.StringResult) )
    if (r0Result != null) {
      CCStat.Result r = myStat.computeAtX(myOwner, t);
      compTracker.compareResultAtX(r);

      if (myProbe != null) {
        myProbe.logTableContents(t);
        myProbe.logStatResult(r);
      }
    }
  }

  //---------------------------------------------------------------------------
  public CCStat.Result getObservedResult() {
    return observedResult;
  }

  //---------------------------------------------------------------------------
  Report newReport() {
    if (myProbe != null) {
      myProbe.closeLog();
    }

    //if ( r0Result instanceof ResultImp.StringResult )
    //if ( r0Result instanceof ResultManager.StringResult )
    //pValue = resultManager.new StringResult("-");
    if (r0Result == null) {
      pValue = resultManager.new Real(Double.NaN);
    }
    else {
      pValue = myStat.getInferentialResult(compTracker, myRefColIndex);
    }

    //final int[] numSimulations = compTracker.getComparisonCount();

    return new Report() {
      public String getTitle() {
        return myStat.getName();
      }

      //------------------------------------------------------------------------
      public String getCompStatTitle() {
        return myStat.getCompStatTitle();
      }

      //------------------------------------------------------------------------
      //public int[] getNumSimulationReport()
      //{
      //  return numSimulations;
      //}

      //------------------------------------------------------------------------
      public String getFullObservationalReport() {
        return "Observed statistic : " + observedResult;
      }

      //------------------------------------------------------------------------
      public CCStat.Result getObservationalReport()
      //public CCStat.Result getObservationalReport()
      {
        if (observedResult == null) {
          //System.out.println("null for " + myStat.getName() + "result ");
          //return new ResultImp.StringResult("null");
          return nullStr;
        }
        else {
          //System.out.println(" CCStatRun getObservationalReport ");
          return observedResult;
          //return observedResult;
        }
      }

      //------------------------------------------------------------------------
      public String getObsExtraReport()
      //public CCStat.Result getObsExtraReport()
      {
        String extraStat = null;
        CCStat.Result obsExtra = myStat.getObsExtraStat(r0Result);
        if (obsExtra != null) {
          extraStat = myStat.getObsExtraStatTitle() + obsExtra;
        }
        return extraStat;
        //return myStat.getObsExtraStat(r0Result);
      }

      //------------------------------------------------------------------------
      public String getFullInferentialReport() {
        return "Empirical p-value : " + pValue;
      }

      //------------------------------------------------------------------------
      //public CCStat.Result getInferentialReport()
      public CCStat.Result getInferentialReport() {
        return pValue;
        //return pValue;
      }

      //------------------------------------------------------------------------
      public String getInfExtraStatTitle() {
        return myStat.getInfExtraStatTitle();
      }

      //------------------------------------------------------------------------
      public String getInfExtraReport()
      //public CCStat.Result getInfExtraReport()
      {
        if (myStat.getInfExtraStat(compTracker) != null) {
          return (myStat.getInfExtraStat(compTracker)).toString();
        }
        else {
          return null;
        }
        //return myStat.getInfExtraStat(compTracker);
      }

      //------------------------------------------------------------------------
      public Vector<Vector<Double>> getInfSimReport() {
        String extraStat = null;
        //if ( myStat.getInfSimStat(compTracker) != null )
        return myStat.getInfSimStat(compTracker);
      }

      //------------------------------------------------------------------------
      public String getWarning() {
        // Temporary remove this section, as notval is now int[] not just int
        //int[] notval = compTracker.getnotval();
        //if (notval > 0 )
        //return compTracker.getMessages();
        // can not create message with comptracker inside ccstat, use resultAt0 instead 10/26/09 Jathine
        return r0Result.getMessages();
      }

      //------------------------------------------------------------------------
      //public Result gethitCount()
      //{ return myStat.gethitCount(); }

      //------------------------------------------------------------------------
      //public Result getComparisonCount()
      //{ return myStat.getComparisonCount(); }
    };
  }

  //----------------------------------------------------------------------------
  public String getStatName() {
    return myStat.getName();
  }

  //----------------------------------------------------------------------------
  public CCStat.ComparisonTracker getComparisonTracker() {
    return compTracker;
  }

  public interface Report {
    public String getTitle();

    public String getCompStatTitle();

    //public int[]  getNumSimulationReport();
    public String getFullObservationalReport();

    public CCStat.Result getObservationalReport();

    public String getObsExtraReport();

    public String getFullInferentialReport();

    public CCStat.Result getInferentialReport();

    public String getInfExtraStatTitle();

    public String getInfExtraReport();

    public Vector<Vector<Double>> getInfSimReport();

    public String getWarning();
  }

  public interface Probe {
    public void logTableContents(CCAnalysis.Table[] t);

    public void logTableContents(CCAnalysis.Table t);

    public void logStatResult(CCStat.Result r);

    public void closeLog();
  }
}
