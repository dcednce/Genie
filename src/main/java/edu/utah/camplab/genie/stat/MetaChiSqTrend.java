//*****************************************************************************
// MetaChiSqTrend.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class MetaChiSqTrend extends CCStatImp {
  //---------------------------------------------------------------------------
  public MetaChiSqTrend() {
    title = "Meta Chi-squared Trend";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    CCAnalysis.Table[] metaTable = getNonZeroTables(a, t, compTracker);
    int mt = metaTable.length;
    Number[][] caseCell = new Number[mt][];
    Number[][] columnTotal = new Number[mt][];
    Number[] caseTotal = new Number[mt];
    Number[] tableTotal = new Number[mt];

    if (metaTable == null || metaTable.length == 0) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        compTracker.setMessages
          ("WARNING: All tables have no data");
      }
    }
    else if (metaTable[0].getColumnCount() < 3) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        compTracker.setMessages
          ("WARNING: table has less than 3 columns, **test** has not been performed.");
      }
    }
    else {
      for (int i = 0; i < mt; i++) {
        ContingencyTable table = (ContingencyTable) metaTable[i];
        CCAnalysis.Table.Totals totals = table.getTotals();
        caseCell[i] = table.getRowFor(Ptype.CASE).getCells();
        columnTotal[i] = totals.forColumns();
        caseTotal[i] =
          totals.forRows()[table.ptID2Ix[Integer.parseInt((Ptype.CASE).getID())]];
        tableTotal[i] = totals.forTable();

        //if ( StatUt.checkRowColSum ( totals.forRows() [0],
        //                             totals.forColumns(),
        //                             totals.forTable()
        //                           ))
      }
      //resultAt0 =  new ResultImp.Real(
      res = StatUt.metaChiSqTrend(caseCell,
        columnTotal,
        caseTotal,
        tableTotal,
        a.getColumnWeights());

      //else 
      //  resultAt0 =  new ResultImp.StringResult
      // ("WARNING: row or column sum equals zero, **test** has not been performed.");
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
