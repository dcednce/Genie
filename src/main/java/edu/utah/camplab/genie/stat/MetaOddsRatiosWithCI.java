//*****************************************************************************
// MetaOddsRatiosWithCI.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class MetaOddsRatiosWithCI extends MetaOddsRatios {
  //---------------------------------------------------------------------------
  public MetaOddsRatiosWithCI() {
    super();
    infExtraStatTitle = "Empirical Confidence Intervals : ";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    compTracker = new RatiosComparisonTracker(true, true, 1.0);
    //return new ORCompTrackerCI();
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //----------------------------------------------------------------------------
  public Result getInfExtraStat(ComparisonTracker compTracker) {
    //ORCompTrackerExt ORct = (ORCompTrackerExt) compTracker;
    //ORCompTrackerCI ORct = (ORCompTrackerCI) compTracker;
    RatiosComparisonTracker rct = (RatiosComparisonTracker) compTracker;
    return rct.getConfidenceIntervals();
  }
}
