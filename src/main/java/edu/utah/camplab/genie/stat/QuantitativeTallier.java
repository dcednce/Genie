//******************************************************************************
// QuantitativeTallier.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Gtype;
import edu.utah.camplab.genie.ped.Pedigree;
import edu.utah.camplab.genie.util.Counter;

//==============================================================================
class QuantitativeTallier extends TallierImp {
  private final CCAnalysis.Table.Column[] theCols;
  private final int[] theRows;
  private final String theAtype;
  private final int repeatIndex;
  //private final int[]				pid2RowIx = new int[NPTYPES];
  //private final Map				tmGt2ColIx = new HashMap();
  private Counter[][] theSqCounters;

  //----------------------------------------------------------------------------
  QuantitativeTallier(int[] quant_ids, CCAnalysis.Table.Column[] cols,
                      String atype, int inRepeatIndex) {
    super(quant_ids.length, cols.length);
    theRows = quant_ids;
    theCols = cols;
    theAtype = atype;
    repeatIndex = inRepeatIndex;
    nRows = quant_ids.length;
    nCols = theCols.length;
    theCounters = createCounters();
    theSqCounters = createCounters();
    for (int i = 0; i < nRows; i++) {
      for (int j = 0; j < nCols; j++) {
        theSqCounters[i][j].set(0.0);
      }
    }
  }

  //----------------------------------------------------------------------------
  void sumExpressionEvent(Gtype gt, Pedigree ped, int qt, double quantval) {
    sumExpressionEvent(gt, ped, qt, quantval, null);
  }

  //----------------------------------------------------------------------------
  void sumExpressionEvent(Gtype gt, Pedigree ped, int qt,
                          double quantval, Thread p) {
    if (quantval != 0.0) {
      if (theAtype.matches("allele")) {
        for (int i = 0; i < 2; i++) {
          boolean first = ((i == 0) ? true : false);
          for (int icol = 0; icol < nCols; icol++) {
            //int value = theCols[icol].subsumesGtype(gt, first);
            if ((theCols[icol].subsumesAtype(gt, first, repeatIndex)) == 1) {
              //for (int irow = 0; irow < nRows ; irow++ )
              //{
              //  if (irow == qt )
              //  {
              //double sqval = quantval * quantval * value * value;
              double sqval = quantval * quantval;
              //theCounters[irow][icol].sum(quantval * value, value);
              theCounters[qt][icol].sum(quantval, 1);
              theSqCounters[qt][icol].sum(sqval, 1);
              //  }
              //}
              break;
            }
          }
        }
      }
      else {
        for (int icol = 0; icol < nCols; icol++) {
          if ((theCols[icol].subsumesGtype(gt, repeatIndex)) == 1) {
            //for (int irow = 0; irow < nRows ; irow++ )
            //{
            //  if (irow == qt )
            //  {
            //double sqval = quantval * quantval * value * value;
            double sqval = quantval * quantval;
            //    theCounters[irow][icol].sum(quantval * value, value);
            theCounters[qt][icol].sum(quantval, 1);
            theSqCounters[qt][icol].sum(sqval, 1);
            //  }
            //}
            break;
          }
        }
      }
    }
  }

  //----------------------------------------------------------------------------
  public CCAnalysis.Table extractTable() {
    return new QuantitativeTable(theRows, theCols, theCounters, theSqCounters);
  }
}
