//******************************************************************************
// TallierImp.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.util.Counter;

//==============================================================================
public class TallierImp {
  static int nRows;
  static int nCols;
  Counter[][] theCounters = null;

  //----------------------------------------------------------------------------
  public TallierImp(int nR, int nC) {
    nRows = nR;
    nCols = nC;
  }

  //----------------------------------------------------------------------------
  //QuantitativeTallier requires 2 sets of Counter[][]
  public static Counter[][] createCounters() {
    Counter[][] counters = new Counter[nRows][nCols];
    for (int irow = 0; irow < nRows; irow++) {
      for (int icol = 0; icol < nCols; icol++) {
        counters[irow][icol] = new Counter();
      }
    }
    return counters;
  }

  //----------------------------------------------------------------------------
  void resetTallies(int value) {
    for (int irow = 0; irow < nRows; irow++) {
      for (int icol = 0; icol < nCols; icol++) {
        theCounters[irow][icol].set(0);
      }
    }
  }

  //----------------------------------------------------------------------------
  void resetTallies(double value) {
    for (int irow = 0; irow < nRows; irow++) {
      for (int icol = 0; icol < nCols; icol++) {
        theCounters[irow][icol].set(0.0);
      }
    }
  }

  //----------------------------------------------------------------------------
  public Counter[][] getCounters() {
    return theCounters;
  }
}
