//*****************************************************************************
// CombTDT.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class CombTDT extends CCStatImp {

  //---------------------------------------------------------------------------
  public CombTDT() {
    title = "Combined TDT - Two Tailed Test (normal distribution)";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    if (!a.getType().matches("allele")) {
      if (t != null && t.getColumnCount() == 3) {
        CCAnalysis.Table.Totals btotals = t.getTotals();
        //resultAt0 = new ResultImp.Real(
        res = StatUt.combTDT(btotals.forColumns());
      }
      else {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          res.setMessages
            ("WARNING : Failed to extract mean, var or #m frm affected from Trio TDT or Sib TDT, ***test*** has not been performed.");
        }
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING : Unable to apply test to table type = " + a.getType());
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getCombTDTTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getCombTDTTable(p);
  }
}
