//*****************************************************************************
// MetaQtestOR.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
//public class MetaQtestOR extends CCStatImp 
public class MetaQtestOR extends MetaOddsRatios {
  //---------------------------------------------------------------------------
  public MetaQtestOR() {
    title = "Meta Q test for Odds Ratios";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //return new ScalarCompTrackerExt();
    //return new ORCompTrackerExt();
    compTracker = new RatiosComparisonTracker(false, false, 1.0);
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    Result res = null;
    String msg = "";
    boolean hasError = false;
    if (a.checkDistinctRefWt()) {
      if (getdata(a, t)) {
        //return new  ResultImp.RealSeries (
        res = StatUt.metaQtestOR(caseCell,
          controlCell,
          ctotal,
          refColIndex);
      }
      else {
        msg = "WARNING: no valid tables for this analysis";
        hasError = true;
      }
    }
    else {
      msg = "WARNING: Analysis Table has more than one reference column, **test** has not been performed.";
      hasError = true;
    }
    if (hasError) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages(msg);
      }
    }
    return res;
  }
}
