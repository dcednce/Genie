//*****************************************************************************
// SibTDT.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class SibTDT extends CCStatImp {
  //---------------------------------------------------------------------------
  public SibTDT() {
    title = "Sib TDT - Two Tailed Test";
    statType = "stat";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    if (!a.getType().matches("allele")) {
      if (t != null && t.getColumnCount() == 3) {
        SibTDTTable st = (SibTDTTable) t;
        CCAnalysis.Table.Totals stotals = st.getTotals();
        //resultAt0 = new ResultImp.QuartResults (
        res = StatUt.sibTDT(stotals.forColumns());
      }
      else {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          res.setMessages
            ("WARNING : Unable to calculate mean, variance or #m from affected, ***test*** has not been performed");
        }
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING : Unable to apply test to table type = " + a.getType());
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getSibTDTTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getSibTDTTable(p);
  }
}
