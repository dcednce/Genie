//*****************************************************************************
// MetaChiSquared.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class MetaChiSquared extends CCStatImp {
  //---------------------------------------------------------------------------
  public MetaChiSquared() {
    title = "Meta Chi-Squared";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    compTracker = new ScalarCompTracker();
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    if (a.checkDistinctRefWt()) {
      // make sure tableTotal is non zero 
      CCAnalysis.Table[] metaTable = getNonZeroTables(a, t, compTracker);
      int mt = metaTable.length;
      int refColIndex = a.getReferenceColumnIndex();
      Number[] observed = new Number[mt];
      Number[] caseTotal = new Number[mt];
      Number[] controlTotal = new Number[mt];
      Number[] obsTotal = new Number[mt];
      int[] nonObsTotal = new int[mt];
      Number[] tableTotal = new Number[mt];
      if (metaTable == null || metaTable.length == 0) {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          compTracker.setMessages("WARNING: All tables have no data");
        }
      }
      else if (metaTable[0].getColumnCount() > 2) {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          compTracker.setMessages
            ("WARNING: This test is designed for a 2 x 2 table only, **test** has not been performed.");
        }
      }
      else {
        for (int i = 0; i < mt; i++) {
          ContingencyTable table = (ContingencyTable) metaTable[i];
          CCAnalysis.Table.Totals totals = table.getTotals();
          observed[i] =
            (table.getRowFor(Ptype.CASE).getCells())[refColIndex];
          caseTotal[i] =
            totals.forRows()[table.ptID2Ix[Integer.parseInt((Ptype.CASE).getID())]];
          controlTotal[i] =
            totals.forRows()[table.ptID2Ix[Integer.parseInt((Ptype.CONTROL).getID())]];
          obsTotal[i] = totals.forColumns()[refColIndex];
          tableTotal[i] = totals.forTable();
          nonObsTotal[i] = tableTotal[i].intValue() - obsTotal[i].intValue();
        }
        //resultAt0 = new ResultImp.Real(
        res = StatUt.metaChiSquared(observed,
          caseTotal,
          controlTotal,
          obsTotal,
          nonObsTotal,
          tableTotal);
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        compTracker.setMessages
          ("WARNING: Analysis Table has more than one reference column, **test** has not been performed.");
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
