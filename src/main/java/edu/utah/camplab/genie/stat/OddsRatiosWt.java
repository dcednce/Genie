//*****************************************************************************
// OddsRatiosWt.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class OddsRatiosWt extends OddsRatios {
  //---------------------------------------------------------------------------
  public OddsRatiosWt() {
    title = "Odds Ratios, 2-Tailed Test";
    infExtraStatTitle = "";
  }

  //---------------------------------------------------------------------------
  public String getCompStatTitle() {
    return "";
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    Result res = null;
    if (a.checkDistinctRefWt()) {
      if (StatUt.checkRefCell(t.getRowFor(Ptype.CASE).getCells(),
        t.getRowFor(Ptype.CONTROL).getCells(),
        a.getReferenceColumnIndex())) {
        //Result res = new  ResultImp.ResultsWithCISeries (
        res = StatUt.oddsRatiosWithConfidence(
          t.getRowFor(Ptype.CASE).getCells(),
          t.getRowFor(Ptype.CONTROL).getCells(),
          a.getReferenceColumnIndex());
      }
      else {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          res.setMessages
            ("WARNING: reference cell has 0 value, **test** has not been performed.");
        }
      }
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING: Analysis Table has more than one reference column, **test** has not been performed.");
      }
    }
    return res;
  }

  //----------------------------------------------------------------------------
  public synchronized Result getObservedResult(Result obsResult,
                                               int refColIndex) {
    return obsResult;
  }

  //----------------------------------------------------------------------------
  public Result getInferentialResult(ComparisonTracker compTracker,
                                     int refColIndex) {
    return null; //new ResultManager.StringResult("");
  }

  //----------------------------------------------------------------------------
  public Result getInfExtraStat(ComparisonTracker compTracker) {
    return null;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    CCAnalysis.Table wtTable = tm.getWeightedIndexTable();
    return wtTable;
  }

  //---------------------------------------------------------------------------
  //Ryan 06-24-06 passed in thread.
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    CCAnalysis.Table wtTable = tm.getWeightedIndexTable(p);
    return wtTable;
  }
}
