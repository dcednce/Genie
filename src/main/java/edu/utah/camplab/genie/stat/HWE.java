//*****************************************************************************
// HWE.java
// Hardy-Weinbery Equilibrium 
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class HWE extends CCStatImp {
  //---------------------------------------------------------------------------
  public HWE() {
    title = "Hardy-Weinbery Equilibrium";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    //return new ScalarCompTrackerExt();
    //return new ORCompTrackerExt();
    compTracker = new RatiosComparisonTracker(false, false, 0.0);
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    CCAnalysis.Table.Totals totals = t.getTotals();
    String msg = "";
    boolean hasError = false;

    // check both row sum != zero
    double[] rowTotals = new double[2];
    rowTotals[0] = totals.forRows()[0].doubleValue();
    rowTotals[1] = totals.forRows()[1].doubleValue();
    if (rowTotals[0] != 0.0 && rowTotals[1] != 0.0) {
      if (t.getColumnCount() == 3)
      //resultAt0 = new ResultImp.RealSeries(
      {
        res = StatUt.hwe(t.getRowFor(Ptype.CASE).getCells(),
          t.getRowFor(Ptype.CONTROL).getCells(),
          rowTotals);
      }
      else {
        msg = "WARNING: analysis table requires 3 columns";
        hasError = true;
      }
    }
    else {
      msg = ("WARNING: row sum equals to zero, **test** has not been performed.");
      hasError = true;
    }
    if (hasError) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages(msg);
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public synchronized Result getObservedResult(Result obsResult,
                                               int rowIndex) {
    /*
    if ( obsResult instanceof ResultImp.StringResult )
      return obsResult;
    else
    {
      double[] r0Result = obsResult.doubleValues();
      int rows = r0Result.length; 
      String[][] resultPair = new String[rows][2];
      for ( int i = 0 ; i < rows; i++ )
      {
        resultPair[i][0] = new String("row " + (i+1));
        resultPair[i][1] = String.valueOf(r0Result[i]);
      }
      return new ResultImp.PairSeries(resultPair);
    }
    */
    return obsResult;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  //Ryan 06-24-06 passed in thread.
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
