//*****************************************************************************
// ChiSquaredTrend.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class ChiSquaredTrend extends CCStatImp {
  //---------------------------------------------------------------------------
  public ChiSquaredTrend() {
    title = "Chi-squared Trend";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    return new ScalarCompTracker();
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    //System.out.println("Chi Squared Trend" );
    Result res = null;
    CCAnalysis.Table.Totals totals = t.getTotals();
    if (StatUt.checkRowColSum(totals.forRows()[0],
      totals.forColumns(),
      totals.forTable())) {
      res = StatUt.chiSquaredTrend(
        t.getRowFor(Ptype.CASE).getCells(),
        totals.forRows()[0],
        totals.forColumns(),
        totals.forTable(),
        a.getColumnWeights());
    }
    else {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        res.setMessages
          ("WARNING: row or column sum equals zero, **test** has not been performed.");
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
