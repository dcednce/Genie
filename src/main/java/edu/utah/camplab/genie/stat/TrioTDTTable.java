//******************************************************************************
// TrioTDTTable.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.util.Counter;

//==============================================================================
public class TrioTDTTable extends TableImp {

  private final Counter[][] myCounters;
  public int cellB, cellC;
  public int myTrioCount;

  //----------------------------------------------------------------------------
  TrioTDTTable(Column[] cols, Counter[][] counters, int trio) {
    myRows = new Row[counters.length];
    myCols = cols;
    myCounters = counters;
    myTrioCount = trio;
    myTableName = "Trio TDT Table";
    myColumnHeading = "Case's GenoType";
    myRowHeading = "Parents GenoType";
    myColumnLabels = new String[cols.length];
    Number[] rtotals = new Number[myRows.length];
    Number[] ctotals = new Number[myCols.length];
    Number gtotal = new Integer(0);
    String[] myRowLabel = new String[counters.length];

    String mm = "mm";
    String Mm = "Mm";
    String MM = "MM";
    String x = " x ";

    myColumnLabels[0] = MM;
    myColumnLabels[1] = Mm;
    myColumnLabels[2] = mm;
    myRowLabel[0] = MM + x + MM;
    myRowLabel[1] = MM + x + Mm;
    myRowLabel[2] = MM + x + mm;
    myRowLabel[3] = Mm + x + Mm;
    myRowLabel[4] = Mm + x + mm;
    myRowLabel[5] = mm + x + mm;

    for (int icol = 0; icol < myCols.length; icol++) {
      ctotals[icol] = new Integer(0);
    }

    for (int irow = 0; irow < myRows.length; ++irow) {
      myRows[irow] = new RowImp(myRowLabel[irow], myCounters[irow]);
      rtotals[irow] = new Integer(0);
      for (int icol = 0; icol < myCols.length; ++icol) {
        int cell = myCounters[irow][icol].current().intValue();
        rtotals[irow] = new Integer(rtotals[irow].intValue() + cell);
        ctotals[icol] = new Integer(ctotals[icol].intValue() + cell);
        gtotal = new Integer(gtotal.intValue() + cell);
      }
    }

    myMessage = "(# case trios analyzed / total # case trios in dataset : " +
      gtotal.toString() + " / " + Integer.toString(trio) + ")";
    myTotals = new TotalsImp(rtotals, ctotals, gtotal);
  }

  //----------------------------------------------------------------------------
  // cell location has been modified back to original - Kristy 09-10-09
  public int getCellb() {
    int cellB = 0;
    if (myCols.length == 3) {
      cellB = (2 * myCounters[3][2].current().intValue() +
        myCounters[1][1].current().intValue() +
        myCounters[3][1].current().intValue() +
        myCounters[4][2].current().intValue());
    }
      /*
      cellB = ( 2 * myCounters[3][0].current().intValue() +
                    myCounters[4][1].current().intValue() +
                    myCounters[3][1].current().intValue() +
                    myCounters[1][0].current().intValue() );
      */
    return cellB;
  }

  //----------------------------------------------------------------------------
  public int getCellc() {
    int cellC = 0;
    if (myCols.length == 3) {
      cellC = (2 * myCounters[3][0].current().intValue() +
        myCounters[1][0].current().intValue() +
        myCounters[3][1].current().intValue() +
        myCounters[4][1].current().intValue());
    }
      /*
      cellC = ( 2 * myCounters[3][2].current().intValue() +
                    myCounters[4][2].current().intValue() +
                    myCounters[3][1].current().intValue() +
                    myCounters[1][1].current().intValue() );
      */
    return cellC;
  }

  //----------------------------------------------------------------------------
  public Counter[][] getCounters() {
    return myCounters;
  }

  public Column[] getColumns() {
    return myCols;
  }

  public int getTrioCount() {
    return myTrioCount;
  }
}
