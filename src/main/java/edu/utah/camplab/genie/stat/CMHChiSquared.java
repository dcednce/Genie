//*****************************************************************************
// CMHChiSquared.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.gm.Ptype;

//=============================================================================
public class CMHChiSquared extends CCStatImp {
  //---------------------------------------------------------------------------
  public CMHChiSquared() {
    title = "Cochran Mantel Haenszel Chi-Squared";
  }

  //---------------------------------------------------------------------------
  public ComparisonTracker newComparisonTracker() {
    compTracker = new ScalarCompTracker();
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return this.newComparisonTracker();
  }

  //---------------------------------------------------------------------------
  //public boolean checkColumnReq( CCAnalysis.Table t )
  //{
  //  return true;
  //}
  //
  //---------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    // Notes : our table is setup where rows and columns are opposite to  
    // CMH's table. We will reverse the row and column totals in the CMH
    // statistic 

    CCAnalysis.Table[] metaTable = getNonZeroTables(a, t, compTracker);
    if (metaTable == null || metaTable.length == 0) {
      res = resultManager.new Real(Double.NaN);
      if (allowMessages) {
        compTracker.setMessages("WARNING: no valid tables for this analysis");
      }
    }
    else {
      int mt = metaTable.length;
      Number[][] rowTotals = new Number[mt][];
      Number[][] columnTotals = new Number[mt][];
      Number[] tableTotal = new Number[mt];
      Number[][] casecells = new Number[mt][];
      Number[][] controlcells = new Number[mt][];

      for (int i = 0; i < mt; i++) {
        ContingencyTable table = (ContingencyTable) metaTable[i];
        CCAnalysis.Table.Totals totals = table.getTotals();
        rowTotals[i] = totals.forRows();
        columnTotals[i] = totals.forColumns();
        tableTotal[i] = totals.forTable();
        casecells[i] = table.getRowFor(Ptype.CASE).getCells();
        controlcells[i] = table.getRowFor(Ptype.CONTROL).getCells();
      }

      try {
        //resultAt0 = new ResultImp.Real(
        res = StatUt.cmhChiSquared(casecells,
          controlcells,
          rowTotals,
          columnTotals,
          tableTotal,
          constantJ);
      } catch (Exception e) {
        res = resultManager.new Real(Double.NaN);
        if (allowMessages) {
          compTracker.setMessages
            ("WARNING: cannot calculate var[n|H0], test has not been performed.");
        }
      }
    }
    return res;
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm) {
    return tm.getContingencyTable();
  }

  //---------------------------------------------------------------------------
  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return tm.getContingencyTable(p);
  }
}
