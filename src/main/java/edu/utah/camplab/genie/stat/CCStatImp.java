//******************************************************************************
// CCStatImp.java
//******************************************************************************
package edu.utah.camplab.genie.stat;

import edu.utah.camplab.genie.ped.Study;

import java.util.ArrayList;
import java.util.Vector;

//import java.lang.reflect.Method;

//==============================================================================
public class CCStatImp implements CCStat {
  public ComparisonTracker compTracker;
  ResultManager resultManager = new ResultManager();
  String title = null;
  String statType = "stat";
  String obsExtraStatTitle = null;
  String infExtraStatTitle = null;
  int constantJ = 0;
  Result resultAt0, resultAtX, res, obsExtraStatResult, infExtraStatResult, resultInf = null;
  Vector<Vector<Double>> infExtraSimResult;
  String message = "";
  CCAnalysis.Table newTable = null;

  public String getName() {
    return title;
  }

  public String getStatType() {
    return statType;
  }

  public String getCompStatTitle() {
    return "Empirical p-Value : ";
  }

  public ComparisonTracker newComparisonTracker() {
    return compTracker;
  }

  public ComparisonTracker newComparisonTracker(int df) {
    return compTracker;
  }

  //----------------------------------------------------------------------------
  public Result computeAt0(CCAnalysis a, CCAnalysis.Table t) {
    resultAt0 = computation(a, t, true);
    return resultAt0;
  }

  public Result computeAt0(CCAnalysis a, CCAnalysis.Table[] t) {
    resultAt0 = computation(a, t, true);
    return resultAt0;
  }

  //----------------------------------------------------------------------------
  public Result computeAtX(CCAnalysis a, CCAnalysis.Table t) {
    resultAtX = computation(a, t, false);
    return resultAtX;
  }

  public Result computeAtX(CCAnalysis a, CCAnalysis.Table[] t) {
    resultAtX = computation(a, t, false);
    return resultAtX;
  }

  //----------------------------------------------------------------------------
  //Only use Thread in SIM 
  //Ryan 06-024-07 added for overloaded method.
  //public Result computeAt0(CCAnalysis a, CCAnalysis.Table t, Thread p) 
  //{ return resultAt0; }

  //----------------------------------------------------------------------------
  public Result computation(CCAnalysis a,
                            CCAnalysis.Table t,
                            boolean allowMessages) {
    return res;
  }

  public Result computation(CCAnalysis a,
                            CCAnalysis.Table[] t,
                            boolean allowMessages) {
    return res;
  }

  //----------------------------------------------------------------------------
  public Result getObservedResult(Result resultAt0, int refCol) {
    return resultAt0;
  }

  public Result getInferentialResult(ComparisonTracker compTracker,
                                     int refCol) {
    return compTracker.getComparisonsResult();
  }

  //public Result gethitCount() ( ComparisonTracker compTracker )
  //{ return new ResultManager.Real(compTracker.gethitCount()); }

  //public Result getComparisonCount( ComparisonTracker compTracker ) 
  //{ return new ResultManager.Real(compTracker.getComparisonCount()); }

  public String getObsExtraStatTitle() {
    return obsExtraStatTitle;
  }

  public String getInfExtraStatTitle() {
    return infExtraStatTitle;
  }

  public Result getObsExtraStat(Result result) {
    return obsExtraStatResult;
  }

  public Result getInfExtraStat(ComparisonTracker compTracker) {
    return infExtraStatResult;
  }

  public Vector<Vector<Double>> getInfSimStat(ComparisonTracker compTracker) {
    return infExtraSimResult;
  }

  public CCAnalysis.Table getTable(TableMaker tm) {
    return newTable;
  }

  public CCAnalysis.Table getTable(TableMaker tm, Thread p) {
    return newTable;
  }

  //----------------------------------------------------------------------------
  public CCAnalysis.Table[] getNonZeroTables(CCAnalysis a,
                                             CCAnalysis.Table[] t,
                                             ComparisonTracker comparisonTracker) {
    //System.out.println("--- Total # of table : " + t.length);
    Study[] study = a.study;
    ArrayList<String> studyNoData = new ArrayList<String>();
    ArrayList<CCAnalysis.Table> ltable = new ArrayList<CCAnalysis.Table>();
    for (int i = 0; i < t.length; i++) {
      //System.out.print("Table : " + i );
      CCAnalysis.Table.Totals totals = t[i].getTotals();
      if (totals.forTable().intValue() == 0) {
        //System.out.print(" " + study[i].getStudyName() + " has no data");
        //System.out.println("");
        //studyNoData.add(study[i].getStudyName());
        if (i == 0) {
          ;
        }
        message = "WARNING: study has no data -";
        message += " " + study[i].getStudyName();
      }
      else {
        //System.out.print(" " + study[i].getStudyName()+ " has total : " + totals.forTable().intValue() );
        ltable.add(t[i]);
      }
    }
    //if ( !studyNoData.isEmpty() )
    //{
    //  String message = new String(); 
    //  for ( Iterator snd = studyNoData.iterator(); snd.hasNext(); )
    //  {
    //    message += snd.next() + " ";
    //  }
    //  comparisonTracker.setMessages("WARNING - study : " + message + " has no data, not included in this test");
    //}
    CCAnalysis.Table[] tables = ltable.toArray(new CCAnalysis.Table[0]);
    if (tables.length == 0) {
      System.out.println("All tables are zero ");
      message = "WARNING: All tables have no data";
      return null;
    }
    else {
      return tables;
    }
  }

  //----------------------------------------------------------------------------
  public int getDegreeOfFreedom(int ncol) {
    return ncol - 1;
  }

  //----------------------------------------------------------------------------
  public String getMessage() {
    return message;
  }
}
