//*****************************************************************************
// MetaTrioTDT.java
//*****************************************************************************
package edu.utah.camplab.genie.stat;

//=============================================================================
public class MetaTrioTDT extends TrioTDT {

  //---------------------------------------------------------------------------
  public MetaTrioTDT() {
    super();
    title = "Case Parents Meta Trio TDT (Chi-Squared Distribution)";
    statType = "metaTDT";
  }
}
