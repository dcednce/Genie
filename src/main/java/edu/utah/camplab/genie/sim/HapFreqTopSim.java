//******************************************************************************
// HapFreqTopSim.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.gm.FreqDataSet;
import edu.utah.camplab.genie.gm.Gtype;
import edu.utah.camplab.genie.util.GEException;
import edu.utah.camplab.genie.util.Randy;

//==============================================================================
public class HapFreqTopSim extends AlleleFreqTopSim {
  public static String[] defaultnames;
  public static double[] defaultcumfreq;
  protected Randy r;
  protected String[] code;
  protected FreqDataSet[] newcumfreq;

  //----------------------------------------------------------------------------
  public HapFreqTopSim() {
    super();
  }

  //----------------------------------------------------------------------------
  public void preProcessor() throws GEException {
    // hap frequency file loaded in IOManager
    /*
    for ( int i = 0; i < study.length; i++ )
    {
      HapDataLoader loader = new HapDataLoader();
      try 
      { loader.parse( study[i].getHaplotypeFile(), gdef.getAlleleFormat() ); }
      catch ( IOException e )
      { throw new GEException (e.getMessage()); }

      FreqDataSet[] freqdata = loader.getHapFreq();
      study[i].setFreqDataSet(freqdata);
    }
    **/
  }

  //----------------------------------------------------------------------------
  public void simulateFounderGenotypes(int index) throws GEException {
    for (int i = 0; i < nStudy; i++) {
      for (int j = 0; j < nFounders[i]; j++) {
        Gtype gt = newrandomGtype(i);
        founderInds[i][j].setSimulatedGtype(gt, index);
      }
    }
  }

  //----------------------------------------------------------------------------
  //Ryan overloaded to pass in cGtype to compress simulated founder gtypes
  /*
  public void simulateFounderGenotypes(int index, compressGtype cGtype, int step) throws GEException
  {
	if(step == 1){
	  if(index == 0){
	    cGtype.initiateFounderGtypes(founderInds[0].length);
	  }
      for ( int i = 0 ; i < nStudy; i++ )
      {
        for (int j = 0; j < nFounders[i]; j++ )
        {
          Gtype gt = newrandomGtype(i);
          founderInds[i][j].setSimulatedGtype(gt, 0);
          cGtype.store_tgt(gt, j, index);
        }
      }
	}
	else{
	  restoreFounderGtypes(cGtype,index);
      //restore gtypes for founders.
	}
  }

  //----------------------------------------------------------------------------
  //Ryan add 08-19-07
  public void restoreFounderGtypes(compressGtype cGtype, int index) throws GEException{
	gtBuilder.buildclean();
	for ( int j = 0; j < founderInds[0].length; j++ )
    {      
	  int[] aps = cGtype.get_tAllelePairs(j,index);
	  for(int i=0; i<aps.length; i++){
		int code = aps[i];
		//String a1 = "";
		//String a2 = "";
                byte a1, a2;
		if(code == 3){
		  //a1 = "2";
		  //a2 = "2";
                  a1 = 2;
                  a2 = 2;
		}
		else if(code == 2){
		  //a1 = "1";
		  //a2 = "2";
                  a1 = 1;
                  a2 = 2;
		}
		else if(code == 1){
		  //a1 = "2";
		  //a2 = "1";
                  a1 = 2; 
                  a2 = 1;
		}
		else{
		  //a1 = "1";
		  //a2 = "1";
                  a1 = 1;
                  a2 = 1;
		}
		gtBuilder.addAllelePair( a1, a2 );
	  }

	  double freq = cGtype.getTFreq(j,index);
      gtBuilder.addHaploFrequency(Double.valueOf(freq).doubleValue());
      Gtype gt = gtBuilder.buildNext();
      founderInds[0][j].setSimulatedGtype(gt, 0);
    }
  }
  */

  //----------------------------------------------------------------------------
  public Gtype newrandomGtype(int studyID) throws GEException {
    byte[] Hap1 = null;
    byte[] Hap2 = null;

    gtBuilder.buildclean();
    r = Randy.getInstance();
    newcumfreq = getcumfreq(studyID);
    //newnames = getnames();
    Hap1 = randomHaptype();
    Hap2 = randomHaptype();

    for (int i = 0; i < nLoci; ++i) {
      gtBuilder.addAllelePair(Hap1[i], Hap2[i]);
    }

    return gtBuilder.buildNext();
  }

  //----------------------------------------------------------------------------
  //public String[] randomHaptype ()
  public byte[] randomHaptype() {
    double randomNum = 0.0;
    randomNum = r.nextDouble();

    //int[] h = new int[nLoci];
    //byte[] h = new byte[nLoci];
    byte[] haplotype = new byte[nLoci];

    for (int j = 0; j < newcumfreq.length; j++) {
      if (randomNum < newcumfreq[j].getFrequency()) {
        haplotype = newcumfreq[j].getCode();
        break;
      }
    }
    return haplotype;

    //String ss = hapString.replaceAll("[^0-9]", " ");
    //StringTokenizer st = new StringTokenizer(ss, " " );
    //int i = 0;

    //while ( st.hasMoreTokens())
    //{
    //   h[i] = st.nextToken();
    //   i++;
    //}

    //return h;
  }

  //---------------------------------------------------------------------
  //public static void setcumfreq(double[] cf)
  //{ defaultcumfreq = cf; }

  //---------------------------------------------------------------------
  //public static void setnames(String[] n)
  //{ defaultnames = n; }

  //---------------------------------------------------------------------
  public FreqDataSet[] getcumfreq(int studyID) {
    FreqDataSet[][] freq = study[studyID].getFreqDataSet();
    FreqDataSet[] cumfreq = new FreqDataSet[freq[0].length];
    cumfreq[0] = freq[0][0];
    for (int i = 1; i < cumfreq.length; i++) {
      cumfreq[i] = new FreqDataSet(cumfreq[i - 1].getFrequency() +
        freq[0][i].getFrequency(),
        freq[0][i].getCode(),
        "HapFreqTopSim");
    }

    return cumfreq;
  }

  //---------------------------------------------------------------------
/**  public void getdbl()
 {
 Randy a = Randy.getInstance();
 double randomNum = a.nextDouble();
 }
 */
}

