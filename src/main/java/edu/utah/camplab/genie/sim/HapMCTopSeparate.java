//******************************************************************************
// HapMCTopSeparate.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.ped.Indiv;
import edu.utah.camplab.genie.ped.PedQuery;
import edu.utah.camplab.genie.prep.PhaseData;
import edu.utah.camplab.genie.util.GEException;
//Ryan 08-19-07
//import alun.genio.LinkageParameterData;

//==============================================================================
//public class HapMCTopSeparate extends HapMCTopSim
public class HapMCTopSeparate extends HapFreqTopSim {

  //----------------------------------------------------------------------------
  public HapMCTopSeparate() {
    super();
  }

  //----------------------------------------------------------------------------
  public void preProcessor() throws GEException {
    PedQuery.Predicate[] querySample = new PedQuery.Predicate[]{
      PedQuery.IS_CASE,
      PedQuery.IS_CONTROL};
    PhaseData phaseData = new PhaseData();
    phaseData.setDataSource(study,
      Indiv.GtSelector.OBS,
      gdef,
      -1,
      pInSample,
      querySample);
  }
}

