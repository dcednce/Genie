//******************************************************************************
// HapMCDropSeparate.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.hc.compressGtype;
import edu.utah.camplab.genie.ped.Indiv;
import edu.utah.camplab.genie.ped.PedQuery;
import edu.utah.camplab.genie.prep.PhaseData;
import edu.utah.camplab.genie.util.GEException;

//==============================================================================
//public class HapMCDropSeparate extends HapMCDropSim 
public class HapMCDropSeparate extends DropSim {
  PedQuery.Predicate[] querySample;

  //----------------------------------------------------------------------------
  public HapMCDropSeparate() {
    super();
    querySample = new PedQuery.Predicate[]{PedQuery.IS_CASE,
      PedQuery.IS_CONTROL};
  }

  //----------------------------------------------------------------------------
  public void simulateDescendantGenotypes(int index)
    throws GEException {
    super.simulateDescendantGenotypes(0);
    PhaseData phaseData = new PhaseData();
    phaseData.setDataSource(study, Indiv.GtSelector.SIM,
      gdef, index, null, querySample);
  }

  //----------------------------------------------------------------------------
  //Ryan 08-19-07 overloaded to pass in compressed Gtype datastructure
  public void simulateDescendantGenotypes(int index, compressGtype[] cGtypes,
                                          int step) throws GEException {
    //Ryan changed input to super.simulateDescendantGenotypes index to 0.
    super.simulateDescendantGenotypes(0);
    PhaseData phaseData = new PhaseData();
    phaseData.setDataSource(study, Indiv.GtSelector.SIM,
      gdef, index, null, querySample, cGtypes);
  }
}
