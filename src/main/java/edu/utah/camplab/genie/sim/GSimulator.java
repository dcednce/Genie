//******************************************************************************
// GSimulator.java
//******************************************************************************
package edu.utah.camplab.genie.sim;

import edu.utah.camplab.genie.app.rgen.Specification;
import edu.utah.camplab.genie.gm.GDef;
import edu.utah.camplab.genie.hc.compressGtype;
import edu.utah.camplab.genie.ped.Study;
import edu.utah.camplab.genie.util.GEException;

//Ryan

//==============================================================================
public interface GSimulator {
  public void setUserParameters(Specification spec, Study[] study);

  public void setPedData();

  public void setGDef(GDef gd);

  //public void setNumSimulatedData ( int num );
  public void preProcessor() throws GEException;

  public interface Top extends GSimulator {
    public void simulateFounderGenotypes(int index) throws GEException;

    public void simulateFounderGenotypes(int index, compressGtype cGtype, int step) throws GEException;
  }

  public interface Drop extends GSimulator {
    public void simulateDescendantGenotypes(int index) throws GEException;

    //Ryan 08-19-07 added to overload
    public void simulateDescendantGenotypes(int index, compressGtype[] study_Gtypes, int step) throws GEException;
  }
}
