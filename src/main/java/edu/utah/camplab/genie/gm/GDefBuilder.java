//******************************************************************************
// GDefBuilder.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

import edu.utah.camplab.genie.util.Ut;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//==============================================================================
public class GDefBuilder {

  private final List lGDefLoci = new ArrayList();
  public boolean charCode = false;

  //----------------------------------------------------------------------------
  public void addLocus(int id, String marker, double distance,
                       boolean ordered, int gene) {
    lGDefLoci.add(new LocusImp(id, marker, distance, ordered, gene));
  }

  //----------------------------------------------------------------------------
  public GDef build() {
    return new GDef() {
      private final List lLoci = Collections.unmodifiableList(
        (List) ((ArrayList) lGDefLoci).clone()
      );
      private final GtypeBuilder gtBuilder = new GtypeBuilder(this);
      public AlleleFormat alleleformat;
      public boolean decoderFlag;
      String pkg = Ut.pkgOf(AlleleFormat.class);

      public int getLocusCount() {
        return lLoci.size();
      }

      public Locus getLocus(int index) {
        return (Locus) lLoci.get(index);
      }

      public GtypeBuilder getGtypeBuilder() {
        return gtBuilder;
      }

      //byte b = Integer.valueOf(inString).byteValue();
      public AlleleFormat getAlleleFormat() {
        return alleleformat;
      }

      public void setAlleleFormat(AlleleFormat af) {
        alleleformat = af;
      }

      public boolean getDecoderFlag() {
        return decoderFlag;
      }

      public void setDecoderFlag(boolean decoder) {
        decoderFlag = decoder;
      }
    };
  }
}

