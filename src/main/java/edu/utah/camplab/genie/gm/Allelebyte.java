//******************************************************************************
// Allelebyte.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

//==============================================================================
public class Allelebyte extends AlleleFormat {
  public static byte missingData = (byte) 0;

  public Allelebyte() {
    super("num");
  }

  //----------------------------------------------------------------------------
  public byte convertAllele(String inString) {
    if (inString.length() == 1) {
      return Integer.valueOf(inString).byteValue();
    }
    else {
      System.out.println("Failed to read allele. Allele is more than a single character.");
      return missingData;
    }
  }

  //----------------------------------------------------------------------------
  public String toString(byte inbyte) {
    int intValue = inbyte;
    return String.valueOf(intValue);
  }

  //----------------------------------------------------------------------------
  public byte getMissingData() {
    return missingData;
  }

  //----------------------------------------------------------------------------
  public void setMissingData(String inString) {
    setMissingData(Integer.valueOf(inString).byteValue());
  }

  //----------------------------------------------------------------------------
  public void setMissingData(byte inbyte) {
    missingData = inbyte;
  }
}
