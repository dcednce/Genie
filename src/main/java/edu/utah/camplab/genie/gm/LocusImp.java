//******************************************************************************
// LocusImp.java
//******************************************************************************
package edu.utah.camplab.genie.gm;

import java.util.TreeMap;

//==============================================================================
public class LocusImp implements GDef.Locus {

  private final int myID;
  private final double myTheta;
  private final boolean allelesOrdered;
  private final int geneID;
  public String myMarker;
  private TreeMap<Byte, FreqDataSet> decoderMap = null;
  //private Frequency[] frequency;

  //----------------------------------------------------------------------------
  public LocusImp(int id,
                  String marker,
                  double distance,
                  boolean alleles_ordered,
                  int gene_id) {
    myID = id;
    //if ( marker == null )
    //  myMarker = String.valueOf(id);
    //else myMarker = marker;
    myMarker = marker;
    myTheta = distance > 0.5 ? cM2Theta(distance) : distance;
    allelesOrdered = alleles_ordered;
    geneID = gene_id;
    //frequency = null;
  }

  /// GDef.Locus implementation ///

  //----------------------------------------------------------------------------
  //public Frequency[] getFrequency()
  //{
  //  return frequency;
  //}
  //----------------------------------------------------------------------------
  private static double cM2Theta(double x) {
    return (Math.exp(-x / 50.0) - 1.0) / -2.0;
  }

  //----------------------------------------------------------------------------
  public int getID() {
    return myID;
  }

  //----------------------------------------------------------------------------
  public String getMarker() {
    return myMarker;
  }

  //----------------------------------------------------------------------------
  public void setMarker(String inMarker) {
    myMarker = inMarker;
  }

  //----------------------------------------------------------------------------
  public double getTheta() {
    return myTheta;
  }

  /// Internals ///

  //----------------------------------------------------------------------------
  public boolean alleleOrderIsSignificant() {
    return allelesOrdered;
  }

  //----------------------------------------------------------------------------
  //public void addFrequency(Frequency[] freq)
  //{
  //  frequency = freq;
  //}

  //----------------------------------------------------------------------------
  public int getGeneID() {
    return geneID;
  }

  //----------------------------------------------------------------------------
  public void addDecoderMap(TreeMap inMap) {
    decoderMap = inMap;
  }

  //----------------------------------------------------------------------------
  public TreeMap<Byte, FreqDataSet> getDecoderMap() {
    return decoderMap;
  }
}

