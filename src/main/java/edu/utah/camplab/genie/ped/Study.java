//******************************************************************************
// Study.java
//******************************************************************************
package edu.utah.camplab.genie.ped;

import alun.genio.LinkageParameterData;
import edu.utah.camplab.genie.gm.FreqDataSet;
import edu.utah.camplab.genie.util.EmpiricalRandomizer;

import java.io.File;

public class Study {
  File genotypefile, quantitativefile, haplotypefile, linkageparfile = null;
  String studyname = null;
  PedData pedData;
  FreqDataSet[][] freqdataset = null;
  EmpiricalRandomizer[] allelemap = null;
  LinkageParameterData linkageparameterdata = null;

  //---------------------------------------------------------------------------
  public Study(String stdname,
               String genofile,
               String quantfile,
               String hapfile,
               String parfile) {
    studyname = stdname;
    genotypefile = new File(genofile);
    if (quantfile != null) {
      quantitativefile = new File(quantfile);
    }
    if (hapfile != null) {
      haplotypefile = new File(hapfile);
    }
    if (parfile != null) {
      linkageparfile = new File(parfile);
    }
  }

  //---------------------------------------------------------------------------
  public String getStudyName() {
    return studyname;
  }

  //---------------------------------------------------------------------------
  public File getGenotypeFile() {
    return genotypefile;
  }

  //---------------------------------------------------------------------------
  public File getQuantitativeFile() {
    return quantitativefile;
  }

  //---------------------------------------------------------------------------
  public File getHaplotypeFile() {
    return haplotypefile;
  }

  //---------------------------------------------------------------------------
  public File getLinkageParFile() {
    return linkageparfile;
  }

  //---------------------------------------------------------------------------
  public LinkageParameterData getLinkageParameterData() {
    return linkageparameterdata;
  }

  //---------------------------------------------------------------------------
  public void setLinkageParameterData(LinkageParameterData lpd) {
    linkageparameterdata = lpd;
  }

  //---------------------------------------------------------------------------
  public void addFreqDataSet(FreqDataSet[] freq) {
    int freqlength = freqdataset.length;
    int newfreqlgth = freq.length;
    FreqDataSet[] newfreq;
  }

  //---------------------------------------------------------------------------
  public PedData getPedData() {
    return pedData;
  }

  //---------------------------------------------------------------------------
  public void setPedData(PedData ped) {
    pedData = ped;
  }

  //---------------------------------------------------------------------------
  public FreqDataSet[][] getFreqDataSet() {
    return freqdataset;
  }

  //---------------------------------------------------------------------------
  // most freq data are only [] not [][]
  public void setFreqDataSet(FreqDataSet[] freq) {
    if (freq == null) {
      System.out.println("No available frequency data");
      freqdataset[0] = new FreqDataSet[0];
      return;
    }
    freqdataset = new FreqDataSet[1][freq.length];
    freqdataset[0] = freq;
  }

  public void setFreqDataSet(FreqDataSet[][] freq) {
    freqdataset = freq;
  }

  //---------------------------------------------------------------------------
  public void setAlleleMap(EmpiricalRandomizer[] alp) {
    allelemap = alp;
  }

  //---------------------------------------------------------------------------
  public int getLocusNumAllele(int locusID) {
    return allelemap[locusID].getNumKey();
  }
}
