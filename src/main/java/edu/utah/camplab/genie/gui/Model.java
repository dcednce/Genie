package edu.utah.camplab.genie.gui;

public interface Model {
  public ColumnPattern[] getColPattern();
}
