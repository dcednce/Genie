package edu.utah.camplab.genie.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LeftPanel extends JPanel
  implements ActionListener {

  public LeftPanel() {
    setLayout(new BorderLayout());
    JButton button1 = new JButton("1");
    button1.addActionListener(this);
    add(button1);
  }

  public void actionPerformed(ActionEvent e) {
  }
}
