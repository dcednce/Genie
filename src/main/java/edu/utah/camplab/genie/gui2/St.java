package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.io.XUt;
import edu.utah.camplab.genie.util.Ut;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

//import java.util.List;

public class St extends TabImp {
  Sp sp;
  int nLocus = 0;
  String[] locusName;
  StudyMain studyMain;
  //StudyDetail firstStudy;
  boolean isNew = true;
  String title;

  public St() {
    super();
    title = "Study";
  }

  //-------------------------------------------------------------------------
  private static void createAndShowGUI(String paramsfile) {
    St st = new St();
    //st.build();
    JPanel newContentPane = st;
    newContentPane.setOpaque(true); //content panes must be opaque
    String title = newContentPane.toString();
    JFrame frame = new JFrame(title);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //Create and set up the content pane.
    frame.setContentPane(newContentPane);

    //Display the window.
    frame.pack();
    frame.setSize(new Dimension(950, 800));
    //frame.setResizable(false);
    frame.setVisible(true);
    st.parse(paramsfile);
  }

  //---------------------------------------------------------------------------
  public static void main(String[] args) {
    final String filename = args[0];
    SwingUtilities.invokeLater(
      new Runnable() {
        public void run() {
          createAndShowGUI(filename);
        }
      });
  }

  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    setLayout(new BorderLayout());
    sp = new Sp(title, inGUI);
    add(sp);
  }

  public Sp getSp() {
    return sp;
  }

  //-------------------------------------------------------------------------
  public void populate() {
    sp.populate();
  }

  //-------------------------------------------------------------------------
  public Parameters parse(String fileStr) {
    File parameterfile = new File(fileStr);
    Parameters parameters = null;
    try {
      parameters = new Parameters(parameterfile);
    } catch (Exception e) {
      System.out.println(" failed to parse parameter file " + fileStr + " " + e.getMessage());
    }
    return parameters;
  }

  //----------------------------------------------------------------------------
  public class Sp extends JPanel
    implements ActionListener,
    TreeSelectionListener {
    private final String SPKG_GUI = Ut.pkgOf(DetailPanel.class);
    protected JTree tree;
    protected DefaultTreeModel treeModel;
    protected DefaultMutableTreeNode rootNode;
    protected Detail rootPanel;
    boolean filehasheader = true;
    // all these are from treexdetail
    String title;
    JPanel treepanel, buttonPanel;
    JLayeredPane layeredPane;
    DefaultMutableTreeNode newNode = null;
    JButton addButton, removeButton, commitButton;
    MouseListener ml = new MouseAdapter() {
    };
    private String ADD_COMMAND = "add";
    private String REMOVE_COMMAND = "remove";
    private String COMMIT_COMMAND = "commit";
    private Toolkit toolkit = Toolkit.getDefaultToolkit();

    public Sp(String inTitle, GenieGUI inGUI) {
      //super(inTitle, inGUI);
      //if ( inGUI.isNew ) {
      //  StudyDetail studydetail = (StudyDetail) addAction();
      // add the treexDetail here
      //super(new GridLayout(1,0));
      title = inTitle;
      rootPanel = buildRootPanel();
      rootNode = new DefaultMutableTreeNode(rootPanel, true);
      //rootPanel.setGUI(gui);
      rootPanel.setMyNode(rootNode);
      rootPanel.setRootNode(rootNode);
      //rootPanel.setTreexDetail(this);
      treeModel = new DefaultTreeModel(rootNode);
      tree = new JTree(treeModel);
      tree.getSelectionModel().setSelectionMode(
        TreeSelectionModel.SINGLE_TREE_SELECTION);
      tree.addTreeSelectionListener(this);
      tree.addMouseListener(ml);
      tree.setShowsRootHandles(true);
      tree.setExpandsSelectedPaths(true);
      JScrollPane scrollPane = new JScrollPane(tree);

      // File List Panel
      treepanel = new JPanel();
      treepanel.setLayout(new BorderLayout());
      TitledEtched bordertitle = new TitledEtched(title + " Listing");
      treepanel.setBorder(bordertitle.title);
      buttonPanel = new JPanel();
      addButton = new JButton(ADD_COMMAND);
      removeButton = new JButton(REMOVE_COMMAND);
      commitButton = new JButton(COMMIT_COMMAND);
      addButton.setActionCommand(ADD_COMMAND);
      removeButton.setActionCommand(REMOVE_COMMAND);
      commitButton.setActionCommand(COMMIT_COMMAND);
      addButton.addActionListener(this);
      removeButton.addActionListener(this);
      commitButton.addActionListener(this);
      buttonPanel.add(addButton);
      buttonPanel.add(removeButton);
      buttonPanel.add(commitButton);

      treepanel.add(scrollPane, BorderLayout.CENTER);
      treepanel.add(buttonPanel, BorderLayout.SOUTH);

      // Detail Pane
      layeredPane = new JLayeredPane();
      layeredPane.setLayout(new BorderLayout());

      // add treepanel and layeredPane to splitpane
      JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
      splitPane.setLeftComponent(treepanel);
      splitPane.setRightComponent(layeredPane);

      add(splitPane);
    }

    //-------------------------------------------------------------------------
    public Detail buildRootPanel() {
      studyMain = new StudyMain();
      //studyMain.setGUI(gui);
      //studyMain.setTreexDetail(this);
      return (Detail) studyMain;
    }

    //-------------------------------------------------------------------------
    public StudyDetail addChild() {
      StudyDetail studydetail = new StudyDetail();
      studydetail.setParentNode(studyMain.myNode);
      studydetail.setParentPanel(studyMain);
      studydetail.setRootNode(rootNode);
      //studydetail.setTreexDetail(this);
      studydetail.build(studyMain.getChildIndex());
      studydetail.setOpaque(true);
      studydetail.setVisible(true);
      studydetail.setEnabled(true);
      studyMain.incrementChildIndex();
      DefaultMutableTreeNode node = setNode(studyMain.myNode, studydetail);
      studydetail.setMyNode(node);
      /*
      layeredPane.remove(studydetail);
      layeredPane.updateUI();
      layeredPane.validate();
      layeredPane.add(studydetail);
      System.out.println("before movetofront " + studydetail.toString());
      layeredPane.moveToFront(studydetail);
      */
      return studydetail;
    }

    //-------------------------------------------------------------------------
    public DefaultMutableTreeNode setNode(DefaultMutableTreeNode parentNode,
                                          StudyDetail studydetail) {
      DefaultMutableTreeNode childnode = new DefaultMutableTreeNode
        (studydetail, false);
      try {
        treeModel.insertNodeInto(childnode,
          parentNode,
          parentNode.getChildCount());
      } catch (Exception e) {
        System.err.println("Error adding node to tree " + e.getMessage());
      }
      tree.setModel(treeModel);
      tree.scrollPathToVisible(new TreePath(childnode.getPath()));
      tree.updateUI();
      return childnode;
    }

    //-------------------------------------------------------------------------
    public void setLocusInfo() throws IOException {
      System.out.println("setlocusinfo");
      File file = null;
      // retrieve the first genotype file from the first Study Panel
      //DetailPanelImp[] dpi = getDetailPanel();
      //DetailPanelImp[] dpi = studyMain.getChildDetail();
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)
        rootNode.getFirstChild();
      StudyDetail sd = (StudyDetail) node.getUserObject();
      file = new File(sd.getGenoFile());

      if (file == null) {
        JOptionPane.showMessageDialog(this, "No Genotype File selected");
      }
      else {
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line = in.readLine();
        if (line != null) {
          String[] tokens = line.trim().split("\\s+");

          if (filehasheader) {
            nLocus = tokens.length - 7;
            locusName = new String[nLocus];
            for (int i = 0; i < nLocus; i++) {
              locusName[i] = tokens[i + 7];
            }
          }
          else {
            nLocus = (int) ((tokens.length - 7) / 2);
            for (int i = 0; i < nLocus; i++) {
              locusName[i] = new String("Maker " + i);
            }
          }
        }
        in.close();
      }
    } // end setLocusInfo

    //-------------------------------------------------------------------------
    public int getNumLocus() {
      return nLocus;
    }

    //-------------------------------------------------------------------------
    public String[] getLocusName() {
      return locusName;
    }

    //-------------------------------------------------------------------------
    public void commitedAction() {
      System.out.println("StudyTab  - commitedAction");
      try {
        setLocusInfo();
        /*
        if ( nLocus != 0 ) {
          //gui.addTab("Locus");
          gui.locusTab.build(gui);
 	  if ( gui.isNew ) {
            setDisplayOnly();
            gui.setSelected(gui.locusTab);
  	  }
	} else  {
          JOptionPane.showMessageDialog
          (this, "Genotype file has no genotype data");
	}
        */
      } catch (Exception e) {
        JOptionPane.showMessageDialog
          (this, "Failed to read Genotype Data File");
      }
      System.out.println("StudyTab  - completed commitedAction");
    }

    //-------------------------------------------------------------------------
    public String toString() {
      return title;
    }

    //-------------------------------------------------------------------------
    public void setDisplayOnly() {
      Util.disable(addButton);
      Util.disable(removeButton);
      Util.disable(commitButton);
      //DetailPanelImp[] detailpanel = sp.getDetailPanel();
      DetailPanel[] detailpanel = studyMain.getChildDetail();
      for (int i = 0; i < detailpanel.length; i++) {
        StudyDetail sd = (StudyDetail) detailpanel[i];
        sd.setDisplayOnly();
      }
    }

    //-------------------------------------------------------------------------
    public void populate() {
      System.out.println("++++  Populate Studys +++++");
      layeredPane.removeAll();
      //Parameters params = gui.getParameters();
      Parameters params = parse("/export/home/jathine/data/smtest.rgen");
      NodeList studydata = params.getDataFile();
      int nStudyData = studydata.getLength();
      for (int i = 0; i < nStudyData; i++) {
        String stdname, genodata, hap, quant, par = null;
        Element edata = (Element) studydata.item(i);
        try {
          stdname = XUt.stringAtt(edata, Parameters.Att.STUDYNAME);
          genodata = XUt.stringAtt(edata, Parameters.Att.GENOTYPEDATA);
          hap = XUt.stringAtt(edata, Parameters.Att.HAPLOTYPE);
          quant = XUt.stringAtt(edata, Parameters.Att.QUANTITATIVE);
          par = XUt.stringAtt(edata, Parameters.Att.LINKAGEPARAMETER);
          System.out.println(" Check in 1");
          if (genodata != null && genodata.length() > 0) {
            //StudyDetail studydetail = (StudyDetail) addAction(rootNode);
            StudyDetail studydetail = (StudyDetail) addChild();
            studydetail.genofileT.setText(genodata);
            if (stdname != null && stdname.length() > 0) {
              studydetail.studynameT.setText(stdname);
            }
            if (hap != null && hap.length() > 0) {
              studydetail.freqfileT.setText(hap);
            }
            if (quant != null && quant.length() > 0) {
              studydetail.covariatefileT.setText(quant);
            }
            if (par != null && par.length() > 0) {
              studydetail.linkageparfileT.setText(par);
            }
            System.out.println(" Check in 2");
            studydetail.repaint();
            studydetail.updateUI();
            studydetail.validate();
            layeredPane.add(studydetail);
            System.out.println(" Check in 3");
            sp.updateUI();
            sp.validate();
            System.out.println(" Check in 4");
            displaySelectedScreen((DetailPanel) studydetail);
          }
        } catch (Exception e) {
          System.out.println("1 Failed to read file " + e.getMessage());
          System.exit(0);
        }
      }
      System.out.println("++++  completed Populate Studys +++++");
      commitedAction();
    }

    //---------------------------------------------------------------------------
    public void actionPerformed(ActionEvent e) {
      String cmd = e.getActionCommand();
      actionPerformed(cmd);
    }

    public void actionPerformed(String cmd) {
      if (ADD_COMMAND.equals(cmd)) {
        System.out.println(" ---  pressed the add Button");
        Detail screen = addChild();
        System.out.println(" --- completed press add button");
      }
      else if (REMOVE_COMMAND.equals(cmd)) {
        //DefaultMutableTreeNode selectedNode = getCurrentNode();
        //if ( selectedNode != rootNode) {
        //  removeCurrentNode();
        //  DefaultMutableTreeNode nextNode = (DefaultMutableTreeNode)
        //                         rootNode.getFirstChild();
        //  displaySelectedScreen(nextNode);
        //} else
        //  JOptionPane.showMessageDialog(this, "Cannot remove this last Component!");
      }
      else if (COMMIT_COMMAND.equals(cmd)) {
        commitedAction();
      }
    }

    //---------------------------------------------------------------------------
    public void valueChanged(TreeSelectionEvent tse) {
      DefaultMutableTreeNode node = getCurrentNode();
      if (node == null) {
        node = (DefaultMutableTreeNode) rootNode.getFirstChild();
      }
      //DetailPanel panel = (DetailPanel) node.getUserObject();
      displaySelectedScreen(node);
    }

    //---------------------------------------------------------------------------

    /**
     * return current selected node.
     */
    public DefaultMutableTreeNode getCurrentNode() {
      DefaultMutableTreeNode currentNode = null;
      TreePath currentPath = tree.getSelectionPath();
      if (currentPath != null) {
        currentNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        String currenttitle = currentNode.getUserObject().toString();
      }
      else {
        currentNode = rootNode;
      }
      return currentNode;
    }

    //-------------------------------------------------------------------------
    public void displaySelectedScreen(DefaultMutableTreeNode selectedNode) {
      if (selectedNode == rootNode) {
        selectedNode = (DefaultMutableTreeNode) rootNode.getFirstChild();
      }
      DetailPanel panel = (DetailPanel) selectedNode.getUserObject();
      displaySelectedScreen(panel);
    }

    public void displaySelectedScreen(DetailPanel panel) {
      panel.setOpaque(true);
      //layeredPane.remove(panel);
      //layeredPane.add(panel);
      layeredPane.updateUI();
      System.out.println(" Check in 5");
      layeredPane.validate();
      System.out.println(" Check in 6");
      layeredPane.moveToFront(panel);
      System.out.println(" Check in 7");
      if (panel.isShowing()) {
        System.out.println("displaySelectedScreen " + panel.toString() + " visible ");
      }
      else {
        System.out.println("displaySelectedScreen " + panel.toString() + " is invisible");
      }
    }
  } // end sp
}
