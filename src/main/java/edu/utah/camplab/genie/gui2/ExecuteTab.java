package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.app.rgen.MainManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class ExecuteTab extends TabImp
  implements ActionListener {
  static JTextArea outputArea = null;
  ParameterFile rgenfile = null;
  ButtonGroup runBG;
  JRadioButton fileOnlyB, executeB;
  JFileChooser filechooser;
  JLabel fileL, executeMessageL;
  OptionTab optionTab;
  GlobalTab globalTab;
  StudyTab studyTab;
  StatisticTab statisticTab;
  LocusTab locusTab;
  AnalysisTab analysisTab;
  BufferedReader err = null;
  BufferedReader in = null;

  public ExecuteTab() {
    super();
    title = "Execute";
  }

  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    optionTab = gui.optionTab;
    globalTab = gui.globalTab;
    studyTab = gui.studyTab;
    statisticTab = gui.statisticTab;
    locusTab = gui.locusTab;
    analysisTab = gui.analysisTab;
    fileOnlyB = new JRadioButton("Save Parameter File only");
    fileOnlyB.addActionListener(this);
    fileOnlyB.setSelected(false);
    fileL = new JLabel("");
    executeB = new JRadioButton("Save Parameter File and Run Genie");
    executeB.addActionListener(this);
    executeB.setSelected(false);
    executeMessageL = new JLabel("");
    runBG = new ButtonGroup();
    runBG.add(fileOnlyB);
    runBG.add(executeB);
    JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
    splitPane.setDividerLocation(100);
    JPanel topPanel = new JPanel();
    TitledEtched titledEtched = new TitledEtched("Exeute Genie");
    topPanel.setBorder(titledEtched.title);
    topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
    topPanel.add(Box.createRigidArea(new Dimension(10, 0)));
    topPanel.add(fileOnlyB);
    topPanel.add(executeB);
    JScrollPane bottomPane = new JScrollPane();
    {
      outputArea = new JTextArea();
      outputArea.setEditable(false);
      //outputArea.setPreferredSize(new Dimension(875,350));
      PrintStream out = new PrintStream(new TextAreaOutputStream(outputArea));
      System.setOut(out);
      System.setErr(out);
      bottomPane.setViewportView(outputArea);
    }
    bottomPane.setPreferredSize(new Dimension(900, 400));
    splitPane.setTopComponent(topPanel);
    splitPane.setBottomComponent(bottomPane);
    splitPane.setOneTouchExpandable(true);
    splitPane.setDividerSize(15);
    add(splitPane, BorderLayout.CENTER);
  }

  public ParameterFile saveFile()
  //throws IOException
  {
    filechooser = new JFileChooser();
    filechooser.setDialogTitle("Save .rgen parameter file");
    int returnVal = filechooser.showSaveDialog(ExecuteTab.this);
    ParameterFile pf = null;

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = filechooser.getSelectedFile();
      try {
        pf = new ParameterFile(file,
          globalTab,
          studyTab,
          statisticTab,
          locusTab,
          analysisTab);
      } catch (IOException ioe) {
        System.out.println("Failed to create parameter file. See following details. Restart GUI with corrected input.");
        JOptionPane.showMessageDialog(this, "Failed to create parameter file. See detail panel error message. Restart GUI with corrected input.");
      }
    }
    return pf;
  }

  public void actionPerformed(ActionEvent ae) {
    Object source = ae.getSource();
    if (source == fileOnlyB) {
      if ((rgenfile = saveFile()) != null) {
        System.out.println("Parameter file : " + rgenfile.getName() + " saved");
      }
      else {
        JOptionPane.showMessageDialog(this, UIConstants.NO_FILE_SAVED);
      }
    }
    else if (source == executeB) {
      if ((rgenfile = saveFile()) != null) {
        String pkg = gui.getPackageName();
        MainManager mm = new MainManager(new String[]{});
        mm.executeGenie(new String[]{pkg, rgenfile.getName()});
      } // end if rgenfile != null 
      else {
        JOptionPane.showMessageDialog(this, UIConstants.NO_FILE_SAVED);
      }
    }
  }
}
