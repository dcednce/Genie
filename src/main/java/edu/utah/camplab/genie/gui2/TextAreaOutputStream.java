package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.io.OutputStream;

public class TextAreaOutputStream extends OutputStream {
  protected JTextArea textArea;

  public TextAreaOutputStream(JTextArea ta) {
    textArea = ta;
  }

  public void write(int i) {
    textArea.append((char) i + "");
  }
}
