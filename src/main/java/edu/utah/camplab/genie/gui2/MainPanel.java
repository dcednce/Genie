package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainPanel extends JPanel
  implements ActionListener {

  public MainPanel() {
    setLayout(new BorderLayout());
    JButton button2 = new JButton("2");
    button2.addActionListener(this);
    add(button2);
  }

  public void displayPanel(String panelName) {
    try {
      this.setVisible(false);
      Class panelClass = DetailPanel.class.forName("edu.utah.camplab.genie.gui2" + "." + panelName);
      ((DetailPanel) panelClass.newInstance()).setVisible(true);
    } catch (ClassNotFoundException cnf) {
      cnf.printStackTrace();
    } catch (InstantiationException ie) {
      ie.printStackTrace();
    } catch (IllegalAccessException iae) {
      iae.printStackTrace();
    }
  }

  public void actionPerformed(ActionEvent e) {
  }
}
