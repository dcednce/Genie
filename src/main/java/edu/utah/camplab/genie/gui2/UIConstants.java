package edu.utah.camplab.genie.gui2;

public class UIConstants {
  public static final String BROWSE = "Browse";
  public static final String NOFILESELECTED = "No File Selected";
  public static final String REPEAT_MESSAGE = "Divide all selected loci into groups. Group size is the value in the number of Locus field. Each group will be analyzed with the column group definition and the selected statistics.\nFor example, 5 loci selected and number of locus is set to 2.\nRepeatLoci : there will be 2 groups, locus 1 & 2, and locus 3 & 4, with the last locus not analysed.\nRepeatSlidingWindow : there will be 4 groups, locus 1 & 2, locus 2 & 3, locus 3 & 4, and locus 4 & 5.";
  public static final String WEIGHT_MESSAGE = "Weight value for this column. Column with the lowest weight value is the reference column.";
  public static final String NO_FILE_SAVED = "No file saved";
  public static int SCREEN_WIDTH = 900;
  public static int SCREEN_HEIGHT = 700;
  public static int TEXT_WIDTH = 60;
  public static int TEXT_HEIGHT = 25;

  public UIConstants() {
  }
}
