package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//==============================================================================
public class StatisticTab extends TabImp
  implements ActionListener {
  String title = "Statistics";
  StatisticDetail statisticDetail;
  MetaDetail metaDetail;
  JButton commitB;

  //----------------------------------------------------------------------------
  public StatisticTab() {
    super();
    title = "Statistic";
  }

  //----------------------------------------------------------------------------
  public void setTitle(String inTitle) {
    title = inTitle;
  }

  //----------------------------------------------------------------------------
  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    commitB = new JButton("Commit and build Analysis");
    commitB.addActionListener(this);
    JPanel statisticMain = new JPanel();
    statisticMain.setLayout(new BorderLayout());
    statisticDetail = new StatisticDetail();
    metaDetail = new MetaDetail();
    if (statisticDetail != null && metaDetail != null) {
      statisticDetail.build();
      statisticDetail.setOpaque(true);
      metaDetail.build();
      metaDetail.setOpaque(true);
      statisticMain.add(statisticDetail, BorderLayout.NORTH);
      statisticMain.add(metaDetail, BorderLayout.CENTER);
      statisticMain.add(commitB, BorderLayout.PAGE_END);
      statisticMain.updateUI();
    }
    else {
      JOptionPane.showMessageDialog
        (this,
          "Failed to create Statistics Detail");
    }
    add(statisticMain);
  }

  //----------------------------------------------------------------------------
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source == commitB) {
      int nStats = (statisticDetail.getSelectedStat()).length;
      if (nStats > 0) {
        setDisplayOnly();
        //gui.addTab("Analysis");
        gui.analysisTab.build(gui);
        gui.setSelected(gui.analysisTab);
      }
      else {
        JOptionPane.showMessageDialog
          (this,
            "Select Statistic for Analysis");
      }
    }
  }

  //----------------------------------------------------------------------------
  public void setDisplayOnly() {
    statisticDetail.setDisplayOnly();
    metaDetail.setDisplayOnly();
    Util.disable(commitB);
  }

  //----------------------------------------------------------------------------
  //Quit the application.
  protected void quit() {
    System.exit(0);
  }

  //----------------------------------------------------------------------------
  public StatisticDetail getStatisticDetail() {

    return statisticDetail;
  }

  //----------------------------------------------------------------------------
  public MetaDetail getMetaDetail() {
    return metaDetail;
  }

  //----------------------------------------------------------------------------
  public void populate() {
    Parameters params = gui.getParameters();
    String[] stats = (String[]) params.getCCStats().toArray(new String[0]);
    String[] metas = (String[]) params.getMetaStats().toArray(new String[0]);
    statisticDetail.populate(stats);
    metaDetail.populate(metas);
    statisticDetail.updateUI();
    metaDetail.updateUI();
  }
}
