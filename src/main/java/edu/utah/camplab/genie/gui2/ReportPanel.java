package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//=============================================================================
public class ReportPanel extends BasicPanel
  implements ActionListener {
  JRadioButton detailRB, summaryRB, bothRB;
  ButtonGroup reportBgroup;

  public ReportPanel() {
    super("Report");
    detailRB = new JRadioButton("Detail Report", true);
    summaryRB = new JRadioButton("Summary Report");
    bothRB = new JRadioButton("Summary and Detail Reports");
    detailRB.setActionCommand("report");
    summaryRB.setActionCommand("summary");
    bothRB.setActionCommand("both");
    reportBgroup = new ButtonGroup();
    reportBgroup.add(detailRB);
    reportBgroup.add(summaryRB);
    reportBgroup.add(bothRB);
    add(detailRB);
    add(summaryRB);
    add(bothRB);
    detailRB.addActionListener(this);
    summaryRB.addActionListener(this);
    bothRB.addActionListener(this);
  }

  public void actionPerformed(ActionEvent e) {
  }

  public String getReport() {
    return reportBgroup.getSelection().getActionCommand();
  }

  public void populate(Parameters params) {
    String report = params.get(Parameters.Att.REPORT);
    if (report.equals("summary")) {
      summaryRB.setSelected(true);
    }
    else if (report.equals("both")) {
      bothRB.setSelected(true);
    }
  }
}
