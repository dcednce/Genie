package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.io.ResourceResolver;
import edu.utah.camplab.genie.io.XLoader;
import edu.utah.camplab.genie.io.XUt;
import edu.utah.camplab.genie.util.GEException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;

import java.io.File;
import java.util.*;

//=============================================================================

/**
 * Read in Content of the parameter file and create a map for all the variables
 * This file should be share with Specification
 */

public class Parameters {
  private static final EntityResolver DTD_RESOLVER = new ResourceResolver(
    "ge-rgen.dtd");
  private static final String OPT_DISABLER = ".";
  protected final String nsURI;
  private final Map<String, String> mParams = new TreeMap<String, String>();
  private final List<String> lStats = new ArrayList<String>();
  private final List<String> lMetas = new ArrayList<String>();
  //private List<String>          lAnalyses  = new ArrayList<CCAnalysis>();
  private final List<String> lcovarIDs = new ArrayList<String>();
  private final List<String> ltabletype = new ArrayList<String>();
  private Map<String, Object> hapC_params = new HashMap<String, Object>();
  private NodeList datafilelist;
  private NodeList locuslist;
  private NodeList analysislist;
  private Element edoc;
  //----------------------------------------------------------------------------
  public Parameters(File f) throws GEException {
    edoc = new XLoader().loadDocumentElement(f, DTD_RESOLVER);
    nsURI = edoc.getNamespaceURI();
    mParams.putAll(getGlobalAttributes(edoc));
    mParams.putAll(getOptionalParameters(kidsOf(edoc, Tag.PARAM)));
    mParams.putAll(getLocusParameters(kidsOf(edoc, Tag.LOCUS)));
    organizeParameters();
  }

  //----------------------------------------------------------------------------
  private Map<String, String> getGlobalAttributes(Element doc_elem) {
    Map<String, String> m = new HashMap<String, String>();
    NamedNodeMap atts = doc_elem.getAttributes();

    for (int i = 0; i < atts.getLength(); ++i) {
      m.put(atts.item(i).getNodeName(), atts.item(i).getNodeValue());
    }
    m.remove("xmlns" + ":" + doc_elem.getPrefix());
    return m;
  }

  //----------------------------------------------------------------------------
  private Map<String, String> getOptionalParameters(NodeList nlparams) {
    Map<String, String> m = new HashMap<String, String>();
    for (int i = 0; i < nlparams.getLength(); ++i) {
      Element e = (Element) nlparams.item(i);
      String value = e.getFirstChild().getNodeValue().trim();
      if (!value.startsWith(OPT_DISABLER)) {
        m.put(e.getAttribute(Att.NAME), value);
      }
    }
    return m;
  }

  //----------------------------------------------------------------------------
  private Map<String, String> getLocusParameters(NodeList nlparams) {
    Map<String, String> m = new HashMap<String, String>();
    for (int i = 0; i < nlparams.getLength(); ++i) {
      Element e = (Element) nlparams.item(i);
      if (e.getAttribute(Att.MARKER).length() == 0) {
        m.put("locus " + (i + 1), e.getAttribute(Att.ID));
      }
      else {
        m.put("locus " + (i + 1), e.getAttribute(Att.MARKER));
      }
    }
    return m;
  }

  //---------------------------------------------------------------------------
  public Map<String, String> getAll() {
    return mParams;
  }

  public String get(String inStr) {
    Map m = Collections.unmodifiableMap(mParams);
    return (String) m.get(inStr);
  }

  public Object getHapCParameter(String inStr) {
    return null;
  }

  public List<String> getCCStats() {
    return lStats;
  }

  public List<String> getMetaStats() {
    return lMetas;
  }

  public List<String> getCovar() {
    return lcovarIDs;
  }

  public NodeList getDataFile() {
    return datafilelist;
  }

  public NodeList getLocus() {
    return locuslist;
  }

  public NodeList getAnalysis() {
    return analysislist;
  }

  //----------------------------------------------------------------------------
  public NodeList kidsOf(Element e, String kid_elements_name) {
    return XUt.kidsOf(e, kid_elements_name, nsURI);
  }

  //---------------------------------------------------------------------------
  public void organizeParameters() {
    for (Iterator<String> it = mParams.keySet().iterator(); it.hasNext(); ) {
      String key = it.next().toLowerCase();
      if (key.startsWith(PName.CCSTAT)) {
        lStats.add(mParams.get(key));
      }
      else if (key.startsWith(PName.METASTAT)) {
        lMetas.add(mParams.get(key));
      }
      else if (key.startsWith(PName.COVAR)) {
        lcovarIDs.add(mParams.get(key));
      }
      else if (key.startsWith(PName.TABLETYPE)) {
        ltabletype.add(mParams.get(key));
      }
    }

    datafilelist = kidsOf(edoc, Tag.DATAFILE);
    locuslist = kidsOf(edoc, Tag.LOCUS);
    analysislist = kidsOf(edoc, Tag.CCTABLE);
  }

  public interface Tag {
    String DATAFILE = "datafile";
    String LOCUS = "locus";
    String PARAM = "param";
    String CCTABLE = "cctable";
    String COL = "col";
    String G = "g";
    String A = "a";
  }

  public interface Att {
    String NS = "xmlns:ge";
    String NSIMS = "nsims";
    String RSEED = "rseed";
    String TOP = "top";
    String DROP = "drop";
    String REPORT = "report";
    String PERCENT = "percent";
    String NAME = "name";
    String ID = "id";
    String DIST = "dist";
    String MARKER = "marker";
    String ORDERED = "ordered";
    String GENE = "gene";
    String LOCI = "loci";
    String REPEAT = "repeat";
    String STATS = "stats";
    String METAS = "metas";
    String MODEL = "model";
    String TABLES = "tables";
    String TYPE = "type";
    String WT = "wt";
    String STUDYNAME = "studyname";
    String GENOTYPEDATA = "genotypedata";
    String QUANTITATIVE = "quantitative";
    String HAPLOTYPE = "haplotype";
    String LINKAGEPARAMETER = "linkageparameter";
    String ALLELEFORMAT = "alleleformat";
    String MISSINGDATA = "missingdata";
    String DISTRIBUTION = "distribution";
    String DECODEFILE = "decodefile";
    String DECODE = "decode";
    String PRINTFREQ = "printfreq";
    //String SCIENTIFICNOTATION = "scientificnotation";
    String MAXFRACTIONDIGITS = "maxfractiondigits";
    String INTERVALCHECK = "intervalcheck";
  }

  public interface PName {
    String CCSTAT = "ccstat";
    String DUMPER = "dumper";
    String METASTAT = "metastat";
    //String QUANTFILE = "quantfile";
    String COVAR = "covar";
    String TABLETYPE = "tabletype";
    String HAPC = "hapc";
  }
}
