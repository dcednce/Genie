package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;

public class BasicPanel extends JPanel {
  PackagePanel parentPanel;

  public BasicPanel(String titleName) {
    super();
    TitledEtched titledEtched = new TitledEtched(titleName);
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(300, 90));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
  }

  protected static ImageIcon createImageIcon(String path,
                                             String description) {
    java.net.URL imgURL = GlobalTab.class.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }

  public void setParentPanel(JPanel parent) {
    parentPanel = (PackagePanel) parent;
  }
}
