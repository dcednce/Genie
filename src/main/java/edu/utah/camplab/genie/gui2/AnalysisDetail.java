package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.gm.LocusImp;
import edu.utah.camplab.genie.io.XUt;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//==============================================================================
public class AnalysisDetail extends DetailPanel
  implements ActionListener {
  String[] type;
  ButtonGroup typeBG, locusBG, statsBG, repeatBG;
  JRadioButton genotypeB, alleleB, allStatsB, getStatsB,
    repeatLociB, repeatSlidingWB, noActionB;
  JLabel locusL, statL, typeL, modelL, numLocusL, repeatL;
  JTextField numLocusT, modelT;
  JButton typeHelpB, repeatHelpB, locusHelpB, statHelpB, infoCommitB, statsCommitB, locusCommitB;
  JTabbedPane tabbedPane;
  JPanel infoPanel;
  JScrollPane scrollLocus;
  LocusDetail subsetLocusDetail;
  StatisticDetail subsetStatDetail;
  MetaDetail subsetMetaDetail;
  int paneCount = 0;
  String title = "AnalysisDetail";

  public AnalysisDetail() {
    super();
    childName = "Column";
    allowsChildren = true;
  }

  public void build(int inIndex) {
    super.build(inIndex);
    setPreferredSize(new Dimension(600, 400));
    setLayout(new BorderLayout());
    TitledEtched titledEtched = new TitledEtched(title + " " + getIndex());
    setBorder(titledEtched.title);

    tabbedPane = new JTabbedPane();
    JPanel panel = buildSubsetStatisticPanel();
    scrollLocus = new JScrollPane(buildSubsetLocusPanel());
    infoPanel = buildCCtableInfoPanel();
    tabbedPane.addTab("Subset Statistics", panel);
    tabbedPane.addTab("Subset Locus", scrollLocus);
    tabbedPane.addTab("Analysis Detail", infoPanel);
    add(tabbedPane);
  } //end buildDetail()

  //----------------------------------------------------------------------------
  // infoPanel 
  public JPanel buildCCtableInfoPanel() {
    String nLocus = "";
    if (subsetLocusDetail != null) {
      nLocus = Integer.toString(getSubsetLocusCount());
    }
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    infoPanel = new JPanel();
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints constraints = new GridBagConstraints();
    infoPanel.setLayout(gridbag);
    typeL = new JLabel("Analysis Type");
    genotypeB = new JRadioButton("Genotype");
    genotypeB.setActionCommand("Genotype");
    genotypeB.setSelected(true);
    genotypeB.addActionListener(this);
    alleleB = new JRadioButton("Allelic");
    alleleB.setActionCommand("Allele");
    alleleB.addActionListener(this);
    typeBG = new ButtonGroup();
    typeBG.add(genotypeB);
    typeBG.add(alleleB);
    modelL = new JLabel("Model Name ");
    modelT = new JTextField("", 15);
    modelT.setPreferredSize(new Dimension(15, 4));
    repeatL = new JLabel("Repeat Analysis Method");
    //noActionB = new JRadioButton("No Repeat Method");
    //noActionB.setSelected(true);
    //noActionB.setActionCommand("NoAction");
    //noActionB.addActionListener(this);
    repeatLociB = new JRadioButton("RepeatLoci");
    repeatLociB.setActionCommand("RepeatLoci");
    repeatLociB.addActionListener(this);
    repeatSlidingWB = new JRadioButton("RepeatSlidingWindow");
    repeatSlidingWB.setActionCommand("RepeatSlidingWindow");
    repeatSlidingWB.addActionListener(this);
    repeatBG = new ButtonGroup();
    //repeatBG.add(noActionB);
    repeatBG.add(repeatLociB);
    repeatBG.add(repeatSlidingWB);
    numLocusL = new JLabel("Number of Locus ");
    //numLocusT = new JTextField(Integer.toString(getSubsetLocusCount()), 15);
    numLocusT = new JTextField(nLocus, 15);
    numLocusT.setPreferredSize(new Dimension(15, 4));
    typeHelpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif",
      "type help button"));
    typeHelpB.setContentAreaFilled(false);
    typeHelpB.setBorderPainted(false);
    typeHelpB.setHorizontalAlignment(JLabel.LEFT);
    typeHelpB.addActionListener(this);
    typeHelpB.setActionCommand("typeHelp");
    repeatHelpB = new JButton(createImageIcon("/toolbarButtonGraphics/general/Help24.gif",
      "type help button"));
    repeatHelpB.setContentAreaFilled(false);
    repeatHelpB.setBorderPainted(false);
    repeatHelpB.setHorizontalAlignment(JLabel.LEFT);
    repeatHelpB.addActionListener(this);
    repeatHelpB.setActionCommand("repeatHelp");
    infoCommitB = new JButton("Build Column Weight");
    infoCommitB.addActionListener(this);
    infoCommitB.setActionCommand("infoCommit");

    //constraints.gridwidth = GridBagConstraints.REMAINDER;
    //constraints.anchor = GridBagConstraints.LINE_START;
    constraints.ipadx = 10;
    constraints.ipady = 10;
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.gridwidth = 20;
    constraints.gridheight = 2;
    constraints.anchor = GridBagConstraints.WEST;
    gridbag.setConstraints(typeL, constraints);
    constraints.gridx = 30;
    gridbag.setConstraints(genotypeB, constraints);
    constraints.gridx = 55;
    gridbag.setConstraints(alleleB, constraints);
    constraints.gridx = 80;
    gridbag.setConstraints(typeHelpB, constraints);
    constraints.gridy += 20;
    constraints.gridx = 0;
    gridbag.setConstraints(repeatL, constraints);
    constraints.gridx = 30;
    gridbag.setConstraints(repeatLociB, constraints);
    //gridbag.setConstraints(noActionB, constraints);
    constraints.gridx = 55;
    gridbag.setConstraints(repeatSlidingWB, constraints);
    constraints.gridx = 80;
    gridbag.setConstraints(repeatHelpB, constraints);
    constraints.ipady = 15;
    constraints.gridy += 30;
    constraints.gridx = 0;
    gridbag.setConstraints(modelL, constraints);
    constraints.gridx = 40;
    gridbag.setConstraints(modelT, constraints);
    constraints.gridy += 40;
    constraints.gridx = 0;
    gridbag.setConstraints(numLocusL, constraints);
    constraints.gridx = 40;
    gridbag.setConstraints(numLocusT, constraints);

    infoPanel.add(typeL);
    infoPanel.add(genotypeB);
    infoPanel.add(alleleB);
    infoPanel.add(typeHelpB);
    infoPanel.add(modelL);
    infoPanel.add(modelT);
    infoPanel.add(repeatL);
    infoPanel.add(repeatLociB);
    infoPanel.add(repeatSlidingWB);
    infoPanel.add(repeatHelpB);
    infoPanel.add(numLocusL);
    infoPanel.add(numLocusT);

    panel.add(infoPanel, BorderLayout.CENTER);
    panel.add(infoCommitB, BorderLayout.SOUTH);

    return panel;
  }

  //----------------------------------------------------------------------------
  public JPanel buildSubsetLocusPanel() {
    // subset locus panel
    JPanel panel = new JPanel();
    panel.setOpaque(true);
    subsetLocusDetail = buildSubsetLocusDetail();
    subsetLocusDetail.setOpaque(true);
    panel.setLayout(new BorderLayout());
    locusCommitB = new JButton("Commit and Build Next");
    locusCommitB.addActionListener(this);
    locusCommitB.setActionCommand("locusCommit");
    panel.add(subsetLocusDetail, BorderLayout.CENTER);
    panel.add(locusCommitB, BorderLayout.PAGE_END);
    //scrollLocus = new JScrollPane(locusPanel);
    return panel;
  }

  //--------------------------------------------------------------------------
  public LocusDetail buildSubsetLocusDetail() {
    GenieGUI gui = ((AnalysisMain) rootNode.getUserObject()).getGUI();
    LocusTab locustab = (LocusTab) gui.locusTab;
    String messageText2 = new String("No Locus info");
    if (locustab != null) {
      LocusImp[] locus = locustab.getSelectedLocus();
      int nlocus = locus.length;
      if (nlocus > 0) {
        String[] locusName = new String[nlocus];
        String[] locusDist = new String[nlocus];
        String[] gene = new String[nlocus];
        for (int i = 0; i < nlocus; i++) {
          locusName[i] = locus[i].getMarker();
          locusDist[i] = String.valueOf(locus[i].getTheta());
          gene[i] = String.valueOf(locus[i].getGeneID());
        }

        LocusDetail panel = new LocusDetail
          ("Subset Locus",
            nlocus,
            locusName,
            locusDist,
            gene);
        return panel;
      }
      else {
        JOptionPane.showMessageDialog(this, messageText2);
      }
    }
    else {
      JOptionPane.showMessageDialog(this, messageText2);
    }
    return null;
  } //end buildSubsetLocus

  //----------------------------------------------------------------------------
  public JPanel buildSubsetStatisticPanel() {
    // subset Statistic panel
    AnalysisMain aMain = (AnalysisMain) rootNode.getUserObject();
    GenieGUI gui = aMain.getGUI();
    StatisticTab stattab = (StatisticTab) gui.statisticTab;
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());
    panel.setOpaque(true);
    subsetStatDetail = buildSubsetStatisticDetail(stattab);
    subsetStatDetail.setOpaque(true);
    subsetMetaDetail = buildSubsetMetaDetail(stattab);
    subsetMetaDetail.setOpaque(true);
    statsCommitB = new JButton("Commit and Build Subset Locus");
    statsCommitB.addActionListener(this);
    statsCommitB.setActionCommand("statsCommit");
    panel.add(subsetStatDetail, BorderLayout.NORTH);
    panel.add(subsetMetaDetail, BorderLayout.CENTER);
    panel.add(statsCommitB, BorderLayout.SOUTH);
    return panel;
  }

  //--------------------------------------------------------------------------
  public StatisticDetail buildSubsetStatisticDetail(StatisticTab stattab) {
    StatisticDetail statisticdetail = new StatisticDetail();
    StatisticDetail sdetail = stattab.getStatisticDetail();
    if (sdetail != null) {
      Statistic[] stats = sdetail.getSelectedStat();
      statisticdetail.setStatistic(stats);
      statisticdetail.build();
    }
    return statisticdetail;
  }

  //--------------------------------------------------------------------------
  public MetaDetail buildSubsetMetaDetail(StatisticTab stattab) {
    MetaDetail metadetail = new MetaDetail();
    MetaDetail mdetail = stattab.getMetaDetail();
    if (mdetail != null) {
      Statistic[] stats = mdetail.getSelectedStat();
      if (stats.length > 0) {
        metadetail.setStatistic(stats);
        metadetail.build();
      }
    }
    return metadetail;
  }

  //--------------------------------------------------------------------------
  public int getnumLocus() {
    return new Integer(numLocusT.getText()).intValue();
  }

  public int getSubsetLocusCount() {
    return subsetLocusDetail.getSelectedLocus().length;
  }

  //************************************************************************
  public void actionPerformed(ActionEvent ae) {
    GenieGUI gui = ((AnalysisMain) rootNode.getUserObject()).getGUI();
    Object source = ae.getSource();
    if (source == typeHelpB) {
      JOptionPane.showMessageDialog(this,
        "Type of analyze, Genotype or Allele");
    }
    else if (source == repeatHelpB) {
      JOptionPane.showMessageDialog(this, UIConstants.REPEAT_MESSAGE);
    }
    else if (source == statsCommitB) {
      subsetStatDetail.setDisplayOnly();
      subsetMetaDetail.setDisplayOnly();
      statsCommitB.setVisible(false);
      //scrollLocus = new JScrollPane(buildSubsetLocusPanel());
      //tabbedPane.addTab("Subset Locus", scrollLocus);
      tabbedPane.setSelectedComponent(scrollLocus);
    }
    else if (source == locusCommitB) {
      //infoPanel = buildCCtableInfoPanel();
      //tabbedPane.addTab("Analysis Detail", infoPanel);
      numLocusT.setText(Integer.toString(getSubsetLocusCount()));
      subsetLocusDetail.setDisplayOnly();
      locusCommitB.setVisible(false);
      tabbedPane.setSelectedComponent(infoPanel);
    }
    else if (source == infoCommitB) {
      //DetailPanelImp columndetail = createChildPanel(this.myNode);
      //ColumnDetail columndetail = null;
      try {
        System.out.println(" ------------------1");
        DetailPanel parentPanel = (DetailPanel) parentNode.getUserObject();
        System.out.println("treexdetail " + parentPanel.title);
        Detail detail = parentPanel.treexdetail.addAction(myNode);
        parentPanel.validate();
        System.out.println(" ------------------1");
        //buildChild();
        //columndetail = (ColumnDetail) createChildPanel(childName );
        //columndetail.setOpaque(true);
      } catch (Exception ee) {
        System.err.println("Error creating Child Panel : " + ee.getMessage());
      }
      disableinfoPanel();
      //treexdetailpanel.layeredPane.add(columndetail,columndetail.getIndex());
      //treexdetailpanel.layeredPane.validate();
      //treexdetailpanel.displaySelectedScreen(columndetail);
    }
    if (source == repeatLociB) {
      numLocusT.setText("1");
    }
  } // actionperformed

  //------------------------------------------------------------------------
  public void disableinfoPanel() {
    Util.disable(typeBG);
    Util.disable(repeatBG);
    Util.disable(modelT);
    Util.disable(numLocusT);
    Util.disable(typeHelpB);
    Util.disable(infoCommitB);
  }

  //------------------------------------------------------------------------
  public void updateNumLocusT(int value) {
    if (getnumLocus() > value) {
      numLocusT.setText(Integer.toString(value));
    }
  }

  //public boolean getFlag()
  //{
  //  return this.locusCommitB.isVisible();
  //}

  public String getAnalysisType() {
    return typeBG.getSelection().getActionCommand();
  }

  public String getModelName() {
    return modelT.getText();
  }

  public String getRepeatMethod() {
    ButtonModel bm = repeatBG.getSelection();
    if (bm == null) {
      return "";
    }
    else {
      return bm.getActionCommand();
    }
  }

  //---------------------------------------------------------------------------
  public String toString() {
    return "Detail " + myIndex;
  }

  //---------------------------------------------------------------------------
  public void populateLocus(int[] inLocus) {
    subsetLocusDetail.populate(inLocus);
  }

  //---------------------------------------------------------------------------
  public void populateStatistic(int[] inStat) {
    subsetStatDetail.populate(inStat);
  }

  //---------------------------------------------------------------------------
  public void populateMetaStat(int[] inMeta) {
    subsetMetaDetail.populate(inMeta);
  }

  //---------------------------------------------------------------------------
  public void populateInfoPanel(String model,
                                String type,
                                String repeat,
                                int groupsize) {
    modelT.setText(model);
    String genoStr = genotypeB.getActionCommand();
    if (!genoStr.equals(type)) {
      genotypeB.setSelected(false);
      alleleB.setSelected(true);
    }
    //noActionB.setSelected(false);
    repeatLociB.setSelected(false);
    repeatSlidingWB.setSelected(false);
    String repeatStr = repeatLociB.getActionCommand();
    if (repeat.length() != 0) {
      if (repeatStr.equals(repeat)) {
        repeatLociB.setSelected(true);
      }
      else {
        repeatSlidingWB.setSelected(true);
      }
    }
    numLocusT.setText("" + groupsize);
    infoPanel.updateUI();
  }

  //---------------------------------------------------------------------------
  public void populateColumn(Parameters params, NodeList colList) {
    int nCols = colList.getLength();
    for (int i = 0; i < nCols; i++) {
      ColumnDetail columnDetail = (ColumnDetail) treexdetail.addAction(myNode);
      Element ecol = (Element) colList.item(i);
      int wt = 0;
      try {
        wt = XUt.intAtt(ecol, Parameters.Att.WT);
      } catch (Exception e) {
        System.out.println("Failed to read Weight value " + e.getMessage());
        System.exit(0);
      }
      NodeList groupList = params.kidsOf(ecol, Parameters.Tag.G);
      columnDetail.populate(params, wt, groupList);
    }
  }
} // end AnalysisDetail 

