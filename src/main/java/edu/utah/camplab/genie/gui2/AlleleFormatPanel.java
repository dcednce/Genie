package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//=============================================================================
public class AlleleFormatPanel extends BasicPanel
  implements ActionListener {
  ButtonGroup formatBgroup;
  JRadioButton numericRB, characterRB;

  public AlleleFormatPanel() {
    super("Allele Format");
    numericRB = new JRadioButton("Numeric", true);
    characterRB = new JRadioButton("Character");
    numericRB.setActionCommand("numeric");
    characterRB.setActionCommand("character");
    formatBgroup = new ButtonGroup();
    formatBgroup.add(numericRB);
    formatBgroup.add(characterRB);
    numericRB.addActionListener(this);
    characterRB.addActionListener(this);
    add(numericRB);
    add(characterRB);
  }

  public void actionPerformed(ActionEvent e) {
  }

  public String getAlleleFormat() {
    return formatBgroup.getSelection().getActionCommand();
  }

  public void populate(Parameters params) {
    String actionCommand = params.get(Parameters.Att.ALLELEFORMAT);
    if (actionCommand.equals("character")) {
      characterRB.setSelected(true);
    }
  }
}
