package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.util.Ut;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.Enumeration;
import java.util.TreeMap;

public class DetailPanelImp extends DetailPanel {
  private static final String SPKG_GUI = Ut.pkgOf(DetailPanel.class);
  String title = null;
  boolean allowsChildren;
  DefaultMutableTreeNode myNode, parentNode, rootNode;
  TreexDetailPanel treexdetailpanel;
  int index;
  DetailPanelImp[] list;
  String childName;
  int childCount = 0;

  public DetailPanelImp() {
  }

  public void setRootNode(DefaultMutableTreeNode node) {
    rootNode = node;
  }

  public void setParentNode(DefaultMutableTreeNode node) {
    parentNode = node;
  }

  public void setMyNode(DefaultMutableTreeNode node) {
    myNode = node;
  }

  public void setTreexDetailPanel(TreexDetailPanel treexdetail) {
    treexdetailpanel = treexdetail;
  }

  public void buildDetail(int index) {
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String inTitle) {
    title = inTitle;
  }

  public String toString() {
    return title + " " + index;
  }

  //---------------------------------------------------------------------------
  public void buildChild() throws Exception {
    //childPaneCount++;
    String suffix = new String("Detail");
    System.out.println("Create Child Panel name " + childName + " suffix " + suffix);
    Object newPanel = Ut.newModule(SPKG_GUI, childName, suffix);
    DetailPanelImp childDetail = (DetailPanelImp) newPanel;
    childDetail.setTreexDetailPanel(treexdetailpanel);
    //childDetail.buildDetail(childPaneCount);
    int index = getChildCount();
    childDetail.buildDetail(index);
    DefaultMutableTreeNode childNode = new DefaultMutableTreeNode
      (childDetail,
        childDetail.getAllowsChildren());
    childDetail.setMyNode(childNode);
    childDetail.setParentNode(myNode);
    childDetail.setOpaque(true);
    childDetail.setVisible(true);
    treexdetailpanel.layeredPane.add(childDetail, childDetail.getIndex());
    treexdetailpanel.layeredPane.validate();
    treexdetailpanel.displaySelectedScreen(childDetail);
    DefaultMutableTreeNode node = treexdetailpanel.addNode(myNode, childNode);
    incrementChildCount();
    //return childDetail;
  }

  //---------------------------------------------------------------------------
  public void incrementChildCount() {
    childCount++;
  }

  public int getChildCount() {
    return childCount;
  }

  public int getIndex() {
    return index;
  }

  public boolean getFlag() {
    return true;
  }

  public boolean getAllowsChildren() {
    return allowsChildren;
  }

  //---------------------------------------------------------------------------
  public void setAllowsChildren(boolean inBoolean) {
    allowsChildren = inBoolean;
  }

  public String getChildName() {
    return childName;
  }

  //---------------------------------------------------------------------------
  public TreeMap<DefaultMutableTreeNode, DetailPanelImp>
  getDetailPanel() {
    DefaultMutableTreeNode node = treexdetailpanel.rootNode;
    return getDetailPanel(node);
  }

  public TreeMap<DefaultMutableTreeNode, DetailPanelImp>
  getDetailPanel(DefaultMutableTreeNode inNode) {
    TreeMap<DefaultMutableTreeNode, DetailPanelImp> detailnode =
      new TreeMap<DefaultMutableTreeNode, DetailPanelImp>();
    for (Enumeration e = inNode.children(); e.hasMoreElements(); ) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)
        e.nextElement();
      detailnode.put(node, (DetailPanelImp) node.getUserObject());
    }
    return detailnode;
  }

  //---------------------------------------------------------------------------
  public DetailPanelImp[] getChildDetail() {
    list = new DetailPanelImp[myNode.getChildCount()];
    int i = 0;
    for (Enumeration e = myNode.children(); e.hasMoreElements(); ) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
      list[i] = (DetailPanelImp) node.getUserObject();
      i++;
    }
    return list;
  }

  //---------------------------------------------------------------------------
  public ImageIcon createImageIcon(String path,
                                   String description) {
    java.net.URL imgURL = getClass().getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }
}
