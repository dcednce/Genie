package edu.utah.camplab.genie.gui2;

import edu.utah.camplab.genie.app.rgen.MainManager;
import edu.utah.camplab.genie.util.Ut;

import javax.swing.*;
import java.awt.*;

//==============================================================================
public class GenieGUI extends JPanel {
  JTabbedPane tabbedPane;
  String pkg, packageName, title;
  TabImp[] tabs;
  Parameters parameters = null;
  boolean isNew = true;
  OptionTab optionTab = new OptionTab();
  ;
  GlobalTab globalTab = new GlobalTab();
  StudyTab studyTab = new StudyTab();
  LocusTab locusTab = new LocusTab();
  StatisticTab statisticTab = new StatisticTab();
  AnalysisTab analysisTab = new AnalysisTab();
  ExecuteTab executeTab = new ExecuteTab();

  //---------------------------------------------------------------------------
  public GenieGUI() {
    title = "Genie " + MainManager.application_version;
    pkg = Ut.pkgOf(this);
    tabbedPane = new JTabbedPane();
    //addTab("Option");
    createTabs();

    //Add the tabbed pane to this panel.
    add(tabbedPane);

    //The following line enables to use scrolling tabs.
    tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
  }

  /**
   * Create the GUI and show it.  For thread safety,
   * this method should be invoked from the
   * event dispatch thread.
   */
  private static void createAndShowGUI() {
    //Create and set up the window.
    JPanel newContentPane = new GenieGUI();
    newContentPane.setOpaque(true); //content panes must be opaque
    String title = newContentPane.toString();
    JFrame frame = new JFrame(title);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    //Create and set up the content pane.
    frame.setContentPane(newContentPane);

    //Display the window.
    frame.pack();
    frame.setSize(new Dimension(950, 800));
    //frame.setResizable(false);
    frame.setVisible(true);
  }

  //---------------------------------------------------------------------------
  public static void main(String[] args) {
    //Turn off metal's use of bold fonts
    //UIManager.put("swing.boldMetal", Boolean.FALSE);
    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.
    SwingUtilities.invokeLater(
      new Runnable() {
        public void run() {
          createAndShowGUI();
        }
      });
  }

  //---------------------------------------------------------------------------
  public String toString() {
    return title;
  }

  //---------------------------------------------------------------------------
  public void createTabs() {
    String suffix = "Tab";
    tabs = new TabImp[7];
    // Add Option Tab
    optionTab.build(this);
    optionTab.updateUI();
    tabbedPane.addTab("Option", optionTab);
    tabbedPane.addTab("Global", globalTab);
    tabbedPane.addTab("Study", studyTab);
    tabbedPane.addTab("Locus", locusTab);
    tabbedPane.addTab("Statistic", statisticTab);
    tabbedPane.addTab("Analysis", analysisTab);
    tabbedPane.addTab("Execute", executeTab);
    tabbedPane.setSelectedComponent(optionTab);
    tabbedPane.updateUI();
    tabs[0] = optionTab;
    tabs[1] = globalTab;
    tabs[2] = studyTab;
    tabs[3] = locusTab;
    tabs[4] = statisticTab;
    tabs[5] = analysisTab;
    tabs[6] = executeTab;
  }

  //---------------------------------------------------------------------------
  public void addTab(String tabname) {
    try {
      String suffix = "Tab";
      String pkg = Ut.pkgOf(this);
      TabImp t = (TabImp) Ut.newModule(pkg, tabname, suffix);
      t.build(this);
      if (!isNew) {
        t.populate();
        t.updateUI();
      }
      int nt = 0;
      if (tabs != null) {
        nt = tabs.length;
      }
      TabImp[] newTabs = new TabImp[nt + 1];
      for (int i = 0; i < nt; i++) {
        newTabs[i] = tabs[i];
      }
      newTabs[nt] = t;
      tabs = newTabs;
      tabbedPane.insertTab(tabname, null, t, "aa", nt);
      if (isNew) {
        tabbedPane.setSelectedComponent(t);
      }
      tabbedPane.updateUI();
    } catch (Exception e) {
      System.out.println("Error creating new Tab for panel : " + tabname + " " + e.getMessage());
      System.exit(0);
    }
  }

  //---------------------------------------------------------------------------
  public TabImp getTab(String tabname) {
    TabImp tab = null;
    for (TabImp t : tabs) {
      if (t.toString().equals(tabname)) {
        tab = t;
        break;
      }
    }
    return tab;
  }

  //---------------------------------------------------------------------------
  public Parameters getParameters() {
    return parameters;
  }

  //---------------------------------------------------------------------------
  public void setParameters(Parameters params) {
    parameters = params;
    isNew = false;
  }

  //---------------------------------------------------------------------------
  public String getPackageName() {
    return packageName;
  }

  //---------------------------------------------------------------------------
  public void setPackageName(String inStr) {
    packageName = inStr;
  }

  //---------------------------------------------------------------------------
  public void setChoice(boolean isNew) {
    this.isNew = isNew;
  }

  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  public void populate() {
    int ntabs = tabs.length - 1;
    for (int i = 1; i < 6; i++) {
      tabs[i].build(this);
      tabs[i].populate();
      tabbedPane.setSelectedComponent(tabs[0]);
    }
  }

  //---------------------------------------------------------------------------
  public void setSelected(TabImp inTab) {
    tabbedPane.setSelectedComponent(inTab);
    tabbedPane.updateUI();
  }
}
