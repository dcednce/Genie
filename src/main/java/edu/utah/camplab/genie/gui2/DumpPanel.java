package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//=============================================================================
public class DumpPanel extends JPanel
  implements ActionListener {
  JRadioButton nodumpB, genodumpB, haplodumpB, tdtdumpB, descentdumpB;
  ButtonGroup dumpBgroup;

  public DumpPanel() {
    TitledEtched titledEtched = new TitledEtched("Dump Data");
    setBorder(titledEtched.title);
    setLayout(new GridLayout(5, 1));
    nodumpB = new JRadioButton("no dumping", true);
    genodumpB = new JRadioButton("genotype data pre-makeped file");
    haplodumpB = new JRadioButton("haplotype with frequency");
    tdtdumpB = new JRadioButton("genotype with variable for TDT input");
    descentdumpB = new JRadioButton("genotype data in html format");
    dumpBgroup = new ButtonGroup();
    dumpBgroup.add(nodumpB);
    dumpBgroup.add(genodumpB);
    dumpBgroup.add(haplodumpB);
    dumpBgroup.add(tdtdumpB);
    dumpBgroup.add(descentdumpB);
    nodumpB.addActionListener(this);
    genodumpB.addActionListener(this);
    haplodumpB.addActionListener(this);
    tdtdumpB.addActionListener(this);
    descentdumpB.addActionListener(this);
    add(nodumpB);
    add(genodumpB);
    add(tdtdumpB);
    add(descentdumpB);
    add(haplodumpB);
    haplodumpB.setVisible(false);
  }

  public void updateDisplay(boolean isHapMC) {
    haplodumpB.setVisible(isHapMC);
    updateUI();
  }

  public void actionPerformed(ActionEvent ae) {
  }

  public String getDump() {
    return dumpBgroup.getSelection().getActionCommand();
  }
}
