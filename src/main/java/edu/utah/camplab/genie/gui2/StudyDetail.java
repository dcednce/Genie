package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.ArrayList;
//import java.util.Enumeration;

public class StudyDetail extends DetailPanel
  implements ActionListener {
  JFileChooser filechooser;
  JButton studynameB, genofileB, freqfileB, covariatefileB, linkageparfileB;
  JLabel studynameL, genofileL, freqfileL, covariatefileL, linkageparfileL;
  JTextField studynameT, genofileT, freqfileT, covariatefileT, linkageparfileT;
  File genofile, freqfile, covariatefile, linkageparfile;
  String title = "Study Detail";

  public StudyDetail() {
    super();
    allowsChildren = false;
  }

  public void build(int inIndex) {
    super.build(inIndex);
    System.out.println("2 -- build StudyDetail " + inIndex);
    GridBagLayout gridbag = new GridBagLayout();
    GridBagConstraints constraintsL = new GridBagConstraints();
    GridBagConstraints constraintsT = new GridBagConstraints();
    GridBagConstraints constraintsB = new GridBagConstraints();
    setLayout(gridbag);
    setPreferredSize(new Dimension(600, 600));
    //setLayout(new SpringLayout());

    TitledEtched etchedtitle = new TitledEtched(title + " " + inIndex);
    setBorder(etchedtitle.title);
    studynameL = new JLabel("Study Name", JLabel.TRAILING);
    studynameT = new JTextField("Study_" + myIndex + "  ", 35);
    studynameT.setPreferredSize(new Dimension(35, 5));
    studynameL.setLabelFor(studynameT);
    studynameB = new JButton(UIConstants.BROWSE);
    studynameB.setVisible(false);
    studynameB.setEnabled(false);
    genofileL = new JLabel("Genotype Data File", JLabel.TRAILING);
    genofileT = new JTextField(UIConstants.NOFILESELECTED, 35);
    genofileT.setPreferredSize(new Dimension(35, 5));
    genofileL.setLabelFor(genofileT);
    genofileB = new JButton(UIConstants.BROWSE);
    freqfileL = new JLabel("Frequency File", JLabel.TRAILING);
    freqfileT = new JTextField(UIConstants.NOFILESELECTED, 35);
    freqfileT.setPreferredSize(new Dimension(35, 5));
    freqfileL.setLabelFor(freqfileT);
    freqfileB = new JButton(UIConstants.BROWSE);
    covariatefileL = new JLabel("Covariate File ", JLabel.TRAILING);
    covariatefileT = new JTextField(UIConstants.NOFILESELECTED, 35);
    covariatefileL.setLabelFor(covariatefileT);
    covariatefileT.setPreferredSize(new Dimension(35, 5));
    covariatefileB = new JButton(UIConstants.BROWSE);
    linkageparfileL = new JLabel("Linkage Parameter File", JLabel.TRAILING);
    linkageparfileT = new JTextField(UIConstants.NOFILESELECTED, 35);
    linkageparfileT.setPreferredSize(new Dimension(35, 5));
    linkageparfileL.setLabelFor(linkageparfileT);
    linkageparfileB = new JButton(UIConstants.BROWSE);

    // add listeners
    genofileB.addActionListener(this);
    freqfileB.addActionListener(this);
    covariatefileB.addActionListener(this);
    linkageparfileB.addActionListener(this);

    genofile = null;
    freqfile = null;
    covariatefile = null;
    linkageparfile = null;

        /*
        // create array for grouplayout
        JLabel[] labels = { studynameL, 
			    genofileL, 
			    freqfileL, 
			    covariatefileL,
			    linkageparfileL};
        JTextField[] textfields = { studynameT,
				    genofileT,
				    freqfileT,
				    covariatefileT,
				    linkageparfileT };
        JButton[] buttons = { studynameB,
			      genofileB,
  			      freqfileB,
			      covariatefileB,
			      linkageparfileB };

        for ( int i = 0; i < 5; i++ ) {
	  add(labels[i]);
	  add(textfields[i]);
	  add(buttons[i]);
        }
	SpringUtilities.makeCompactGrid( this, 5, 3, 16, 16, 16, 16);
        revalidate();
        repaint();
	// constraints for filetype button
        */
    int gridyVal = 0;
    constraintsL.ipadx = 0;
    constraintsL.ipady = 0;
    constraintsL.gridx = 0;
    constraintsL.gridy = 0;
    constraintsL.gridwidth = 1;
    constraintsL.insets = new Insets(5, 5, 5, 5);
    //constraintsL.gridheight = 1;
    constraintsT.ipadx = 250;
    //constraintsT.ipady = 0;
    constraintsT.gridx = 3;
    constraintsT.gridwidth = 1;
    constraintsT.insets = new Insets(5, 5, 5, 5);
    //constraintsT.gridheight = 10;
    constraintsB.ipadx = 0;
    constraintsB.ipady = 0;
    constraintsB.gridx = 8;
    constraintsB.gridwidth = 1;
    constraintsB.insets = new Insets(5, 5, 5, 5);
    //constraintsB.gridheight = 10;

    gridbag.setConstraints(studynameL, constraintsL);
    gridbag.setConstraints(studynameT, constraintsT);
    add(studynameL);
    add(studynameT);
    add(studynameB);

    gridyVal++;
    constraintsL.gridy = gridyVal;
    constraintsT.gridy = gridyVal;
    constraintsB.gridy = gridyVal;
    gridbag.setConstraints(genofileL, constraintsL);
    gridbag.setConstraints(genofileT, constraintsT);
    gridbag.setConstraints(genofileB, constraintsB);
    add(genofileL);
    add(genofileT);
    add(genofileB);

    gridyVal++;
    constraintsL.gridy = gridyVal;
    constraintsT.gridy = gridyVal;
    constraintsB.gridy = gridyVal;
    gridbag.setConstraints(freqfileL, constraintsL);
    gridbag.setConstraints(freqfileT, constraintsT);
    gridbag.setConstraints(freqfileB, constraintsB);
    add(freqfileL);
    add(freqfileT);
    add(freqfileB);

    gridyVal++;
    constraintsL.gridy = gridyVal;
    constraintsT.gridy = gridyVal;
    constraintsB.gridy = gridyVal;
    gridbag.setConstraints(covariatefileL, constraintsL);
    gridbag.setConstraints(covariatefileT, constraintsT);
    gridbag.setConstraints(covariatefileB, constraintsB);
    add(covariatefileL);
    add(covariatefileT);
    add(covariatefileB);

    gridyVal++;
    constraintsL.gridy = gridyVal;
    constraintsT.gridy = gridyVal;
    constraintsB.gridy = gridyVal;
    gridbag.setConstraints(linkageparfileL, constraintsL);
    gridbag.setConstraints(linkageparfileT, constraintsT);
    gridbag.setConstraints(linkageparfileB, constraintsB);
    add(linkageparfileL);
    add(linkageparfileT);
    add(linkageparfileB);
    this.setOpaque(true);
  } // filedetail constructor

  //---------------------------------------------------------------------------
  public void actionPerformed(ActionEvent ae) {
    Object source = ae.getSource();
    if (source == genofileB) {
      filechooser = new JFileChooser();
      filechooser.setDialogTitle("Genotype Data File");
      int returnVal = filechooser.showOpenDialog(StudyDetail.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        genofile = filechooser.getSelectedFile();
        genofileT.setText(genofile.toString());
      }
    }
    else if (source == freqfileB) {
      filechooser = new JFileChooser();
      filechooser.setDialogTitle("Frequency File");
      int returnVal = filechooser.showOpenDialog(StudyDetail.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        freqfile = filechooser.getSelectedFile();
        freqfileT.setText(freqfile.toString());
      }
    }
    else if (source == covariatefileB) {
      filechooser = new JFileChooser();
      filechooser.setDialogTitle("Variable File");
      int returnVal = filechooser.showOpenDialog(StudyDetail.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        covariatefile = filechooser.getSelectedFile();
        covariatefileT.setText(covariatefile.toString());
      }
    }
    else if (source == linkageparfileB) {
      filechooser = new JFileChooser();
      filechooser.setDialogTitle("Linkage Parameter File");
      int returnVal = filechooser.showOpenDialog(StudyDetail.this);

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        linkageparfile = filechooser.getSelectedFile();
        linkageparfileT.setText(linkageparfile.toString());
      }
    }
  }  // actionPerformed

  //----------------------------------------------------------------------------
  public String toString() {
    if (studynameT == null) {
      return title;
    }
    else {
      return studynameT.getText();
    }
  }

  public String getStudyName() {
    return studynameT.getText();
  }

  public String getGenoFile() {
    return genofileT.getText();
  }

  public String getHapFreq() {
    return freqfileT.getText();
  }

  public String getVariableFile() {
    return covariatefileT.getText();
  }

  public String getTitle() {
    return title;
  }

  public String getLinkageParFile() {
    return linkageparfileT.getText();
  }

  public void setDisplayOnly() {
    Util.disable(studynameT);
    Util.disable(genofileT);
    Util.disable(freqfileT);
    Util.disable(covariatefileT);
    Util.disable(linkageparfileT);
    Util.disable(genofileB);
    Util.disable(freqfileB);
    Util.disable(covariatefileB);
    Util.disable(linkageparfileB);
  }
} 
