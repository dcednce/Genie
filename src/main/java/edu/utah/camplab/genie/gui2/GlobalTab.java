package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//=============================================================================
public class GlobalTab extends TabImp {

  GenieGUI gui;
  VariablesPanel variablesPanel = new VariablesPanel();
  AlleleFormatPanel alleleFormatPanel = new AlleleFormatPanel();
  MissingAllelePanel missingAllelePanel = new MissingAllelePanel();
  TopSimPanel topSimPanel = new TopSimPanel();
  SamplingPanel samplingPanel = new SamplingPanel();
  ReportPanel reportPanel = new ReportPanel();
  DecodePanel decodePanel = new DecodePanel();
  DumpPanel dumpPanel = new DumpPanel();
  IntervalPanel intervalPanel = new IntervalPanel();
  String packageName;

  //----------------------------------------------------------------------------
  public GlobalTab() {
    super();
    title = "Global";
  }

  //----------------------------------------------------------------------------
  protected static ImageIcon createImageIcon(String path,
                                             String description) {
    java.net.URL imgURL = GlobalTab.class.getResource(path);
    if (imgURL != null) {
      return new ImageIcon(imgURL, description);
    }
    else {
      System.err.println("Couldn't find file: " + path);
      return null;
    }
  }

  public void build(GenieGUI inGUI) {
    super.build(inGUI);
    gui = inGUI;
    setLayout(new VerticalLayout());
    GlobalPanel globalPanel = new GlobalPanel();
    add(globalPanel);
  }

  //----------------------------------------------------------------------------
  public VariablesPanel getVariablesPanel() {
    return variablesPanel;
  }

  //----------------------------------------------------------------------------
  public AlleleFormatPanel getAlleleFormatPanel() {
    return alleleFormatPanel;
  }

  //----------------------------------------------------------------------------
  public MissingAllelePanel getMissingAllelePanel() {
    return missingAllelePanel;
  }

  //----------------------------------------------------------------------------
  public TopSimPanel getTopSimPanel() {
    return topSimPanel;
  }

  //----------------------------------------------------------------------------
  public SamplingPanel getSamplingPanel() {
    return samplingPanel;
  }

  //----------------------------------------------------------------------------
  public ReportPanel getReportPanel() {
    return reportPanel;
  }

  //----------------------------------------------------------------------------
  public DumpPanel getDumpPanel() {
    return dumpPanel;
  }

  //----------------------------------------------------------------------------
  public DecodePanel getDecodePanel() {
    return decodePanel;
  }

  //----------------------------------------------------------------------------
  public IntervalPanel getIntervalPanel() {
    return intervalPanel;
  }

  //----------------------------------------------------------------------------
  public String getNsim() {
    return variablesPanel.numSimT.getText();
  }

  //----------------------------------------------------------------------------
  public String getRseed() {
    return variablesPanel.rSeedT.getText();
  }

  //----------------------------------------------------------------------------
  public String getAlleleFormat() {
    return alleleFormatPanel.formatBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getMissingAllele() {
    return missingAllelePanel.mAlleleT.getText();
  }

  //----------------------------------------------------------------------------
  public String getTop() {
    //if ( packageName.equals("hapMC") ) 
    //  return new String ("HapMCTopTogether");
    //else
    return topSimPanel.topBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getDrop() {
    if (packageName.equals("hapMC")) {
      return new String("HapMCDropTogether");
    }
    else if (getTop().equals("XTopSim")) {
      return new String("XDropSim");
    }
    else {
      return new String("DropSim");
    }
  }

  //----------------------------------------------------------------------------
  public String getReport() {
    return reportPanel.reportBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getDump() {
    return dumpPanel.dumpBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getDecode() {
    return decodePanel.decodeBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getDecodeFile() {
    return decodePanel.decodefilePanel.decodefileT.getText();
  }

  //----------------------------------------------------------------------------
  public String getInterval() {
    return intervalPanel.intervalBgroup.getSelection().getActionCommand();
  }

  //----------------------------------------------------------------------------
  public String getPackage() {
    return packageName;
  }

  //----------------------------------------------------------------------------
  public void populate() {
    Parameters parameters = gui.getParameters();
    variablesPanel.populate(parameters);
    alleleFormatPanel.populate(parameters);
    missingAllelePanel.populate(parameters);
    topSimPanel.populate(parameters);
    samplingPanel.populate(parameters);
    reportPanel.populate(parameters);
    decodePanel.populate(parameters);
    intervalPanel.populate(parameters);
  }

  //---------------------------------------------------------------------------
  public class GlobalPanel extends JPanel
    implements ActionListener {
    JPanel infoPanel;
    JButton commitB;

    public GlobalPanel() {
      super();
      setPreferredSize(new Dimension(UIConstants.SCREEN_WIDTH,
        UIConstants.SCREEN_HEIGHT));
      setLayout(new BorderLayout());
      setOpaque(true);
      infoPanel = new JPanel();
      infoPanel.setLayout(new GridLayout(5, 2));
      infoPanel.setOpaque(true);
      commitB = new JButton("Commit and Build Study Tab");
      commitB.addActionListener(this);
      decodePanel.setParentPanel(this);
      infoPanel.add(variablesPanel);
      //OptionTab optiontab = (OptionTab) gui.getTab("Option");
      //PackagePanel packagePanel = optiontab.getPackagePanel();
      packageName = gui.getPackageName();
      //packageName = packagePanel.getPackage();
      if (packageName.equals("hapMC")) {
        topSimPanel.setHapMC();
      }
      infoPanel.add(topSimPanel);
      infoPanel.add(alleleFormatPanel);
      infoPanel.add(missingAllelePanel);
      infoPanel.add(samplingPanel);
      infoPanel.add(intervalPanel);
      infoPanel.add(reportPanel);
      infoPanel.add(dumpPanel);
      infoPanel.add(decodePanel);
      add(infoPanel, BorderLayout.CENTER);
      add(commitB, BorderLayout.SOUTH);
    }

    //--------------------------------------------------------------------------
    public void actionPerformed(ActionEvent ae) {
      Object source = ae.getSource();
      if (source == commitB) {
        setDisplayOnly();
        //gui.addTab("Study");
        gui.studyTab.build(gui);
        gui.setSelected(gui.studyTab);
      }
    }

    //--------------------------------------------------------------------------
    public void setDisplayOnly() {
      Util.disable(commitB);
      Util.disable(variablesPanel.rSeedT);
      Util.disable(variablesPanel.numSimT);
      Util.disable(samplingPanel.samplingBgroup);
      Util.disable(reportPanel.reportBgroup);
      Util.disable(topSimPanel.topBgroup);
      Util.disable(dumpPanel.dumpBgroup);
      Util.disable(decodePanel.decodeBgroup);
      if (decodePanel.decodefilePanel != null) {
        Util.disable(decodePanel.decodefilePanel.decodefileT);
      }
    }
  }
}
