package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;

public class TabImp extends JPanel {
  GenieGUI gui;
  String title;

  public TabImp() {
    super();
  }

  public void build(GenieGUI inGUI) {
    gui = inGUI;
    setLayout(new BorderLayout());
    setPreferredSize(new Dimension(UIConstants.SCREEN_WIDTH, UIConstants.SCREEN_HEIGHT));
  }

  public String toString() {
    return title;
  }

  public void populate() {
  }
}
