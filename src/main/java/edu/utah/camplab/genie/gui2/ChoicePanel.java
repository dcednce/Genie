package edu.utah.camplab.genie.gui2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//==============================================================================
public class ChoicePanel extends JPanel
  implements ActionListener {
  JRadioButton newRB, modRB;
  ButtonGroup choiceBgroup;
  OptionTab parentPanel;
  PackagePanel packagePanel;
  GenieGUI gui;

  //----------------------------------------------------------------------------
  public ChoicePanel() {
    super();
    TitledEtched titledEtched = new TitledEtched("New / Modify");
    setBorder(titledEtched.title);
    setPreferredSize(new Dimension(800, 200));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    newRB = new JRadioButton("Create New Analysis");
    modRB = new JRadioButton("Modify existing Parameter File");
    choiceBgroup = new ButtonGroup();
    choiceBgroup.add(newRB);
    choiceBgroup.add(modRB);
    newRB.addActionListener(this);
    modRB.addActionListener(this);
    //newRB.setActionCommand("new");
    //modRB.setActionCommand("mod");
    add(newRB);
    add(modRB);
  }

  //----------------------------------------------------------------------------
  public void setGUI(GenieGUI inGUI) {
    gui = inGUI;
  }

  //----------------------------------------------------------------------------
  public void setParentPanel(JPanel parent) {
    parentPanel = (OptionTab) parent;
  }

  //----------------------------------------------------------------------------
  public void actionPerformed(ActionEvent e) {
    boolean isNew = true;
    Object source = e.getSource();
    if (source == modRB) {
      // populate pkgPanel before adding to parentPanel;
      ParameterFilePanel parameterFilePanel = new ParameterFilePanel();
      parameterFilePanel.setOpaque(true);
      parentPanel.add(parameterFilePanel);
      parameterFilePanel.setParentPanel(parentPanel);
      parameterFilePanel.setGUI(gui);
      parentPanel.setParameterFilePanel(parameterFilePanel);
      isNew = false;
      //populateAllPanels();
      //modPanel = new ModParameterFile();
      //add(modPanel);
    }
    else {
      packagePanel = new PackagePanel();
      packagePanel.setParentPanel(parentPanel);
      packagePanel.setGUI(gui);
      packagePanel.setOpaque(true);
      parentPanel.setPackagePanel(packagePanel);
      parentPanel.add(packagePanel);
    }
    gui.setChoice(isNew);
    updateUI();
    setDisplayOnly();
  }

  //----------------------------------------------------------------------------
  /*
  public class ParameterFilePanel extends BasicPanel
				  implements ActionListener {
    JButton browsefileB, commitB;
    JLabel parameterfileL;
    JTextField parameterfileT;
    File parameterfile;

    public ParameterFilePanel() {
      super("Parameter File");
      TitledEtched titledEtched = new TitledEtched("Parameter File");
      setBorder(titledEtched.title);
      GridBagLayout gridbag = new GridBagLayout();
      GridBagConstraints constraintsL = new GridBagConstraints();
      GridBagConstraints constraintsT = new GridBagConstraints();
      GridBagConstraints constraintsB = new GridBagConstraints();
      setLayout(gridbag);
      setPreferredSize(new Dimension(300, 90));
      String nfs = new String("No File Selected");
      parameterfileT = new JTextField("");
      parameterfileT.setPreferredSize(new Dimension(280, 20));
      parameterfileL = new JLabel("Parameter File", JLabel.TRAILING); 
      parameterfileL.setLabelFor(parameterfileT);
      browsefileB = new JButton("Browse");
      browsefileB.addActionListener(this);
      commitB = new JButton("Commit");
      commitB.addActionListener(this);
      parameterfile = null;
      int gridyVal = 0;
      constraintsL.ipadx = 20;
      constraintsL.ipady = 20;
      constraintsL.gridx = 0;
      constraintsL.gridwidth = 30;
      constraintsT.ipadx = 20;
      constraintsT.ipady = 20;
      constraintsT.gridx = 60;
      constraintsT.gridwidth = 50;
      constraintsL.ipadx = 20;
      constraintsL.ipady = 20;
      constraintsL.gridx = 100;
      constraintsL.gridwidth = 3;
      gridbag.setConstraints(parameterfileL, constraintsL);
      gridbag.setConstraints(parameterfileT, constraintsT);
      gridbag.setConstraints(commitB, constraintsB);
      add(parameterfileL);
      add(parameterfileT);
      add(commitB);
    }

    public void actionPerformed (ActionEvent e) {
      Object source = e.getSource();
      if ( source == browsefileB ) {
        JFileChooser filechooser = new JFileChooser();
        filechooser.setDialogTitle("Parameter File");
        int returnVal = filechooser.showOpenDialog(ChoicePanel.this);
        if ( returnVal == JFileChooser.APPROVE_OPTION ) {
          parameterfile = filechooser.getSelectedFile();
          parameterfileT.setText(parameterfile.toString());
        }
      } else if ( source == commitB ) {
        // test whether retrievefile was selected in choicepanel
        Util.disable(parameterfileT);
        Util.disable(commitB);
      }
    }
  }
*/

  //---------------------------------------------------------------------------
  public void populateAllPanels() {
  }

  //---------------------------------------------------------------------------
  public void setDisplayOnly() {
    Util.disable(choiceBgroup);
  }
}


