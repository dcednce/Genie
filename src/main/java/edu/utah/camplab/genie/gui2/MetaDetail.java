package edu.utah.camplab.genie.gui2;

public class MetaDetail extends StatisticImp {

  public MetaDetail() {
    super("Meta Statistic");
  }

  public void build() {
    super.build("m");
  }
}

