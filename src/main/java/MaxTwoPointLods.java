import alun.genepi.LociVariables;
import alun.genepi.MaxLoder;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.util.GoldenSection;

/**
 * This program finds the maximum lod score for values of the
 * recombination between 0 and 1 (not 0 and 0.5).
 * <ul>
 * Usage : <b> java MaxTwoPointLods input.par input.ped </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * </ul>
 * <p>
 * The results are written to the standard output file as a bare table
 * with a row for each marker and two columns. The first column gives
 * the maximizing value of the recombination fraction and the second
 * gives the maximized lod score.
 * <p>
 * The method used is a golden section search that takes about 20
 * function evaluations to get to within 0.001 of the maximizing value.
 * <p>
 * The program maximizes the lod scores separately over the two intervals
 * (0,0.5) and (0.5,1), because there may be a local maximum in each
 * of these intervals. The output is a bare table of 4 columns
 * giving the maximizing value and maximum in the region (0,0.5)
 * followed by the maximizing value and maximum in the region (0.5,1).
 * Values of the recombination paramter in the region (0.5,1) have
 * no genetic interpretation, but may occur if the phase information
 * is low and/or the model is misspecified.
 * If you don't think you want values in the (0.5,1) region, just
 * ignore the last two columns.
 */

public class MaxTwoPointLods {
  public static void main(String[] args) {
    try {
      LinkageDataSet[] data = null;
      double error = 0.001;

      switch (args.length) {
        case 2:
          LinkageDataSet x = new LinkageDataSet(args[0], args[1]);
          data = x.splitByPedigree();
          for (int i = 0; i < data.length; i++) {
            data[i].downCode(false);
          }
          break;
        default:
          System.err.println("Usage: java MaxTwoPointLods input.par input.ped");
          System.exit(1);
      }

      LociVariables[] lvs = new LociVariables[data.length];
      for (int i = 0; i < data.length; i++) {
        lvs[i] = new LociVariables(new LinkageInterface(data[i]));
      }

      for (int j = 1; j < lvs[0].nLoci(); j++) {
        MaxLoder ml = new MaxLoder(lvs, j);
        System.out.print(GoldenSection.maximum(ml, 0, 0.5, error));
        System.out.print("\t");
        System.out.println(GoldenSection.maximum(ml, 0.5, 1, error));
      }
      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in MaxTwoPointLods:main()");
      e.printStackTrace();
    }
  }
}
