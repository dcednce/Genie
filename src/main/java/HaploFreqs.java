import alun.markov.Variable;
import alun.mcld.BnBForLD;
import alun.mcld.LDModel;
import alun.util.InputFormatter;

/**
 * This program find all the haplotypes with frequency above a specified
 * threshold for a graphical model for linkage disequilibrium.
 * <ul>
 * Usage : <b> java HaploFreqs < model.file [threshold] </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> model.file </b> is file containing a graphical model for linkage disequilibrium
 * as output by
 * <a href="HapGraph.html"> HapGraph </a>
 * for example. </li>
 * <li> <b> threshold </b> is the minimum frequency required for a haplotype to be output.
 * The default value is 0.</li>
 * </ul>
 * <p>
 * A list of haplotypes and their frequencies are written to the standard output file.
 * The total number of haplotypes written and their cumulative frequency are
 * written to the screen.
 */

public class HaploFreqs {
  public static void main(String[] args) {
    try {
      double bound = 0;
      switch (args.length) {
        case 1:
          bound = new Double(args[0]).doubleValue();
      }

      LDModel ldm = new LDModel(new InputFormatter());
      BnBForLD bnb = new BnBForLD(ldm, bound);

      double tot = 0;
      int count = 0;

      while (bnb.next()) {
        double x = ldm.getValue();
        tot += x;
        count++;
        for (Variable l : ldm.getLoci()) {
          System.out.print(1 + l.getState());
        }
        System.out.println("\t" + x);
      }
      System.err.println("\n\t" + count + "\t" + tot);
    } catch (Exception e) {
      System.err.println("Caught in HaploFreqs:main()");
      e.printStackTrace();
    }
  }
}
