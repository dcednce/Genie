import alun.graph.Graph;
import alun.graph.Network;
import alun.view.FrameQuitter;
import alun.viewgraph.*;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This is and interactive graphical program for viewing and editing a graph.
 *
 * <ul>
 * Usage : <b> java ViewGraph < input.graph </b>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.graph </b> is a file containing a graph specified as an adjacency list.
 * More specifically, each vertex is represented by a unique string. For each line of
 * input the vertex represented by the first string on the line is connected to the
 * vertices represented by any following strings. </li>
 * </ul>
 *
 * <p>
 * A complete description of the mouse and button controls for the program
 * is given at this
 * <a href="viewgraph.html"> link. </a>
 *
 * <p>
 * As an example, the following two listings specifiy the graph shown in the
 * picture below.
 * <br><hr>
 * A B <br>
 * C D <br>
 * C E <br>
 * B E <br>
 * D F <br>
 * F G <br>
 * A G <br>
 * G H <br>
 * G C <br>
 * B C <br>
 * I <br>
 * <br><hr>
 * C B D E <br>
 * B A E <br>
 * F D G <br>
 * G A H C <br>
 * I <br>
 * <br><hr>
 * <img src="graph.jpg" align=center border=0>
 * <br><hr>
 *
 * <p>
 * <b> ViewGraph mouse, button and key instructions </b>
 * <p>
 * <p>
 * ViewGraph is controled by a slide bar, some buttons, the arrow, home and shift keys,
 * but mostly by mouse operations.
 * <br>
 * <br>
 * All three mouse buttons are used.
 * For mice with 1 or 2 buttons, left mouse with the control key held down should have the
 * same effect as middle mouse, and left mouse with the meta key held down should have
 * the same effect as right mouse.
 * (Warning for my PC the control key fix works but the meta key one doesn't. I'm
 * not even sure which the meta key is. However, the right mouse button of the two on the
 * PC works like the right one of three.)
 * <br>
 *
 * <ul>
 * <li>
 * The <b>slide bar</b> on the right hand side controls the repulsive force in
 * the energy equation used to set the coordinates. Slide up to
 * reduce repulsion and down to increase it.
 * </li>
 * <br> <br>
 *
 * <li>
 * At the bottom of the frame there are 5 controls. From left to right they
 * are as follows.
 * <br>
 * <ul>
 * <li>
 * The <b> page control pull down menu </b> controls the appearance, orientation and
 * size of a page of paper. The top left hand corner of the page is always
 * at the origin. This is useful to preview how a print out will look.
 * </li>
 * <br>
 * <li>
 * The <b> print button </b> calls up the system's print frame.
 * The hard copy produced should look the same as what shows on the paper
 * chosen by the pull down menu above.
 * </li>
 * <br>
 * <li>
 * The <b> run/stop </b> button starts and stops the thread that optimised the
 * positions of the vertices. You should stop the thread if you need to print.
 * </li>
 * <br>
 * <li>
 * The <b> view/edit </b> button changes the effect of mouse operations in the
 * main canvas. See below for what they do.
 * </li>
 * <br>
 * <li>
 * The <b> text input </b> space on the right hand side allows you to type
 * in the name of a vertex. When you then press return the vertex moves to
 * the origin. If the vertex has previously been deleted it is put back into
 * the graph.
 * </li>
 * <br>
 * </ul>
 *
 * </li>
 * <br>
 *
 * <li>
 * Mouse operations in <b> view </b> mode with <b> no shift key pressed </b>.
 * <ul>
 * <li> Left mouse.
 * Drags a vertex. Vertex is free on release.
 * </li>
 * <li> Middle mouse.
 * Drags a vertex. Vertex is fixed at release position.
 * </li>
 * <li> Right mouse.
 * Translates the view by the amount dragged.
 * A bit like putting your finger on a piece of paper and moving it.
 * </li>
 * <br>
 * <br>
 * <li> Double click with any mouse button in the background resets the
 * vertices to their initial positions.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Mouse operations in <b> view </b> mode with <b> shift key pressed </b>.
 * <ul>
 * <li> Left mouse.
 * Drags a vertex and the component it is in. Vertex and component free
 * on release.
 * </li>
 * <li> Middle mouse.
 * Drags a vertex and the component it is in. Vertex and component are
 * fixed at release positions.
 * </li>
 * <li> Right mouse.
 * Translates the positions of the vertices relative to the position
 * of the paper by the amount dragged.
 * This is useful to center the picture on the paper ready for outputing.
 * </li>
 * <br>
 * <br>
 * <li> Double click with any mouse button in the background same as
 * view mode without shift.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Mouse operations in <b> edit </b> mode with <b> no shift key pressed </b>.
 * <ul>
 * <li> Left mouse.
 * Click on a vertex to add its neighbours to the graph.
 * </li>
 * <li> Middle mouse.
 * Click on a vertex to delete it from the graph.
 * </li>
 * <li> Right mouse.
 * Same as view mode without shift.
 * </li>
 * <br>
 * <br>
 * <li> Double click left mouse replaces all deleted
 * vertices in the graph.
 * </li>
 * <li> Double click middle mouse deletes all vertices from the
 * graph.
 * </li>
 * <li> Double click right mouse  has no effect.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Mouse operations in <b> edit </b> mode with <b> shift key pressed </b>.
 * <ul>
 * <li> Left mouse.
 * Click on a vertex to replace all vertices in the same component
 * to the graph.
 * </li>
 * <li> Middle mouse.
 * Click on a vertex to delete it and the component it is in from
 * the graph.
 * </li>
 * <li> Right mouse.
 * Does nothing.
 * </li>
 * <br>
 * <br>
 * <li>
 * Double click left mouse is the same as edit mode without shift key.
 * </li>
 * <li>
 * Double click middle mouse is the same as edit mode without shift key.
 * </li>
 * <li>
 * Double click right mouse is the same as edit mode without shift key.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Arrow key functions with <b> no shift key pressed </b>.
 * Mouse has to be in the picture canvas.
 * <ul>
 * <li> Up arrow. Increases the scale of viewing by 10%.
 * </li>
 * <li> Down arrow. Decreased the scale of viewing by 10%.
 * </li>
 * <li> Left arrow. Rotates the view by 15 degrees clockwise.
 * </li>
 * <li> Right arrow. Rotates the view by 15 degrees anticlockwise.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Arrow key functions with <b> shift key pressed </b>.
 * Mouse has to be in the picture canvas.
 * <ul>
 * <li> Up arrow. Increases the vertex positions by 10% relative to the
 * scale of the paper.
 * </li>
 * <li> Down arrow. Decreases the vertex positions by 10% relative to the
 * scale of the paper.
 * </li>
 * <li> Left arrow. Rotates the vertex positions by 15 degrees clockwise relative
 * to the paper orientation.
 * </li>
 * <li> Right arrow. Rotates the vertex poritions by 15 degrees anticlockwise
 * relative to the paper orientation.
 * </li>
 * </ul>
 * </li>
 * <br>
 *
 * <li>
 * Pressing the <b> home key </b> undoes all transformations and
 * replaces the origin at the top left of the screen.
 * </li>
 * <br>
 * </ul>
 */

public class ViewGraph {
  public static void main(String[] args) {
    try {
      Graph<String> g = Network.read(new BufferedReader(new InputStreamReader(System.in)));
      MappableGraph m = new MyMap<String>(g);
      MapViewer r = new MapEditor();
      r.init();
      r.setMap(m);
      Frame fr = new Frame();
      fr.addWindowListener(new FrameQuitter());
      fr.add(r);
      fr.pack();
      fr.setVisible(true);
      r.start();
    } catch (Exception e) {
      System.err.println("Caught in ViewGraph:main()");
      e.printStackTrace();
    }
  }
}

class MyMap<V> extends StaticMap<V> {
  public MyMap(Graph<V> g) {
    super(g);
  }

  public Mappable makeRep(Object a) {
    LabelledBlob b = new LabelledBlob(a.toString());
    b.setShowText(true);
    //b.setColor(Color.yellow);
    b.setColor(Color.white);
    b.setTextColor(Color.black);
    b.setSize(12, 12);
    b.setShape(LabelledBlob.RECTANGLE);
    return b;
  }
}
