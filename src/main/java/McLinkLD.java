import alun.genepi.LinkageVariables;
import alun.genepi.Tlods;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.mcld.DecomposableSearch;
import alun.mcld.GraphMHScheme;
import alun.mcld.JointScheme;
import alun.mcld.LDSampler;
import alun.util.StringFormatter;

/**
 * This program calculates linkage statistics while jointly estimating a
 * model for linkage disequilibrium between the loci. It successively updates
 * the LD model given the current haplotypes for the founders in the pedigree,
 * then updates the genotypes and inheritance states of the pedigree given
 * the current LD model and uses these to compute linkage statistics.
 *
 * <ul>
 * Usage <b> java McLink input.par input.ped [n_simulations] [-v/-t] </b>
 * </ul>
 * <p>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * <li> <b> n_simulations </b> is the number of MCMC samples to use in the calculations.
 * The default is 1000. </li>
 * <li> If <b> -v </b> option is specified lod scores are output after each iteration which
 * may be useful in tracking the performance of the sampler. If <b> -t </b> is
 * specified there is no intermediate output. <b> -t </b> is the default. </li>
 * </ul>
 *
 * <p>
 * The output is a row of TLOD, or complex lod score, statistics for each
 * locus. There is a column for each
 * values of the recombination fraction in {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1}.
 */

public class McLinkLD {
  public static void main(String[] args) {
    try {
      double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1};
      int wherehalf = 6;
      int n_samples = 1000;
      boolean report = false;
      double errorprob = 0.010;
      int metropergibbs = 1000;
      LinkageDataSet data = null;

      switch (args.length) {
        case 4:
          if (args[3].equals("-v")) {
            report = true;
          }
          else if (args[3].equals("-t")) {
            report = false;
          }

        case 3:
          n_samples = new Integer(args[2]).intValue();

        case 2:
          data = new LinkageDataSet(args[0], args[1]);
          break;

        default:
          System.err.println("Usage: java McLinkLD input.par input.ped [n_samples] [-v/-t]");
          System.exit(1);
      }

      LinkageVariables vars = new LinkageVariables(new LinkageInterface(data), errorprob);
      LDSampler haps = new LDSampler(vars, 1);
      GraphMHScheme sch = new DecomposableSearch(haps.getLoci());
      JointScheme mc = new JointScheme(haps, sch);
      System.err.println("BIC parameter penalty = " + mc.getParameterPenalty());

      Tlods tl = new Tlods(vars, thetas);
      double[][] lods = new double[vars.nLoci()][tl.nThetas()];

      for (int i = 1; i <= n_samples; i++) {

        for (int j = 0; j < metropergibbs; j++) {
          mc.metropolisUpdate(1.0);
        }
        System.err.print("'");
        mc.gibbsUpdate();
        System.err.print("'");

        for (int j = 1; j < lods.length; j++) {
          double[] lik = tl.twoPointLikelihood(j);
          for (int k = 0; k < lik.length; k++) {
            lods[j][k] += lik[k] / lik[wherehalf];
          }

          if (report) {
            for (int k = 0; k < lods[j].length; k++) {
              System.out.print("\t" +
                StringFormatter.format(Math.log(lods[j][k] / (i + 1)) / Math.log(10), 2, 4));
            }
            System.out.println();
          }
        }

        System.err.print(".");
        if (report) {
          System.out.println();
        }
        if (i % 100 == 0) {
          System.err.println("\n" + i);
        }
        else if (i % 10 == 0) {
          System.err.println();
        }
      }
      System.err.println();

      for (int j = 1; j < lods.length; j++) {
        for (int k = 0; k < lods[j].length; k++) {
          lods[j][k] = Math.log(lods[j][k] / n_samples) / Math.log(10);
          System.out.print(StringFormatter.format(lods[j][k], 2, 4) + " ");
        }
        System.out.println();
      }

      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in McLinkLD:main()");
      e.printStackTrace();
    }
  }
}
