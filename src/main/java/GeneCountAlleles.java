import alun.genepi.GeneCounter;
import alun.genio.LinkageDataSet;

import java.io.PrintWriter;

/**
 * This program calculates maximum likelihood estimates of allele frequencies
 * from genotypes observed in individuals related in pedigrees.
 *
 * <ul>
 * Usage : <b> java GeneCountAlleles input.par input.ped [conv_thresh] [-a] </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is the original LINKAGE parameter file. </li>
 * <li> <b> input.ped </b> is the original LINKAGE pedigree file. </li>
 * <li> <b> conv_thresh </b> is an optional parameter specifiying the largest difference
 * between allele frequencies in successive iterations that is acceptable before
 * convergence is assumed. The default value is 0.000001.</li>
 * <li> if <b> -a </b> is specified the frequencies for all loci are estimated (see below).</li>
 * </ul>
 *
 * <p>
 * The output is a linkage parameter file that is
 * the same as the old one, except that the original allele
 * frequencies will be replaced by the new maximum likelihood estimates.
 * This is writen to standard output.
 *
 * <p>
 * This program is an extension of C A B Smith's gene counting method or EM
 * algorithm. It works
 * by iteratively inferring the genotypes of the founders in a pedigree given
 * the current alleles frequency estimates, the pedigree structure and any
 * genotypes observed in the pedigree (the E step). From the posterior
 * distribution of the founder alleles frequencies new maximum likelihood
 * estimates for the allele frequencies are derived (the M step).
 * <p>
 * The program also uses C A B Smith's trick of using the rate of convergence
 * of the algorithm to estimate the curvature of the likelihood, and hence
 * get an inflation factor for the variance of the estimators. As the program
 * works through each locus it outputs to the screen the initial estimates,
 * the final estimates,
 * and the standard errors of the estimates incorporating the inflation factor.
 * <p>
 * In a typical use of this program the input parameter file will contain
 * trait data coded as LINKAGE affection status loci. Because the pedigrees are usually
 * ascertained in ways that bias the estimation of alleles at trait loci
 * the default is not to estimate these frequencies but only those
 * for LINKAGE numbered allele loci.
 * To force estimation at all loci the <b> -a </b> is specified on the command line.
 */

public class GeneCountAlleles {
  public static void main(String[] args) {
    try {
      LinkageDataSet x = null;
      PrintWriter parout = new PrintWriter(System.out);
      boolean all = false;

      int max_its = 10000;
      boolean report = true;
      double max_diff = 0.000001;
      double err_rate = 0.01;

      switch (args.length) {
        case 4:
          if (args[3].equals("-a")) {
            all = true;
          }

        case 3:
          if (args[2].equals("-a")) {
            all = true;
          }
          else {
            max_diff = new Double(args[2]).doubleValue();
          }

        case 2:
          x = new LinkageDataSet(args[0], args[1]);
          break;

        default:
          System.err.println("Usage: java GeneCountAlleles input.par input.ped [conv_thresh] [-a]");
          System.exit(1);
      }

      x.countAlleleFreqs();
      GeneCounter.geneCount(x, report, max_diff, max_its, err_rate);
      parout.println(x.getParameterData());
      parout.flush();
    } catch (Exception e) {
      System.err.println("Caught in EstimateAlleleFreqs:main().");
      e.printStackTrace();
    }
  }
}
