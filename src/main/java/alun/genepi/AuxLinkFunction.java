package alun.genepi;

import alun.markov.Variable;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class AuxLinkFunction extends AuxiliaryFunction {
  private AuxiliaryVariable u = null;
  private AuxiliaryVariable v = null;
  private double mtheta = 0;
  private double ftheta = 0;

  public AuxLinkFunction(AuxiliaryVariable uu, AuxiliaryVariable vv, double mt, double ft) {
    u = uu;
    v = vv;
    mtheta = mt;
    ftheta = ft;
  }

// Private Data.

  public double getValue() {
    double x = 1;
    for (Iterator<Inheritance> i = u.getPats().iterator(), j = v.getPats().iterator(); i.hasNext() && j.hasNext(); ) {
      x *= (i.next().getState() == j.next().getState() ? 1 - mtheta : mtheta);
    }
    for (Iterator<Inheritance> i = u.getMats().iterator(), j = v.getMats().iterator(); i.hasNext() && j.hasNext(); ) {
      x *= (i.next().getState() == j.next().getState() ? 1 - ftheta : ftheta);
    }
    return x;
  }

  public void init() {
  }

  public Set<Variable> getVariables() {
    Set<Variable> s = new LinkedHashSet<Variable>();
    s.add(u);
    s.add(v);
    return s;
  }

  public String toString() {
    return "JV " + getVariables();
  }
}
