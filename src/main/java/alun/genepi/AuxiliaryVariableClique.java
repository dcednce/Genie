package alun.genepi;

import alun.markov.*;

import java.util.Iterator;
import java.util.Set;

public class AuxiliaryVariableClique extends BasicClique {
  public AuxiliaryVariableClique(Set<Variable> v, Clique next) {
    super(v, next);
  }

  public void compute(Table[] t) {
    for (Iterator<Function> i = inputs().iterator(); i.hasNext(); ) {
      ((AuxiliaryFunction) i.next()).init();
    }
    super.compute(t);
  }

  public void drop() {
    super.drop();
    for (Iterator<Variable> i = peeled().iterator(); i.hasNext(); ) {
      ((AuxiliaryVariable) i.next()).set();
    }
  }
}
