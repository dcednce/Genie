package alun.genepi;

import alun.markov.Variable;

import java.util.Collections;
import java.util.LinkedHashSet;

public class AllelePrior extends GeneticFunction {
  private Allele a = null;
  private double[] f = null;

  public AllelePrior(Allele al, double[] allelefreqs) {
    f = allelefreqs;
    a = al;
  }

  public LinkedHashSet<Variable> getVariables() {
    return new LinkedHashSet<Variable>(Collections.singleton(a));
  }

// Private data.

  public double getValue() {
    return f[a.getState()];
  }

  public String toString() {
    return "APRI " + getVariables();
  }
}
