package alun.genepi;

import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.util.Curve;

public class MaxLoder implements Curve {
  private double log10 = Math.log(10);
  private boolean reclaim = false;
  private double lhalf = 0;
  private GraphicalModel[] m = null;
  private OneTheta theta = null;
  public MaxLoder(LociVariables l, int j) {
    this(array(l), j);
  }
  public MaxLoder(LociVariables[] l, int j) {
    reclaim = l.length > 1;
    theta = new OneTheta();
    theta.fix(0.5);
    lhalf = 0;

    m = new GraphicalModel[l.length];

    for (int i = 0; i < m.length; i++) {
      Product p = l[i].unrelatedLocusProduct(j);
      m[i] = new GraphicalModel(p);
      m[i].allocateOutputTables();
      m[i].allocateInvolTables();
      m[i].reduceStates();
      m[i].clearInvolTables();
      m[i].clearOutputTables();

      p = l[i].locusProduct(j);
      m[i] = new GenotypeModel(p);
      m[i].allocateOutputTables();
      m[i].allocateInvolTables();
      m[i].reduceStates();
      m[i].clearInvolTables();
      m[i].clearOutputTables();

      p = l[i].twoPointProduct(0, j, theta);
      p.triangulate();
      m[i] = new GenotypeModel(p, theta);
      m[i].allocateOutputTables();
      lhalf += m[i].logPeel();
      if (reclaim) {
        m[i].clearOutputTables();
      }
    }
  }

  private static LociVariables[] array(LociVariables l) {
    LociVariables[] out = {l};
    return out;
  }

  public double f(double x) {
    theta.fix(x);
    double y = 0;
    for (int i = 0; i < m.length; i++) {
      if (reclaim) {
        m[i].allocateOutputTables();
      }

      y += m[i].logPeel();

      if (reclaim) {
        m[i].clearOutputTables();
      }
    }
    y = (y - lhalf) / log10;
    return y;
  }
}
