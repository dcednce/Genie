package alun.genepi;

import alun.markov.GraphicalModel;
import alun.markov.Variable;

import java.util.LinkedHashSet;
import java.util.Set;

public class AuxLocusFunction extends AuxiliaryFunction {
  private double[] tab = null;
  private AuxiliaryVariable v = null;

  public AuxLocusFunction(AuxiliaryVariable vv) {
    v = vv;
  }

  public Set<Variable> getVariables() {
    Set<Variable> s = new LinkedHashSet<Variable>();
    s.add(v);
    return s;
  }

  public void init() {
    tab = new double[v.getNStates()];
    for (v.init(); v.next(); ) {
      GraphicalModel m = v.getBlock();
      m.allocateOutputTables();
      tab[v.getStateIndex()] = m.peel();
      m.clearOutputTables();
    }
  }

// Private data.

  public double getValue() {
    return tab[v.getStateIndex()];
  }

  public String toString() {
    return "LF " + getVariables();
  }
}
