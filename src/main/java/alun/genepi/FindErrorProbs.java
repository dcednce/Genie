package alun.genepi;

import alun.genio.GeneticDataSource;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.Function;
import alun.markov.GraphicalModel;
import alun.util.StringFormatter;

import java.util.Vector;

public class FindErrorProbs extends ObligatoryErrors {
  public static void main(String[] args) {
    try {
      LinkageDataSet x = null;
      double overallthresh = 0.5;
      double margthresh = 0.25;
      double prior = 0.01;
      int[] zero = {0};
      int[] zeroone = {0, 1};
      int[] one = {1};

      switch (args.length) {
        case 5:
          prior = new Double(args[4]).doubleValue();
        case 4:
          margthresh = new Double(args[3]).doubleValue();
        case 3:
          overallthresh = new Double(args[2]).doubleValue();
        case 2:
          x = new LinkageDataSet(args[0], args[1]);
          break;
        default:
          System.err.println("Usage: java CheckErrors parfile pedfile [main_threshold] [marginal_threshold] [error_prior]");
          System.exit(1);
      }

      LinkageDataSet[] xx = x.splitByPedigree();
      for (int i = 0; i < xx.length; i++) {
        GeneticDataSource d = new LinkageInterface(xx[i]);
        for (int j = 0; j < d.nLoci(); j++) {
          double[] freqs = d.alleleFreqs(j);
          double[] back = new double[freqs.length];
          for (int k = 0; k < back.length; k++) {
            back[k] = freqs[k];
          }
          x.countAlleleFreqs(j);
          freqs = d.alleleFreqs(j);
          for (int k = 0; k < back.length; k++) {
            if (freqs[k] > 0) {
              freqs[k] = back[k];
            }
          }

          try {
            LocusVariables g = new LocusVariables(d, j, prior);
            GraphicalModel m = new GenotypeModel(g.makeUnrelatedProduct());
            m.allocateOutputTables();
            m.allocateInvolTables();
            m.reduceStates();

            Genotype[] geno = g.genotypes();
            alun.genepi.Error[] erro = g.errors();

            m = new GenotypeModel(g.makeLocusProduct());
            for (int k = 0; k < erro.length; k++) {
              if (erro[k] != null) {
                erro[k].setStates(zero);
              }
            }
            m.allocateOutputTables();
            double pnoerror = m.peel();
            boolean oblig = (pnoerror <= Double.MIN_VALUE);
            for (int k = 0; k < erro.length; k++) {
              if (erro[k] != null) {
                erro[k].setStates(zeroone);
              }
            }
            m.allocateOutputTables();
            double allprob = m.peel();
            pnoerror /= allprob;

            if (pnoerror < overallthresh) {
              System.out.println("Pedigree (#" + (1 + i) + ") " + x.name() + "\tlocus (#" + (1 + j) + ") " + d.locName(j));
              if (oblig) {
                System.out.println("\t Obligatory error");
              }
              System.out.println
                (
                  "\t P(at least one error) = "
                    + StringFormatter.format((1 - pnoerror), 2, 3) + " > "
                    + StringFormatter.format(1 - overallthresh, 2, 3)
                );
              System.out.print("\t Most probable individual(s) in error: ");

              m.allocateInvolTables();
              m.max();
              m.drop();
              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  if (erro[k].getState() == 1) {
                    erro[k].setStates(one);
                    System.out.print(" (#" + (1 + k) + ") " + d.id(k) + ",");
                  }
                  else {
                    erro[k].setStates(zero);
                  }
                }
              }
              m.allocateOutputTables();
              double best = m.peel() / allprob;
              System.out.println("   with probability " + StringFormatter.format(best, 2, 4));
              System.out.println("\t Individuals with high posterior error probability:");

              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  erro[k].setStates(zeroone);
                }
              }
              m.allocateOutputTables();
              m.allocateInvolTables();
              m.findMarginals();

              for (int k = 0; k < erro.length; k++) {
                if (erro[k] != null) {
                  Function f = m.getMarginal(erro[k]);
                  erro[k].setState(1);
                  if (f.getValue() > margthresh) {
                    outputErrorUnordered(d, i, j, k, f.getValue(), geno[k], m.getMarginal(geno[k]));
                  }
                }
              }

              System.out.println();
            }
          } catch (OutOfMemoryError e) {
            System.out.println("Pedigree (#" + (1 + i) + ") " + xx[i].name() + "\tlocus (#" + (1 + j) + ") " + d.locName(j));
            System.out.println("Cannot allocate memory for computation");
          }

          for (int k = 0; k < back.length; k++) {
            freqs[k] = back[k];
          }
        }
      }
    } catch (Exception e) {
      System.err.println("Caught in ErrorProbs:main().");
      e.printStackTrace();
    }
  }

  public static void outputError(GeneticDataSource d, int pi, int lj, int k, double prob, Genotype g, Function post) {
    double repthresh = 0.95;
    StringFormatter fm = new StringFormatter();
    System.out.println("\t\t Individual (#" + (1 + k) + ") " + d.id(k));
    System.out.println("\t\t Observation = " + d.call(k, lj));
    System.out.println("\t\t P(Error) = " + fm.format(prob, 1, 3));
    System.out.println("\t\t Probable states (paternal allele,maternal allele):");

    double[] x = new double[g.getNStates()];
    Vector<String> v = new Vector<String>();
    int ii = 0;
    for (g.init(); g.next(); ii++) {
      x[ii] = post.getValue();
      v.add((1 + g.pat()) + "," + (1 + g.mat()));
      for (int jj = ii; jj > 0; jj--) {
        if (x[jj] > x[jj - 1]) {
          double t = x[jj];
          x[jj] = x[jj - 1];
          x[jj - 1] = t;
          String s = v.get(jj);
          v.set(jj, v.get(jj - 1));
          v.set(jj - 1, s);
        }
      }
    }

    double tot = 0;
    for (ii = 0; ii < x.length && tot < repthresh; ii++) {
      tot += x[ii];
      System.out.println("\t\t\t" + "P(" + v.get(ii) + ") = " + fm.format(x[ii], 1, 3));
    }
  }

  public static void outputErrorUnordered(GeneticDataSource d, int pi, int lj, int k, double prob, Genotype g, Function post) {
    double repthresh = 0.95;
    StringFormatter fm = new StringFormatter();
    System.out.println("\t\t Individual (#" + (1 + k) + ") " + d.id(k));
    System.out.println("\t\t Observation = " + d.call(k, lj));
    System.out.println("\t\t P(Error) = " + fm.format(prob, 1, 3));
    System.out.println("\t\t Probable genotypes:");

    MyResult[][] res = new MyResult[d.nAlleles(lj)][d.nAlleles(lj)];
    Vector<MyResult> v = new Vector<MyResult>();
    for (int i = 0; i < res.length; i++) {
      for (int j = 0; j <= i; j++) {
        res[i][j] = new MyResult(i, j);
        v.add(res[i][j]);
      }
    }

    for (g.init(); g.next(); ) {
      int i = g.pat();
      int j = g.mat();
      if (j > i) {
        int kk = j;
        j = i;
        i = kk;
      }
      res[i][j].x += post.getValue();
    }

    for (int i = 0; i < v.size(); i++) {
      for (int j = i; j > 0; j--) {
        if (v.get(j).x > v.get(j - 1).x) {

          MyResult t = v.get(j);
          v.set(j, v.get(j - 1));
          v.set(j - 1, t);
        }
      }
    }

    double tot = 0;
    for (int i = 0; i < v.size() && tot < repthresh; i++) {
      tot += v.get(i).x;
      System.out.println("\t\t\t" + "P(" + v.get(i).s + ") = " + fm.format(v.get(i).x, 1, 3));
    }
  }
}

class MyResult {
  public String s = null;
  public double x = 0;
  public MyResult(int i, int j) {
    s = (1 + j) + "," + (1 + i);
  }
}
