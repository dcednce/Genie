package alun.genepi;

import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.markov.GraphicalModel;

import java.util.LinkedHashSet;

public class LinkedLocusSampler extends IndependentLocusSampler {
  public LinkedLocusSampler(LinkageVariables vars) {
    super(vars);

    LinkedHashSet<GraphicalModel> g = new LinkedHashSet<GraphicalModel>();
    g.add(new GenotypeModel(vars.connectedLocusProduct(1, false, true)));
    for (int j = 2; j < vars.nLoci(); j++) {
      g.add(new GenotypeModel(vars.connectedLocusProduct(j, true, true)));
    }
    setLocusBlocks(g);
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      int runs = 10;
      LinkedLocusSampler mc = null;

      switch (args.length) {
        case 2:
          LinkageDataSet data = new LinkageDataSet(args[0], args[1]);
          data.downCode(false);
          mc = new LinkedLocusSampler(new LinkageVariables(new LinkageInterface(data)));
          break;
        default:
          System.err.println("Usage: java LocusSampler " + "input.par input.ped");
          System.exit(1);
      }

      for (int i = 0; i < runs; i++) {
        mc.sample();
        System.out.print(".");
      }
      System.out.println();
    } catch (Exception e) {
      System.err.println("Caught in LinkedLocusSampler:main()");
      e.printStackTrace();
    }
  }
}
