package alun.genepi;

import alun.markov.Variable;

import java.util.Collections;
import java.util.LinkedHashSet;

public class Penetrance extends GeneticFunction {
  private Genotype g = null;
  private double[][] p = null;

  public Penetrance(Genotype gt, double[][] penet) {
    p = penet;
    g = gt;
  }

  public LinkedHashSet<Variable> getVariables() {
    return new LinkedHashSet<Variable>(Collections.singleton(g));
  }

// Private data.

  public double getValue() {
    return p[g.pat()][g.mat()];
  }

  public String toString() {
    return "PENET " + getVariables();
  }
}
