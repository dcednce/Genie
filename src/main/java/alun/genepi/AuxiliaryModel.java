package alun.genepi;

import alun.markov.Clique;
import alun.markov.GraphicalModel;
import alun.markov.Product;
import alun.markov.Variable;

import java.util.Set;

public class AuxiliaryModel extends GraphicalModel {
  public AuxiliaryModel(Product p) {
    super(p);
  }

  public Clique makeClique(Set<Variable> v, Clique next, Product p) {
    return new AuxiliaryVariableClique(v, next);
  }
}
