package alun.genepi;

import alun.markov.Variable;

import java.util.LinkedHashSet;

public class ErrorPrior extends GeneticFunction {
  private Error e = null;
  private double p = 0;

  public ErrorPrior(Error er, double pri) {
    e = er;
    p = pri;
  }

  public LinkedHashSet<Variable> getVariables() {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    s.add(e);
    return s;
  }

  public double getValue() {
    return e.getState() == 0 ? 1 - p : p;
  }

  public void set(double pri) {
    p = pri;
  }

  public Error getError() {
    return e;
  }
}
