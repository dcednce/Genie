package alun.genepi;

import alun.markov.Variable;

import java.util.LinkedHashSet;

public class AllelePenetrance extends GeneticFunction {
  private Allele pat = null;
  private Allele mat = null;
  private double[][] p = null;

  public AllelePenetrance(Allele paternal, Allele maternal, double[][] penet) {
    pat = paternal;
    mat = maternal;
    fix(penet);
  }

  public LinkedHashSet<Variable> getVariables() {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    s.add(pat);
    s.add(mat);
    return s;
  }

// Private data.

  public double getValue() {
    return p == null ? 1 : p[pat.getState()][mat.getState()];
  }

  public String toString() {
    return "APENET " + getVariables();
  }

  public void fix(double[][] penet) {
    p = penet;
  }
}
