package alun.genepi;

import alun.markov.Variable;

import java.util.Collections;
import java.util.LinkedHashSet;

/**
 * A function that returns the prior probability of the
 * genotype as the product of the priors for the paternal
 * and maternal alleles.
 */
public class GenotypePrior extends GeneticFunction {
  private Genotype g = null;
  private double[] f = null;

  public GenotypePrior(Genotype gt, double[] allelefreqs) {
    f = allelefreqs;
    g = gt;
  }

  public LinkedHashSet<Variable> getVariables() {
    return new LinkedHashSet<Variable>(Collections.singleton(g));
  }

// Private data.

  public double getValue() {
    return f[g.pat()] * f[g.mat()];
  }

  public String toString() {
    return "PRIOR " + getVariables();
  }
}
