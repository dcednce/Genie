package alun.markov;

import alun.util.DoubleValue;
import alun.util.IntValue;

import java.util.*;

/**
 * An implementation of the Table interface that stores only non zero values.
 * This can save a lot of space for some functions at the cost of some
 * speed in setting and getting values.
 */
public class SparseTable extends Table {
  private Iterator<IntValue> ii = null;
  private Map<IntValue, DoubleValue> h = null;

  public SparseTable(Collection<? extends Variable> vv) {
    super(vv);
    h = new HashMap<IntValue, DoubleValue>();
  }

  public String toString() {
    return "S" + super.toString();
  }

  public final double getValue() {
    IntValue v = new IntValue(getStateIndex());
    DoubleValue c = h.get(v);
    return c == null ? 0 : c.x;
  }

  public final void setValue(double d) {
    IntValue v = new IntValue(getStateIndex());

    if (d == 0) {
      h.remove(v);
    }
    else {
      DoubleValue c = h.get(v);
      if (c == null) {
        c = new DoubleValue(0);
        h.put(v, c);
      }
      c.x = d;
    }
  }

  public final void multiply(double m) {
    IntValue v = new IntValue(getStateIndex());

    if (m == 0) {
      h.remove(v);
    }
    else {
      DoubleValue c = h.get(v);
      if (c != null) {
        c.x *= m;
      }
    }
  }

  public final void increase(double d) {
    IntValue v = new IntValue(getStateIndex());
    DoubleValue c = h.get(v);

    if (c == null) {
      c = new DoubleValue(0);
      h.put(v, c);
    }
    c.x += d;

    if (c.x == 0) {
      h.remove(v);
    }
  }

  public final void initToZero() {
    h.clear();
  }

  public final int size() {
    return h.keySet().size();
  }

  public final double sum() {
    double x = 0;
    for (Iterator<DoubleValue> i = h.values().iterator(); i.hasNext(); ) {
      x += i.next().x;
    }
    return x;
  }

  public final void scale(double x) {
    for (Iterator<DoubleValue> i = h.values().iterator(); i.hasNext(); ) {
      i.next().x *= x;
    }
  }

  public final void init() {
    ii = new LinkedHashSet<IntValue>(h.keySet()).iterator();
  }

// Private data and methods.

  public final boolean next() {
    if (ii.hasNext()) {
      setStateIndex(ii.next().i);
      return true;
    }
    else {
      ii = null;
      return false;
    }
  }
}
