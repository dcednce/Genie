package alun.markov;

/**
 * A Variable is an object that can be in some finite number of states.
 */
public interface Variable {
  /**
   * Initialized the Variable so that the next call to the next() method
   * puts the Variable in it's first state.
   */
  public void init();

  /**
   * If there is another state for the Variable, it is advanced to that
   * state and the value true is returned. Otherwise init() is called and and false returned.
   */
  public boolean next();

  /**
   * Returns the value of the current state of the Variable.
   */
  public int getState();

  /**
   * Returns the index of the current state of the Variable.
   */
  public int getStateIndex();

  /**
   * Sets the current state to the one corresponding to the given index.
   */
  public void setStateIndex(int i);

  /**
   * Sets the current state to the given integer.
   * A safe implementation would return a boolean indicating whether
   * the given state was valid.
   */
  public boolean setState(int s);

  /**
   * Returns the number of statest that the Variable can take.
   */
  public int getNStates();
}
