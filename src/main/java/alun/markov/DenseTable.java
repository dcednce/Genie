package alun.markov;

import java.util.Collection;

/**
 * An implementation of the Table interface that stores the values
 * in an array of doubles.
 * This is suitable for use when a large proportion of the states
 * of the Variable set have non zero values associated with them.
 */
public final class DenseTable extends Table {
  private double[] table = null;

  public DenseTable(Collection<? extends Variable> vv, double[] tab) {
    super(vv);
    if (tab.length != getNStates()) {
      throw new RuntimeException("Missmatch of array length in DenseTable");
    }
    table = tab;
  }

  public DenseTable(Collection<? extends Variable> vv) {
    super(vv);
    table = new double[getNStates()];
  }

/*
	public String toString()
	{
		StringBuffer b = new StringBuffer();
		b.append("D"+super.toString());
		for (Variable v: getVariables())
		{
			b.append(" "+v+"("+v.getState()+")");
		}
		b.append("\t");
		for (int i=0; i<table.length; i++)
			b.append(" "+table[i]);
		return b.toString();
	}
*/

  public String toString() {
    return "D" + super.toString();
  }

  public final double getValue() {
    return table[getStateIndex()];
  }

  public final void setValue(double d) {
    table[getStateIndex()] = d;
  }

  public final void multiply(double d) {
    table[getStateIndex()] *= d;
  }

  public final void increase(double d) {
    table[getStateIndex()] += d;
  }

  public final void initToZero() {
    for (int i = 0; i < table.length; i++) {
      table[i] = 0;
    }
  }

  public final double sum() {
    double x = 0;
    for (int i = 0; i < table.length; i++) {
      x += table[i];
    }
    return x;
  }

  public final void scale(double x) {
    for (int i = 0; i < table.length; i++) {
      table[i] *= x;
    }
  }

/*
	public final void init()
	{
		counter = -1;
	}

	public final boolean next()
	{
		while (++counter < table.length && table[counter] == 0);
		if (counter == table.length)
			return false;
		else
		{
			setCurrent(counter);
			return true;
		}
	}
*/

  public final int size() {
    return table.length;
  }

// Private data and methods.

  public double[] getArray() {
    return table;
  }
//	private int counter;
}
