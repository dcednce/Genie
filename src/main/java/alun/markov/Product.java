package alun.markov;

import alun.graph.Graph;
import alun.graph.Network;

import java.util.*;

public class Product implements Graph<Variable> //Function, Graph<Variable>
{
  private LinkedHashMap<Variable, LinkedHashSet<Function>> hv = null;
  private LinkedHashMap<Function, LinkedHashSet<Variable>> hf = null;
  private Network<Variable> fills = null;

  public Product() {
    hv = new LinkedHashMap<Variable, LinkedHashSet<Function>>();
    hf = new LinkedHashMap<Function, LinkedHashSet<Variable>>();
    fills = new Network<Variable>();
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    s.append("PROD\n");
    for (Iterator<Function> i = getFunctions().iterator(); i.hasNext(); ) {
      s.append("\t" + i.next() + "\n");
    }
    if (!getFunctions().isEmpty()) {
      s.deleteCharAt(s.length() - 1);
    }
    return s.toString();
  }

  public boolean contains(Object v) {
    return hv.get(v) != null;
  }

  public boolean connects(Object u, Object v) {
    return contains(u) && contains(v) && (getVariables(getFunctions((Variable) u)).contains(v) || fills.connects(u, v));
  }

  public LinkedHashSet<Variable> getVertices() {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>(hv.keySet());
    return s;
  }

  public LinkedHashSet<Variable> getNeighbours(Object v) {
    LinkedHashSet<Function> f = hv.get(v);
    if (f == null) {
      return null;
    }

    LinkedHashSet<Variable> n = getVariables(f);
    n.remove(v);
    Collection<Variable> fl = fills.getNeighbours(v);
    if (fl != null) {
      n.addAll(fl);
    }
    return n;
  }

  public void add(Function f) {
    LinkedHashSet<Variable> v = hf.get(f);
    if (v == null) {
      v = new LinkedHashSet<Variable>();
      hf.put(f, v);
    }
    v.addAll(f.getVariables());

    for (Iterator<Variable> i = v.iterator(); i.hasNext(); ) {
      Variable vv = i.next();
      LinkedHashSet<Function> vvf = hv.get(vv);
      if (vvf == null) {
        vvf = new LinkedHashSet<Function>();
        hv.put(vv, vvf);
      }
      vvf.add(f);
    }
  }

  public void add(Collection<Function> f) {
    for (Iterator<Function> i = f.iterator(); i.hasNext(); ) {
      add(i.next());
    }
  }

  public void addProduct(Product p) {
    add(p.getFunctions());
  }

  public void remove(Function f) {
    for (Iterator<Variable> i = getVariables(f).iterator(); i.hasNext(); ) {
      hv.get(i.next()).remove(f);
    }
    hf.remove(f);
  }

  public void remove(Collection<Function> f) {
    for (Iterator<Function> i = f.iterator(); i.hasNext(); ) {
      remove(i.next());
    }
  }

  public void removeProduct(Product p) {
    remove(p.getFunctions());
  }

  public void removeVariable(Variable v) {
    for (Iterator<Function> i = getFunctions(v).iterator(); i.hasNext(); ) {
      hf.get(i.next()).remove(v);
    }
    hv.remove(v);
    fills.remove(v);
  }

  public void removeVariables(Collection<Variable> v) {
    for (Iterator<Variable> i = v.iterator(); i.hasNext(); ) {
      removeVariable(i.next());
    }
  }

  public LinkedHashSet<Variable> getVariables() {
    return new LinkedHashSet<Variable>(hv.keySet());
  }

  public LinkedHashSet<Variable> getVariables(Function f) {
    return new LinkedHashSet<Variable>(hf.get(f));
  }

  public LinkedHashSet<Variable> getVariables(Collection<Function> f) {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    for (Iterator<Function> i = f.iterator(); i.hasNext(); ) {
      s.addAll(hf.get(i.next()));
    }
    return s;
  }

  public LinkedHashSet<Function> getFunctions() {
    return new LinkedHashSet<Function>(hf.keySet());
  }

  public LinkedHashSet<Function> getFunctions(Variable v) {
    return new LinkedHashSet<Function>(hv.get(v));
  }

  public LinkedHashSet<Function> getFunctions(Collection<Variable> v) {
    LinkedHashSet<Function> s = new LinkedHashSet<Function>();
    for (Iterator<Variable> i = v.iterator(); i.hasNext(); ) {
      Set<Function> f = hv.get(i.next());
      if (f != null) {
        s.addAll(f);
      }
      //s.addAll(hv.get(i.next()));
    }
    return s;
  }

  public double getValue() {
    double x = 1;
    for (Iterator<Function> i = hf.keySet().iterator(); i.hasNext(); ) {
      x *= i.next().getValue();
    }
    return x;
  }

  public double getLogValue() {
    double x = 0;
    for (Iterator<Function> i = hf.keySet().iterator(); i.hasNext(); ) {
      x += Math.log(i.next().getValue());
    }
    return x;
  }

  public Product subProduct(Collection<Variable> v) {
    Product p = new Product();
    p.add(getFunctions(v));
    Collection<Variable> u = p.getVariables();
    u.removeAll(v);
    p.removeVariables(u);
    return p;
  }

// Private data.

  public void fillIn(Variable u, Variable v) {
    if (contains(u) && contains(v)) {
      fills.connect(u, v);
    }
  }

  public void triangulate() {
    triangulate(new LinkedHashSet<Variable>());
  }

  public void triangulate(Collection<Variable> keep) {
    Network<Variable> g = new Network<Variable>(this);

    for (Variable u = null; (u = cheapest(g, keep)) != null; ) {
      Variable[] n = (Variable[]) g.getNeighbours(u).toArray(new Variable[0]);
      g.remove(u);
      for (int i = 0; i < n.length; i++) {
        for (int j = 0; j < i; j++) {
          if (!g.connects(n[i], n[j])) {
            g.connect(n[i], n[j]);
            fillIn(n[i], n[j]);
          }
        }
      }
    }
  }

  private Variable cheapest(Network<Variable> g, Collection<Variable> keep) {
    int best = Integer.MAX_VALUE;
    Variable u = null;
    for (Iterator<Variable> i = g.getVertices().iterator(); i.hasNext(); ) {
      Variable w = i.next();
      if (keep.contains(w)) {
        continue;
      }
      int ff = nfills(g, w);
      if (ff == 0) {
        return w;
      }
      if (ff < best) {
        best = ff;
        u = w;
      }
    }
    return u;
  }

  private int nfills(Network<Variable> g, Variable v) {
    Variable[] n = (Variable[]) g.getNeighbours(v).toArray(new Variable[0]);
    int count = 0;
    for (int i = 0; i < n.length; i++) {
      for (int j = 0; j < i; j++) {
        if (!g.connects(n[i], n[j])) {
          count++;
        }
      }
    }
    return count;
  }
}
