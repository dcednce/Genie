package alun.markov;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class CompoundVariable implements Variable {
  private Variable[] v = null;
  private int[] a = null;
  private int ii = 0;
  private int ns = 0;

  CompoundVariable(Collection<? extends Variable> vv) {
    Set<Variable> s = new LinkedHashSet<Variable>(vv);
    v = (Variable[]) s.toArray(new Variable[0]);
    a = new int[v.length];
    if (a.length > 0) {
      a[a.length - 1] = 1;
      for (int i = a.length - 2; i >= 0; i--) {
        a[i] = a[i + 1] * v[i + 1].getNStates();
      }
    }
    ns = 1;
    for (int i = 0; i < v.length; i++) {
      ns *= v[i].getNStates();
    }
  }

  /**
   * Test main.
   */

  public static void main(String[] args) {
    try {
      Collection<Variable> v = new java.util.LinkedHashSet<Variable>();
      for (int i = 0; i < 4; i++) {
        v.add(new BasicVariable(i + 2));
      }
      CompoundVariable c = new CompoundVariable(v);

      int i = 0;
      for (c.init(); c.next(); ) {
        System.out.print((i++) + " " + c.getStateIndex() + " :\t");
        for (Variable u : v) {
          System.out.print(" " + u.getState());
        }
        System.out.println();
      }

      for (i = 0; i < c.getNStates(); i++) {
        c.setStateIndex(i);
        System.out.print(i + " " + c.getStateIndex() + " :\t");
        for (Variable u : v) {
          System.out.print(" " + u.getState());
        }
        System.out.println();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public LinkedHashSet<Variable> getVariables() {
    LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
    for (int i = 0; i < v.length; i++) {
      s.add(v[i]);
    }
    return s;
  }

  public int getStateIndex() {
    int k = 0;
    for (int i = 0; i < v.length; i++) {
      k += a[i] * v[i].getStateIndex();
    }
    return k;
  }

  public void setStateIndex(int k) {
    try {
      for (int i = 0; i < v.length; i++) {
        v[i].setStateIndex(k / a[i]);
        k = k % a[i];
      }
    } catch (ArithmeticException e) {
      e.printStackTrace();

      System.err.println("=======================================================");
      System.err.println(this);
      for (int i = 0; i < a.length; i++) {
        System.err.print(a[i] + " ");
      }

      System.exit(1);
    }
  }

  public void init() {
    for (int i = 0; i < v.length; i++) {
      v[i].init();
    }
    ii = 0;
  }

// Private data.

  public boolean next() {
/*
		if (v == null || v.length == 0)
			return false;
*/
    while (ii >= 0) {
      if (!v[ii].next()) {
        ii--;
      }
      else {
        if (++ii == v.length) {
          ii--;
          return true;
        }
      }
    }
    ii = 0;
    return false;
  }

  public int getNStates() {
    return ns;
  }

  public int getState() {
    return getStateIndex();
  }

  public boolean setState(int k) {
    if (k >= 0 && k <= ns) {
      setStateIndex(k);
      return true;
    }
    return false;
  }

  public String toString() {
    return "COMPOUND " + getVariables().toString();
  }
}
