package alun.viewgraph;

import alun.util.CartesianPoint;

import java.awt.*;

public interface Mappable extends CartesianPoint {
  public boolean isMobile();

  public void setMobile(boolean m);

  public boolean contains(double x, double y);

  public void paint(Graphics g);

  public void setHighLighted(boolean b);
}
