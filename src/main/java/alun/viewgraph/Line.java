package alun.viewgraph;

import java.awt.*;

public class Line {
  protected Mappable a = null;
  protected Mappable b = null;
  private Color c = null;
  private int selfx = 50;
  private int selfy = 31;

// Private data.

  public Line(Mappable x, Mappable y, Color c) {
    a = x;
    b = y;
    setColor(c);
  }
  public Line(Mappable x, Mappable y) {
    this(x, y, Color.black);
  }

  public Color getColor() {
    return c;
  }

  public void setColor(Color cc) {
    c = cc;
  }

  public void paint(Graphics g) {
    g.setColor(getColor());
    if (a == b) {
      g.drawOval((int) a.getX(), (int) a.getY() - selfy, selfx, selfy);
    }
    else {
      g.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
    }
  }
}
