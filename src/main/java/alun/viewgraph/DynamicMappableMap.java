package alun.viewgraph;

import alun.graph.Graph;
import alun.graph.GraphFunction;

import java.awt.*;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class DynamicMappableMap<V extends Mappable> implements MappableGraph {
  private Graph<Mappable> g = null;

  public DynamicMappableMap(Graph<? extends V> gg) {
    g = (Graph<Mappable>) gg;
  }

  public Mappable getShowing(double x, double y) {
    for (Mappable m : g.getVertices()) {
      if (m.contains(x, y)) {
        return m;
      }
    }
    return null;
  }

  public void paint(Graphics g) {
    g.setColor(Color.black);

    for (Iterator<Mappable> i = getShownVertices().iterator(); i.hasNext(); ) {
      Mappable a = i.next();
      for (Iterator<Mappable> j = getShownNeighbours(a).iterator(); j.hasNext(); ) {
        Mappable b = j.next();
        if (a != b) {
          g.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
        }
      }
    }

    for (Iterator<Mappable> i = getAllVertices().iterator(); i.hasNext(); ) {
      i.next().paint(g);
    }
  }

  public void show(Mappable x) {
  }

  public void hide(Mappable x) {
  }

  public void show(Collection<? extends Mappable> x) {
  }

  public void hide(Collection<? extends Mappable> x) {
  }

  public Set<Mappable> getAllVertices() {
    return g.getVertices();
  }

  public Set<Mappable> getShownVertices() {
    return g.getVertices();
  }

  public Set<Mappable> getAllComponent(Mappable x) {
    return GraphFunction.reachables(g, x);
  }

  public Set<Mappable> getShownComponent(Mappable x) {
    return GraphFunction.reachables(g, x);
  }

  public Collection<Mappable> getAllNeighbours(Mappable x) {
    return g.getNeighbours(x);
  }

  public Collection<Mappable> getShownNeighbours(Mappable x) {
    return g.getNeighbours(x);
  }

  public Collection<Mappable> getShownOutNeighbours(Mappable x) {
    Set<Mappable> s = new LinkedHashSet<Mappable>(getShownNeighbours(x));
    for (Iterator<Mappable> i = getShownNeighbours(x).iterator(); i.hasNext(); ) {
      Mappable y = i.next();
      if (!g.connects(x, y)) {
        s.remove(y);
      }
    }
    return s;
  }

  public Collection<Mappable> getShownInNeighbours(Mappable x) {
    Set<Mappable> s = new LinkedHashSet<Mappable>(getShownNeighbours(x));
    for (Iterator<Mappable> i = getShownNeighbours(x).iterator(); i.hasNext(); ) {
      Mappable y = i.next();
      if (!g.connects(y, x)) {
        s.remove(y);
      }
    }
    return s;
  }

// Private data.

  public String toString() {
    return g.toString();
  }
}
