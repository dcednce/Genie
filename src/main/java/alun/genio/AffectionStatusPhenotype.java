package alun.genio;

/**
 * This represents the data for an individual at a locus
 * specified in the linkage affected status format.
 */
public class AffectionStatusPhenotype extends LinkagePhenotype {
  private static final int UNKNOWN = 0;
  private static final int AFFECTED = 2;

// Private data.
  private static final int UNAFFECTED = 1;
  public int status = 0;
  public int liability = 0;
  public int nliab = 0;
  /**
   * Creates a new phenotype from the given status and liability class numbers.
   * The data is checked for consistency with the data for the specified locus.
   */
  public AffectionStatusPhenotype(AffectionStatusLocus l, int stat, int liab) {
    setLocus(l);

    switch (status) {
      case UNKNOWN:
      case UNAFFECTED:
      case AFFECTED:
        status = stat;
        break;
      default:
        throw new LinkageException("Inappropriate affectation status " + status);
    }

    if (liab < 1 || liab > l.getLiabilities().length) {
      throw new LinkageException("Liability class out of range " + liab);
    }
    else {
      liability = liab;
    }

    nliab = l.getLiabilities().length;
  }

  /**
   * Returns a string representation of the data.
   */
  public String toString() {
    if (nliab > 1) {
      return f.format(status, 2) + " " + f.format(liability, 2);
    }
    else {
      return f.format(status, 2);
    }
  }
}
