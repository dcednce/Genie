package alun.genio;

import java.util.LinkedHashSet;

public class Family {
  private Object p = null;
  private Object m = null;
  private LinkedHashSet<Object> k = null;

  public Family() {
    k = new LinkedHashSet<Object>();
  }

  public Object getPa() {
    return p;
  }

  public void setPa(Object a) {
    p = a;
  }

  public Object getMa() {
    return m;
  }

  public void setMa(Object a) {
    m = a;
  }

  public Object[] getKids() {
    return k.toArray();
  }

  public int nKids() {
    return k.size();
  }

  public void addKid(Object a) {
    k.add(a);
  }

  public void removeKid(Object a) {
    k.remove(a);
  }
}
