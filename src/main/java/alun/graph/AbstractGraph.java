package alun.graph;

import java.util.*;

public abstract class AbstractGraph<V> implements MutableGraph<V>, DirectedGraph<V> {
  protected Map<V, Collection<V>> f = null;
  protected Map<V, Collection<V>> b = null;

  public abstract Collection<V> makeCollection();

  public boolean contains(Object x) {
    return f.containsKey(x);
  }

  public boolean connects(Object x, Object y) {
    Collection<V> n = f.get(x);
    return n == null ? false : n.contains(y);
  }

  public Set<V> getVertices() {
    return new LinkedHashSet(f.keySet());
  }

  public Collection<V> outNeighbours(Object x) {
    Collection<V> n = f.get(x);
    return n == null ? null : new ArrayList(n);
  }

  public Collection<V> inNeighbours(Object x) {
    Collection<V> n = b.get(x);
    return n == null ? null : new ArrayList(n);
  }

  public void clear() {
    f.clear();
    b.clear();
  }

  public void add(V x) {
    if (f.get(x) == null) {
      f.put(x, makeCollection());
    }
    if (b.get(x) == null) {
      b.put(x, makeCollection());
    }
  }

  public void remove(Object x) {
    Collection<V> ns = f.get(x);
    if (ns != null) {
      for (V n : ns) {
        b.get(n).remove(x);
      }
      f.remove(x);
    }

    if (f != b) {
      ns = b.get(x);
      if (ns != null) {
        for (V n : ns) {
          f.get(n).remove(x);
        }
        f.remove(x);
      }
    }
  }

  public void connect(V x, V y) {
    if (!contains(x)) {
      add(x);
    }
    if (!contains(y)) {
      add(y);
    }

    f.get(x).add(y);
    b.get(y).add(x);
  }

  public void disconnect(Object x, Object y) {
    if (contains(x) && contains(y)) {
      f.get(x).remove(y);
      f.get(y).remove(x);
    }
  }
}
