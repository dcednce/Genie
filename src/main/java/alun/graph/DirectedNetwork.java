package alun.graph;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class DirectedNetwork<V> extends Network<V> {
  public DirectedNetwork() {
    f = new LinkedHashMap<V, Collection<V>>();
    b = new LinkedHashMap<V, Collection<V>>();
  }

  public Collection<V> getNeighbours(Object x) {
    LinkedHashSet<V> s = new LinkedHashSet<V>();
    s.addAll(f.get(x));
    s.addAll(b.get(x));
    return s;
  }

  public V next(Object x) {
    Collection<V> c = f.get(x);
    return c == null || c.size() == 0 ? null : c.iterator().next();
  }
}
