package alun.graph;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class MaskedNetwork<V, E> implements MutableEdgedGraph<V, E> {
  private LinkedHashSet<V> show = null;

// Implement Graph interface
  private MutableEdgedGraph<V, E> hide = null;

  public MaskedNetwork(MutableEdgedGraph<V, E> g) {
    hide = g;
    show = new LinkedHashSet<V>();
    show.addAll(hide.getVertices());
  }

  public boolean contains(Object x) {
    return show.contains(x);
  }

  public boolean connects(Object x, Object y) {
    return show.contains(x) && show.contains(y) && hide.connects(x, y);
  }

// Implement EdgedGraph interface

  public LinkedHashSet<V> getVertices() {
    return new LinkedHashSet(show);
  }

  public LinkedHashSet<V> getNeighbours(Object x) {
    LinkedHashSet<V> s = new LinkedHashSet(hide.getNeighbours(x));
    if (s != null) {
      s.retainAll(show);
    }
    return s;
  }

// Implement MutableGraph interface

  public E connection(Object x, Object y) {
    if (show.contains(x) && show.contains(y)) {
      return hide.connection(x, y);
    }
    else {
      return null;
    }
  }

  public LinkedHashSet<E> getEdges() {
    LinkedHashSet<E> out = new LinkedHashSet<E>();
    for (Iterator<V> i = getVertices().iterator(); i.hasNext(); ) {
      V x = i.next();
      for (Iterator<V> j = getNeighbours(x).iterator(); j.hasNext(); ) {
        V y = j.next();
        out.add(connection(x, y));
      }
    }
    out.remove(null);
    return out;
  }

  public void clear() {
    hide.clear();
    show.clear();
  }

  public void add(V v) {
    hide.add(v);
    show.add(v);
  }

  public void remove(Object v) {
    hide.remove(v);
    show.remove(v);
  }

// Implement MutableEdgedGraph interface

  public void connect(V u, V v) {
    hide.connect(u, v);
    show.add(u);
    show.add(v);
  }

// Control showing and hiding

  public void disconnect(Object u, Object v) {
    hide.disconnect(u, v);
  }

  public void connect(V u, V v, E e) {
    hide.connect(u, v, e);
    show.add(u);
    show.add(v);
  }

  public void hide(V x) {
    show.remove(x);
  }

  public void show(V x) {
    if (hide.contains(x)) {
      show.add(x);
    }
  }

// Private data.

  public void hide(Collection<? extends V> x) {
    show.removeAll(x);
  }

  public void show(Collection<? extends V> x) {
    show.addAll(x);
    show.retainAll(hide.getVertices());
  }
}
