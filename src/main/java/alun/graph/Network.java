package alun.graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.StringTokenizer;

public class Network<V> extends AbstractGraph<V> {
  public Network() {
    f = new LinkedHashMap<V, Collection<V>>();
    b = f;
  }

  public Network(Graph<V> g) {
    this();
    GraphFunction.unite(this, g);
  }

  public static Network<String> read(BufferedReader b) throws IOException {
    Network<String> g = new Network<String>();
    for (String s = b.readLine(); s != null; s = b.readLine()) {
      StringTokenizer t = new StringTokenizer(s);
      String v = null;
      if (t.hasMoreTokens()) {
        v = t.nextToken();
      }
      g.add(v);
      while (t.hasMoreTokens())
        g.connect(v, t.nextToken());
    }
    return g;
  }

  public Collection<V> makeCollection() {
    return new LinkedHashSet<V>();
  }

  public Collection<V> getNeighbours(Object x) {
    return outNeighbours(x);
  }
}
