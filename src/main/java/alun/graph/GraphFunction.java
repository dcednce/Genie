package alun.graph;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Vector;

public class GraphFunction {
  public static <V> void addInducedSubgraph(MutableGraph<? super V> f, Collection<V> v, Graph<V> g) {
    for (Iterator<V> i = v.iterator(); i.hasNext(); ) {
      V x = i.next();
      if (g.contains(x)) {
        f.add(x);
        for (Iterator<V> j = g.getNeighbours(x).iterator(); j.hasNext(); ) {
          V y = j.next();
          if (v.contains(y) && g.connects(x, y)) // check connection in case of directed graphs
          {
            f.connect(x, y);
          }
        }
      }
    }
  }

  public static <V, E> void addInducedSubgraph(MutableEdgedGraph<? super V, ? super E> f, Collection<V> v, EdgedGraph<V, E> g) {
    for (Iterator<V> i = v.iterator(); i.hasNext(); ) {
      V x = i.next();
      if (g.contains(x)) {
        f.add(x);
        for (Iterator<V> j = g.getNeighbours(x).iterator(); j.hasNext(); ) {
          V y = j.next();
          if (v.contains(y) && g.connects(x, y)) // check connection in case of directed graphs
          {
            f.connect(x, y, g.connection(x, y));
          }
        }
      }
    }
  }

  public static <V> void unite(MutableGraph<? super V> f, Graph<V> g) {
    addInducedSubgraph(f, g.getVertices(), g);
  }

  public static <V, E> void unite(MutableEdgedGraph<? super V, ? super E> f, EdgedGraph<V, E> g) {
    addInducedSubgraph(f, g.getVertices(), g);
  }

  public static <V> void addComponent(MutableGraph<? super V> f, V x, Graph<V> g) {
    addInducedSubgraph(f, reachables(g, x), g);
  }

  public static <V> LinkedHashSet<V> reachables(Graph<V> g, V x) {
    LinkedHashSet<V> c = new LinkedHashSet<V>();
    Vector<V> v = new Vector<V>();

    v.add(x);
    for (int i = 0; i < v.size(); i++) {
      for (V w : g.getNeighbours(v.get(i))) {
        if (!c.contains(w)) {
          c.add(w);
          v.add(w);
        }
      }
    }

    return c;
  }
}
