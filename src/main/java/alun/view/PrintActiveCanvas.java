package alun.view;

import java.awt.*;

public class PrintActiveCanvas extends ActiveCanvas {
  private boolean showpage = false;
  private PageType pagatt = null;

  public PrintActiveCanvas(Paintable p) {
    super(p);
    pagatt = new PageType(PageAttributes.MediaType.LETTER, PageAttributes.OrientationRequestedType.PORTRAIT);
  }

  public void paint(Graphics g) {
    if (showpage) {
      g.setColor(Color.white);
      g.fillRect(0, 0, pagatt.getWidth(), pagatt.getHeight());
      g.setColor(Color.black);
      g.drawRect(0, 0, pagatt.getWidth(), pagatt.getHeight());
    }
    super.paint(g);
  }

// Private data, methods and classes.

  /**
   * Output a screen dump of the current image.
   */
  public void screenDump() {
    Toolkit t = Toolkit.getDefaultToolkit();
    if (t == null) {
      return;
    }

    Component c = this;
    while (!(c instanceof Frame))
      c = c.getParent();

    PrintJob j = t.getPrintJob((Frame) c, "Canvas screen dump", pagatt.getJobAttributes(), pagatt.getAttributes());
    if (j == null) {
      return;
    }

    Graphics g = j.getGraphics();
    super.paint(g);
    g.dispose();
    j.end();
  }

  /**
   * Set the current page for output.
   */
  public void setPage(PageType t) {
    if (t.getAttributes() == null) {
      showpage = false;
    }
    else {
      showpage = true;
      pagatt = t;
    }
  }
}
