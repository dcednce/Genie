package alun.view;

/**
 * A template class to handle mouse events, a combination of
 * a mouse adapter and a mouse motion adapter.
 */
public class Mouser extends MouseKey {
  private ViewingApplet target = null;

  public Mouser() {
  }

  public Mouser(ViewingApplet t) {
    target = t;
  }

// Private data

  protected ViewingApplet getTarget() {
    return target;
  }
}
