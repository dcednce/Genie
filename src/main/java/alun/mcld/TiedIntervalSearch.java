package alun.mcld;

import java.util.Collection;

public class TiedIntervalSearch extends IntervalSearch {
  public TiedIntervalSearch(Collection<Locus> l) {
    super(l);
  }

  public void propose() {

    x = (int) (Math.random() * loc.length);
    mid = loc[x].getMiddle();
    len = loc[x].getLength();

    g.remove(loc[x]);

    //loc[x].setRight(loc[x].getPosition()-1*Math.log(Math.random()));
    //loc[x].setLeft(loc[x].getPosition()+1*Math.log(Math.random()));

    //double b = 0.5 * (g.upperBound()-g.lowerBound());
    //loc[x].setRight(loc[x].getPosition()+b*Math.random());
    //loc[x].setLeft(loc[x].getPosition()-b*Math.random());

    double z = loc[x].getRight() - loc[x].getPosition();
    if (Math.random() < 0.5) {
      z += Math.log(Math.random());
    }
    else {
      z -= Math.log(Math.random());
    }
    if (z < 0) {
      z = -z;
    }
    loc[x].setRight(loc[x].getPosition() + z);

    z = loc[x].getPosition() - loc[x].getLeft();
    if (Math.random() < 0.5) {
      z += Math.log(Math.random());
    }
    else {
      z -= Math.log(Math.random());
    }
    if (z < 0) {
      z = -z;
    }
    loc[x].setLeft(loc[x].getPosition() - z);

    g.add(loc[x]);
  }
}
