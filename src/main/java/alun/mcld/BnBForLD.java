package alun.mcld;

import alun.markov.Function;
import alun.markov.Variable;
import alun.util.InputFormatter;

import java.util.Vector;

public class BnBForLD {
  private int i = 0;
  private double bound = 0;

// Private data.
  private Variable[] u = null;
  private Vector<Function> tab = null;
  private double[] x = null;
  public BnBForLD(LDModel l, double b) {
    tab = new Vector<Function>(l.getFunctions());

    u = new Variable[tab.size()];
    for (int i = 0; i < u.length; i++) {
      u[i] = tab.get(i).getVariables().iterator().next();
      u[i].init();
    }

    x = new double[u.length + 1];
    x[0] = 1;

    i = 0;

    bound = b;
  }

  public static void main(String[] args) {
    try {
      double bound = 0;
      switch (args.length) {
        case 1:
          bound = new Double(args[0]).doubleValue();
      }

      LDModel ldm = new LDModel(new InputFormatter());
      BnBForLD bnb = new BnBForLD(ldm, bound);

      double tot = 0;
      int count = 0;

      while (bnb.next()) {
        double x = ldm.getValue();
        tot += x;
        count++;
        for (Variable l : ldm.getLoci()) {
          System.out.print(l.getState());
        }
        System.out.println("\t" + x);
      }
      System.out.println("\n\t" + count + "\t" + tot);
    } catch (Exception e) {
      System.err.println("Caught in BnBForLd:main()");
      e.printStackTrace();
    }
  }

  public boolean next() {
    while (i >= 0) {
      if (!u[i].next()) {
        i--;
      }
      else {
        x[i + 1] = x[i] * tab.get(i).getValue();
        if (x[i + 1] > bound) {
          if (++i == u.length) {
            i--;
            return true;
          }
        }
      }
    }

    return false;
  }
}
