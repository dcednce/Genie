package alun.mcld;

import alun.markov.*;
import alun.util.InputFormatter;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Vector;

public class LDModel extends Product {
  private Vector<Variable> loci = null;

  public LDModel(Collection<? extends Variable> l) {
    loci = new Vector<Variable>();
    for (Variable ll : l) {
      loci.add(ll);
    }
  }

  public LDModel(InputFormatter f) throws IOException {
    loci = new Vector<Variable>();
    f.newLine();
    for (int i = 0; f.newToken(); i++) {
      loci.add(new Locus(i, f.getInt()));
    }

    while (f.newLine()) {
      LinkedHashSet<Variable> v = new LinkedHashSet<Variable>();
      while (f.newToken())
        v.add(loci.get(f.getInt()));
      f.newLine();

      Table t = new DenseTable(v);
      Variable[] u = (Variable[]) t.getVariables().toArray(new Variable[0]);
      if (u.length == 0) {
        t.setValue(f.nextDouble());
      }
      else {
        for (int i = 0; i < u.length; i++) {
          u[i].init();
        }
        for (int i = 0; i >= 0; ) {
          if (!u[i].next()) {
            i--;
          }
          else {
            if (++i == u.length) {
              t.setValue(f.nextDouble());
              i--;
            }
          }
        }
      }

      add(t);
    }
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      System.out.println(new LDModel(new InputFormatter()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public LDModel replicate(Collection<? extends Variable> sl) {
    Vector<Variable> nl = new Vector<Variable>(sl);
    LinkedHashMap<Variable, Variable> hash = new LinkedHashMap<Variable, Variable>();
    for (int i = 0; i < loci.size(); i++) {
      int x = loci.get(i).getNStates();
      int y = nl.get(i).getNStates();
      if (loci.get(i).getNStates() == nl.get(i).getNStates()) {
        hash.put(loci.get(i), nl.get(i));
      }
      else {
        throw new RuntimeException("State space size missmatch in LDModel:replicate()"
          + " " + loci.get(i).getNStates() + " " + nl.get(i).getNStates());
      }
    }

    LDModel ld = new LDModel(sl);
    for (Function f : getFunctions()) {
      DenseTable t = (DenseTable) f;
      LinkedHashSet<Variable> s = new LinkedHashSet<Variable>();
      for (Variable v : t.getVariables()) {
        s.add(hash.get(v));
      }
      DenseTable newt = new DenseTable(s, t.getArray());
      ld.add(newt);
    }

    return ld;
  }

  public LinkedHashSet<Variable> getLoci() {
    return new LinkedHashSet<Variable>(loci);
  }

// Private data.

  public void add(Function t) {
    if (loci.containsAll(t.getVariables())) {
      super.add(t);
    }
  }

  public String toString() {
    StringBuffer s = new StringBuffer();
    for (Variable l : loci) {
      s.append(" " + l.getNStates());
    }

    for (Function t : getFunctions()) {
      s.append("\n");
      Variable[] u = (Variable[]) t.getVariables().toArray(new Variable[0]);

      for (int i = 0; i < u.length; i++) {
        s.append(" " + ((Locus) u[i]).getIndex());
      }
      s.append("\n");

      if (u.length == 0) {
        s.append(" " + t.getValue());
      }
      else {
        for (int i = 0; i < u.length; i++) {
          u[i].init();
        }
        for (int i = 0; i >= 0; ) {
          if (!u[i].next()) {
            i--;
          }
          else {
            if (++i == u.length) {
              s.append(" " + t.getValue());
              i--;
            }
          }
        }
      }
    }

    return s.toString();
  }
}
