package alun.mcld;

import alun.genepi.Allele;
import alun.genepi.AlleleErrorPenetrance;
import alun.genepi.Error;
import alun.genepi.ErrorPrior;
import alun.genio.GeneticDataSource;
import alun.markov.GraphicalModel;
import alun.markov.Product;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Vector;

public class Diplotypes extends PerfectHaplotypes {
  private GeneticDataSource data = null;
  private int first = 0;
  private Product prod = null;
  private Vector<Allele> pats = null;
  private Vector<Allele> mats = null;

// Private data.
  private Vector<AlleleErrorPenetrance> pen = null;
  public Diplotypes(GeneticDataSource dat, double errprob) throws IOException {
    this(dat, errprob, 0);
  }
  public Diplotypes(GeneticDataSource dat, double errprob, int frst) throws IOException {
    data = dat;
    first = frst;

    v = new Vector<int[]>();
    for (int i = 0; i < dat.nIndividuals(); i++) {
      v.add(new int[data.nLoci() - first]);
      v.add(new int[data.nLoci() - first]);
    }

    pats = new Vector<Allele>();
    mats = new Vector<Allele>();
    pen = new Vector<AlleleErrorPenetrance>();

    prod = new Product();
    for (int j = first; j < data.nLoci(); j++) {
      Error e = new Error();
      prod.add(new ErrorPrior(e, errprob));

      Allele p = new Allele(data.nAlleles(j));
      Allele m = new Allele(data.nAlleles(j));
      AlleleErrorPenetrance aep = new AlleleErrorPenetrance(p, m, e, null);
      prod.add(aep);

      pen.add(aep);
      pats.add(p);
      mats.add(m);
    }

    locs = new LinkedHashSet<Locus>();
    for (int j = first; j < data.nLoci(); j++) {
      Locus ll = new Locus(j - first, dat.nAlleles(j), (1.0 + j - first));
      ll.setName("" + j);
      locs.add(ll);
    }

    update(null);
  }

  /**
   * Test main.
   */
  public static void main(String[] args) {
    try {
      alun.genio.LinkageDataSet x = new alun.genio.LinkageDataSet(args[0], args[1]);
      GeneticDataSource d = new alun.genio.LinkageInterface(x);
      System.out.println(new Diplotypes(d, 0));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public boolean update(LDModel m) {
    return update(m, true);
  }

  public boolean maximize(LDModel m) {
    return update(m, false);
  }

  public boolean update(LDModel model, boolean sample) {
    Product ldprod = new Product();
    ldprod.addProduct(prod);
    if (model != null) {
      ldprod.addProduct(model.replicate(pats));
      ldprod.addProduct(model.replicate(mats));
    }

    ldprod.triangulate();
    GraphicalModel gm = new GraphicalModel(ldprod);
    gm.allocateOutputTables();
    gm.allocateInvolTables();

    for (int i = 0; i < data.nIndividuals(); i++) {
      for (int j = 0; j < pen.size(); j++) {
        pen.get(j).fix(data.penetrance(i, j + first));
      }

      gm.collect();

      if (sample) {
        gm.drop();
      }
      else {
        gm.max();
      }

      for (int j = 0; j < pen.size(); j++) {
        v.get(2 * i)[j] = pats.get(j).getState();
        v.get(2 * i + 1)[j] = mats.get(j).getState();
      }
    }

    gm.clearOutputTables();
    gm.clearInvolTables();

    return true;
  }
}
