import alun.genio.LinkageFormatter;
import alun.genio.LinkagePedigreeData;
import alun.genio.MarriageNodeMap;
import alun.view.FrameQuitter;
import alun.viewgraph.DagMover;
import alun.viewgraph.DynamicMappableMap;
import alun.viewgraph.MapEditor;
import alun.viewgraph.Mappable;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This is an interactive graphical program for viewing pedigree data.
 *
 * <ul>
 * Usage : <b> java ViewLinkPed < input.ped [kindred1.id] [kindred2.id] ... </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file. </li>
 * <li> <b> kindred1.id kindred2.id ... </b> is an optional list of kindred identifiers specifying which
 * kindreds to include in the viewer. The default is to include all the kindreds in the file. </li>
 * </ul>
 * <p>
 * The controls are similar to those for
 * <a href="ViewGraph.html"> ViewGraph. </a>
 */

public class ViewLinkPed {
  public static void main(String[] args) {
    try {
      LinkageFormatter lf = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Ped file");
      LinkagePedigreeData p = new LinkagePedigreeData(lf, null, false);
      LinkagePedigreeData[] ped = p.splitByPedigree();

      MarriageNodeMap map = null;
      switch (args.length) {
        case 0:
          map = new MarriageNodeMap(p.getPedigree());
          break;
        default:
          for (int j = 0; j < args.length; j++) {
            int pedid = new Integer(args[j]).intValue();
            for (int i = 0; i < ped.length; i++) {
              if (pedid == ped[i].pedid()) {
                map = new MarriageNodeMap(ped[i].getPedigree());
              }
            }
          }
          break;
      }

      MapEditor ed = new MapEditor();
      ed.init();
      ed.setMover(new DagMover());
      ed.setMap(new DynamicMappableMap<Mappable>(map));

      Frame f = new Frame();
      f.addWindowListener(new FrameQuitter());
      f.add(ed);
      f.pack();
      f.setVisible(true);
      ed.start();
    } catch (Exception e) {
      System.err.println("Caught in ViewLinkPed:main()");
      e.printStackTrace();
    }
  }
}
