import alun.genepi.*;
import alun.genio.LinkageDataSet;
import alun.genio.LinkageInterface;
import alun.mcld.LDModel;
import alun.mcld.LDSampler;
import alun.util.InputFormatter;
import alun.util.StringFormatter;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * This program implements the methods of
 * Thomas, A., Gutin, A., Abkevich, V. &amp; Bansal, A.  (2000), Multipoint linkage
 * analysis by blocked Gibbs sampling, <em>Statistics and Computing</em> <b>
 * 10</b>,&nbsp;259-269
 * in order to calculate linkage statistics using Markov chain Monte Carlo methods.
 *
 * <ul>
 * Usage <b> java McLink input.par input.ped [n_simulations] [-v/-t] [ld.model] </b>
 * </ul>
 * <p>
 * where
 * <ul>
 * <li> <b> input.par </b> is a LINKAGE parameter file </li>
 * <li> <b> input.ped </b> is a LINKAGE pedigree file </li>
 * <li> <b> n_simulations </b> is the number of MCMC samples to use in the calculations.
 * The default is 1000. </li>
 * <li> If <b> -v </b> option is specified lod scores are output after each iteration which
 * may be useful in tracking the performance of the sampler. If <b> -t </b> is
 * specified there is no intermediate output. <b> -t </b> is the default. </li>
 * <li> <b> ld.model </b> is an optional file specifying a graphical model for linkage disequilibrium.
 * Such a file can be produced using HapGraph if a population sample of genotyped
 * individuals is available.
 * </ul>
 *
 * <p>
 * The assumption is that McLink will be used without an LD model when the data are
 * micro satellite genotypes and with an LD model when the data are SNPs, hence, there
 * are differences in what is assumed and what is possible.
 * <p>
 * Run without a specified linkage disequilibrium model:
 * <ul>
 * <li> primarily for use with micro satellite markers. </li>
 * <li> loci are assumed to be in linkage equilibrium. </li>
 * <li> any genotyping errors that lead to Mendelian inheritance
 * inconsitencies will crash the program. Check the data with GMCheck first. </li>
 * <li> space of possible genotype states at each locus is downcoded to speed up the
 * computations. </li>
 * <li> the sampler used is an extended LM-sampler which has better mixing properties. </li>
 * </ul>
 * <p>
 * Run with a specified linkage disequilibrium model:
 * <ul>
 * <li> primarily for use with SNP genoypes. </li>
 * <li> specified LD model is assumed. </li>
 * <li> genotyping error is modelled assuming an error rate of 1%
 * and that the probability of error is independent of the actual
 * genotype. The assumption here is that SNP loci are individially relatively
 * uninformative and there are too many of them to make error checking and
 * correction feasible. </li>
 * <li> no downcoding of the state space is done. </li>
 * <li> the sampler used is an L-sampler since the LM-sampler is not possible
 * with an LD model. The mixing properties are probably not great. </li>
 * </ul>
 *
 * <p>
 * In the output, for each kindred in the pedigree file there is a matrix
 * of TLOD, or complex lod score statistics, with a row for each locus and
 * a column for each value of the recombination fraction in
 * {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1}.
 */

public class McLink {
  public static void main(String[] args) {
    try {
      double[] thetas = {0.0, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1};
      int wherehalf = 6;
      boolean report = false;
      int n_samples = 1000;
      LinkageSampler mc = null;
      int sampler = 2;
      int[] randups = {3}; //{3,4,5,6};
      LDModel ldmod = null;
      double errorprob = 0.01;

      LinkageDataSet[] rawdata = null;

      switch (args.length) {
        case 5:
          ldmod = new LDModel(new InputFormatter(new BufferedReader(new FileReader(args[4]))));

        case 4:
          if (args[3].equals("-v")) {
            report = true;
          }
          else if (args[3].equals("-t")) {
            report = false;
          }

        case 3:
          n_samples = new Integer(args[2]).intValue();

        case 2:
          LinkageDataSet dt = new LinkageDataSet(args[0], args[1]);
          rawdata = dt.splitByPedigree();
          break;

        default:
          System.err.println("Usage: java McLinke input.par input.ped [n_samples] [-v/-t] [ld_model]");
          System.exit(1);
      }

      for (int dat = 0; dat < rawdata.length; dat++) {
        LinkageVariables vars = null;

        if (ldmod == null) {
          rawdata[dat].downCode(false);
          vars = new LinkageVariables(new LinkageInterface(rawdata[dat]));

          switch (sampler) {
            case 0:
              mc = new IndependentLocusSampler(vars);
              break;
            case 1:
              mc = new LinkedLocusSampler(vars);
              break;
            case 2:
              mc = new LocusMeiosisSampler(vars, 2);
              break;
            case 3:
              mc = new ExtendedLMSampler(vars, randups);
              break;
          }
        }
        else {
          vars = new LinkageVariables(new LinkageInterface(rawdata[dat]), errorprob);
          mc = new LDSampler(vars, 1, ldmod);
        }

        Tlods tl = new Tlods(vars, thetas);
        double[][] lods = new double[vars.nLoci()][tl.nThetas()];

        for (int i = 1; i <= n_samples; i++) {
          mc.sample();

          System.err.print("'");

          for (int j = 1; j < lods.length; j++) {
            double[] lik = tl.twoPointLikelihood(j);
            for (int k = 0; k < lik.length; k++) {
              lods[j][k] += lik[k] / lik[wherehalf];
            }

            if (report) {
              for (int k = 0; k < lods[j].length; k++) {
                System.out.print("\t" +
                  StringFormatter.format(Math.log(lods[j][k] / (i + 1)) / Math.log(10), 2, 4));
              }
              System.out.println();
            }
          }

          System.err.print(".");

          if (report) {
            System.out.println();
          }
          if (i % 100 == 0) {
            System.err.println("\n" + i);
          }
          else if (i % 10 == 0) {
            System.err.println();
          }
        }
        System.err.println();

        System.out.println("Pedigree " + dat + ": " + rawdata[dat].getPedigreeData().pedid());
        for (int j = 1; j < lods.length; j++) {
          for (int k = 0; k < lods[j].length; k++) {
            lods[j][k] = Math.log(lods[j][k] / n_samples) / Math.log(10);
            System.out.print(StringFormatter.format(lods[j][k], 2, 4) + " ");
          }
          System.out.println();
        }

        System.out.println();
      }
    } catch (Exception e) {
      System.err.println("Caught in McLink:main()");
      e.printStackTrace();
    }
  }
}
