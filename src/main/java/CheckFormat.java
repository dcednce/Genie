import alun.genio.LinkageDataSet;

import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * This program reads in LINKAGE paramter and pedigree files, checks that
 * they are readable and then outputs the checked files. Error and warning
 * messages are printed to the screen. The checks made by this program are
 * made whenever data is input.
 *
 * <p>
 * See my <a href="linkage.html"> LINKAGE format page </a> for information
 * about the way this suite of programs uses this format, including the
 * difference between "pre-makeped" and standard "post-makeped" formats.
 *
 * <ul>
 * Usage : <b> java CheckFormat input.par input.ped [output.par] [output.ped] [-pre] </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> input.par </b> is the input LINKAGE paramter file. </li>
 * <li> <b> input.ped </b> is the input LINKAGE pedigree file. </li>
 * <li> <b> output.par </b> is the checked output LINKAGE parameter file.
 * The default is to write to standard output.</li>
 * <li> <b> output.ped </b> is the checked output LINKAGE pedigree file.
 * The default is to write to standard output.</li>
 * <li> <b> -pre </b> If this option is specified the data is assumed to be in
 * so called "pre-makeped" format and interpreted appropritately.
 * The output will be in regular format not "pre-makeped" format.  </li>
 * </ul>
 */

public class CheckFormat {
  public static void main(String[] args) {
    try {
      boolean premake = false;
      PrintWriter parout = new PrintWriter(System.out);
      PrintWriter pedout = new PrintWriter(System.out);

      switch (args.length) {
        case 5:
          premake = args[4].equals("-pre");

        case 4:
          if (args[3].equals("-pre")) {
            premake = true;
          }
          else {
            pedout = new PrintWriter(new FileWriter(args[3]));
          }

        case 3:
          if (args[2].equals("-pre")) {
            premake = true;
          }
          else {
            parout = new PrintWriter(new FileWriter(args[2]));
          }

        case 2:
          break;

        default:
          System.err.println("Usage: java CheckFormat input.par input.ped [output.par] [output.ped] [-pre]");
          System.exit(1);
      }

      LinkageDataSet x = new LinkageDataSet(args[0], args[1], premake);

      parout.println(x.getParameterData());
      parout.flush();
      pedout.println(x.getPedigreeData());
      pedout.flush();
    } catch (Exception e) {
      System.err.println("Caught in CheckFormat:main().");
      System.err.println("Likely to be an unexpected type of data formatting error.");
      e.printStackTrace();
    }
  }
}
