import alun.genio.LinkageFormatter;
import alun.genio.LinkageIndividual;
import alun.genio.LinkagePedigreeData;
import alun.genio.MarriageNodeMap;
import alun.view.FrameQuitter;
import alun.viewgraph.DynamicMappableMap;
import alun.viewgraph.MapEditor;
import alun.viewgraph.Mappable;
import alun.viewgraph.NewerDagMover;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * This is an interactive graphical program for viewing pedigree data.
 *
 * <ul>
 * Usage : <b> java ViewPed < triplet.file </b> </li>
 * </ul>
 * where
 * <ul>
 * <li> <b> triplet.file </b> is a file specifiying the pedigree as a list
 * of triplet. One triplet per line. Order is assumed to be individual id,
 * father's id, mother's id. Any other data following this is ignored.  </li>
 * </ul>
 *
 * <p>
 * The controls are the same as those for
 * <a href="ViewLinkPed.html"> ViewLinkPed. </a>
 */

public class ViewPed {
  public static void main(String[] args) {
    try {
      LinkageFormatter lf = new LinkageFormatter(new BufferedReader(new InputStreamReader(System.in)), "Ped file");
      LinkagePedigreeData p = new LinkagePedigreeData(lf, null, LinkageIndividual.TRIPLET);
      MarriageNodeMap map = new MarriageNodeMap(p.getPedigree());

      MapEditor ed = new MapEditor();
      ed.init();
      ed.setMover(new NewerDagMover());
      ed.setMap(new DynamicMappableMap<Mappable>(map));

      Frame f = new Frame();
      f.addWindowListener(new FrameQuitter());
      f.add(ed);
      f.pack();
      f.setVisible(true);
      ed.start();
    } catch (Exception e) {
      System.err.println("Caught in ViewPed:main()");
      e.printStackTrace();
    }
  }
}
