# Genie Contact Information

For any suggestions or requests to PedGenie, please let us know.

Nicola J. Camp, PhD  
Professor, Internal Medicine  

Primary Children’s and Families’ Cancer Research Center  
Huntsman Cancer Institute, Rm 4757  
2000 Circle of Hope  
Salt Lake City, UT 84112  
801-587-9351  
nicola.camp@hci.utah.edu

[Home](genieindex.html)
