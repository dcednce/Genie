# Weighting Algorithm Explanation

The purpose of the weighting algorithm is to calculate weights for all genotyped individuals that can be used in subsequent analyses to account for their relatedness. The gene-drop approach we use to calculate the weights allows us to consider all the relationships between individuals simultaneously when determining the weights, and hence the weights are more accurate than pair-wise methods (that may over-correct when there are several pairs from the same pedigree.) The weights are determined as follows:

  **1.** unique alleles are asigned to all founders (For example, the first founder is given alleles 1 and 2, the second founder 3 and 4, the third founder 5 and 6 and so forth.)  

  **2.** a gene-drop is performed (simulated Mendelian inheritance).

  **3.** after each gene-drop, weights for genotyped individuals are calculated. For each of the alleles observed in an individuals, the number of times those alleles appear in total across all genotyped individuals is counted. the weightf for the individual is the average of the reciprocals of the counts for each of their alleles,

  **4.** repeat steps 1-3 n times, as specified by the users. Final weights are the average of all the gene-drop weights across simulations.

[Home](index.html)   [.rgen Parameter File](rgenDescription.html)
