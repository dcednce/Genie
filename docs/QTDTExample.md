# QTDT Interface Example

Running the PedGenie - QTDT interface requires, in addition to the standard PedGenie files ([.rgen file](rgenDesc.html) and a [pedigree file](pedigreeDesc.html)), a [QTDT data file](http://www.sph.umich.edu/csg/abecasis/QTDT/tour/tour1.html) and a kornshell script that communicates between PedGenie, the modified QTDT and QTDTpval packages. A quantitative [trait file](traitDesc.html) is also required. If you have problems opening a file, at the error page try using toolbar _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Files required to run this example are :

[PhasedPedigree.dat](PhasedPedigree.dat) - Linkage pre-makeped pedigree file with a header line.  
[TDT.rgen](TDT.rgen) - .rgen Parameter File for Quantitative Trait Data  
[Trait.dat](Trait.dat) - Trait / Covariate Data File  
[QTDT\_data.dat](QTDT_data.dat) - QTDT specific data file  
[PhasedPedigree.TDT\_0.ped](PhasedPedigree.TDT_0.ped) - PedGenie Pedigree dump file  
[runQTDT.ksh](runQTDT.ksh) - Example kornshell script that runs PedGenie and QTDTpval packages

PedGenie output file

[TDT.report](TDT.report)

QTDTpval output file

[PhasedPedigree.TDT.out.pval](PhasedPedigree.TDT.out.pval)

To run this analysis:

_runQTDT.ksh TDT.rgen PhasedPedigree.dat QTDT\_data.dat 2000 -am_

**TDT.rgen**  
The set-up of TDT.rgen is similar to the set-up of the [SingleLocus.rgen](SingleLocus.rgen). Only differences between the two files will be described in this section.

1.  Under "optional modules" note that the Quantitative statistic must be included for the QTDT analysis. As part of the quantitative statistic, the trait file name and selected column(s) of data to pull (i.e., covar1) must also be included.
2.  Under "optional modules", the "dumper" must be listed as TDTDumper. The TDTDumper will output, in the current directory, pedigree files containing the newly generated genotype data and the quantitative trait(s). The number of newly generated pedigree files will be the same as the number of simulations. These files will be the input files for the QTDT package.
3.  Under "optional modules", we have selected the transmission/disequilibrium tests as the statistics. Any of the available PedGenie statistics may be listed here if the user wishes to run the QTDT interface.

**QTDT\_data.dat**  
The QTDT package needs a QTDT specific data file to run. See [QTDT web site](http://www.sph.umich.edu/csg/abecasis/QTDT/tour/tour1.html) for QTDT data file format instructions.

**PhasedPedigree.TDT\_0.ped**  
The TDTDumper outputs the same number of newly generated pedigree files as the number of simulations, differentiated by the number proceeding the '.ped'. Here we present one example of the newly generated dumped pedigree files. The file with the '0.ped' extension contains the observed genotype data and the quantitative trait data. Note that the liability column has been removed, as has the header; the format required by the QTDT package.

**TDT.report**  
The TDT.report file contains output from PedGenie similar to the SingleLocus.report. Please see [SingleLocus.report](SingleLocus.report) for a description of the output. Note that the output from the QTDT package is NOT contained in the TDT.report file.

**[PhasedPedigree.TDT.out.pval](PhasedPedigree.TDT.out.pval.html)**  
The PhasedPedigree.TDT.out.pval file contains the observed statistic and empirical p value for the quantitative TDT model selected.

1.  The file begins with the data/time run, the input data files, the number of simulations and the QTDT model selected. The three QTDT permissible models are: -aa for the Allison model, -ar for the Rabinowitz model, and -am for the Monks model.
2.  If more than a single trait is listed in the quantitative trait file, each trait will be analyzed separately. Here we see results for the trait: Quant, analyzed for each marker separately. The observed statistic is listed, as is the empirical p value. The QTDT package has requirements for the number of subjects and heterozygosity of parents needed to run a particular model. Please see the [QTDT web site](http://www.sph.umich.edu/csg/abecasis/QTDT/tour/tour1.html) for specific requirements.

**runQTDT.ksh**  
The runQTDT.ksh is a kornshell script that runs the QTDT interface. It runs PedGenie, links the output form PedGenie to the QTDT package, and computes an empirical p value.

**To run runQTDT.ksh:**  
_runQTDT.ksh <RGEN file.rgen> <pedigree file.dat> <QTDT specific data file> <# simulations> <QTDT model>_  
  
The permissible QTDT models are: -aa for Allison, -ar for Rabinowitz, and -am for Monks models.

[Home](index.html)   [QTDT Interface](QTDTinterface.html)
