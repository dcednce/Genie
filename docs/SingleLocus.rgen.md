# Single Locus .rgen example

<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE ge:rgen SYSTEM "ge-rgen.dtd">

<ge:rgen rseed="777" nsims="2000" top="AlleleFreqTopSim" drop="DropSim">
<!-- Note:  2000 simulation run, alleles will be dropped from from founders-->

  <ge:locus id="1" marker="SNP1"/>  
  <ge:locus id="2" marker="SNP2"/>  
  
  <!-- optional modules -->
  <ge:param name="ccstat1">ChiSquared</ge:param>
  <ge:param name="ccstat2">ChiSquaredTrend</ge:param>
  <ge:param name="ccstat3">OddsRatios</ge:param>
  <ge:param name="ccstat4">Quantitative</ge:param>
  <ge:param name="quantfile">Trait.dat</ge:param>
  <ge:param name="covar1">1</ge:param>
  <ge:param name="dumper">.LineRecsDumper</ge:param> <!-- option: TDTDumper for QTDT analysis-->

  <!-- other optional parameters -->
  <ge:param name="top-sample">all</ge:param>  <!-- options: all, founder, GeneCountAlleles -->

<!-- SNP1-->

 <!-- Dominant Model for mutant-->
 <!-- Note: This analysis is set up to run the first locus listed above or SNP1. -->
 <!-- Chi-Square, Odds ratio, and Quantitative tests using the first covariate -->
 <!-- from the Trait.dat file will be run.  The model name name is reported -->
 <!-- in the output file and is for user convenience.  Weights (wt) define the model.-->
 <!-- The '.' is a wild parameter and refers to any non-zero value not previously defined.-->
 <!-- The '|' means 'or' allowing unphased data to be analyzed.-->

 <ge:cctable loci="1" stats="1 3 4" model="Dom">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/.)|(./2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>
 
<!-- Recessive Model for mutant-->
 <ge:cctable loci="1" stats="1 3 4" model="Rec">
    <ge:col wt="0">
      <ge:g>
        <ge:a>(1/.)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>2/2 </ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

<!-- Additive test, weighting number of 2's-->
<!-- Note: no stats are listed, PedGenie will run all stats in ccstat list above -->

<ge:cctable loci="1" model="Additive">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/1)|(1/2)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="2">
      <ge:g>
        <ge:a>(2/2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Allele test -->
<!-- Note: The default for 'type' is 'genotype', but here we are running -->
<!-- an allele test (heterozygotes counted once, homozygotes counted twice) -->
<!-- PedGenie matches the pattern listed for each weight.-->

<ge:cctable loci="1" type="Allele" stats="1 3 4" model="Allele">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>2</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- SNP2-->

 <!-- Dominant Model for mutant-->
 <ge:cctable loci="2" stats="1 3 4" model="Dom">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/.)|(./2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>
 
<!-- Recessive Model for mutant-->
 <ge:cctable loci="2" stats="1 3 4" model="Rec">
    <ge:col wt="0">
      <ge:g>
        <ge:a>(1/.)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>2/2 </ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

<!-- Additive test, weighting number of 2's-->
<ge:cctable loci="2" model="Additive">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/1)|(1/2)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="2">
      <ge:g>
        <ge:a>(2/2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Alele,  weighting number of 2's-->
<ge:cctable loci="2" type="Allele" stats="1 3 4" model="Allele">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>2</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


</ge:rgen>
