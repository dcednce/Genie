# Single Locus GeneCountAlleles Example

The Single Locus GeneCountAlleles example is similar to the Single Locus Example, but the allele frequency estimation method GeneCounter from [GeneCountAlleles](http://bioinformatics.med.utah.edu/~alun/software/index.html) program is used. If you have problems opening a file, at the error page try using toolbar _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Files required to run this example are :

[PhasedPedigree.dat](PhasedPedigree.dat) - Linkage pre-makeped pedigree file with a header line  
[PhasedPedigree.par](PhasedPedigree.par) - Linkage Loci data file  
[Trait.dat](Trait.dat) - Trait / Covariate data file  
[SingleGeneCount.rgen](SingleGeneCount.rgen) - .rgen Parameter File

Output file :

[SingleGeneCount.report](SingleGeneCount.report)

To run this analysis:

_java -jar Genie.jar PedGenie SingleGeneCount.rgen_

**PhasedPedigree.dat**  
The example [pedigree file](pedigreeDesc.html) is composed of 16 three generation families. Each individual has information on two markers.

**Trait.dat**  
The quantitative test requires a quantitative [trait file](traitDesc.html). It contains the trait and covariate information for each individual.

**SingleGeneCount.rgen**  
The SingleGeneCount.rgen is exactly the same as the [SingleLocus.rgen](SingleLocus.rgen) file except the simulation method top="GeneCounterTopSim" replaces the top="AlleleFreqTopSim" method. Please see notes on SingleLocus.rgen for other details.

**PhasedPedigree.par**  
This file is equivalent to a linkage data file. PedGenie uses allele frequencies from this file to statistically infer the genotypes of founders in a pedigree. Other lines in this file are required but ignored. Please see [Alun Thomas web site](http://bioinformatics.med.utah.edu/~alun/software/index.html) for more information about this file.

**[SingleGeneCount.report](SingleGeneCount.report)** output file

We refer users to the [SingleLocus.report](SingleLocus.report) output for detailed information regarding the SingleGeneCount.report output file. We point out that the observed statistics for the SingleLocus.report and the SingleGeneCount.report files are the same for every analysis, as would be expected. However, the empirical p values differ between the two analyses because different allele frequency estimation methods were used.

[Home](index.html)  [PedGenie](PedGenieDetail.html)  [PedGenie Examples](PedGenieExamples.html)
