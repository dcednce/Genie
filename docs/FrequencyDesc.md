# Frequency file description

The frequency file contains a list of user defined haplotype or allele
frequencies followed by multiple loci or a single locus to gene drop
for generating the empirical null distribution. Each locus within a
haplotype should match the sub-element **Locus** order in the .rgen
file. The sum of the frequencies should equal 1.0. If you want to
enter your own allele frequencies (eg., a multiallelic locus or
biallelic locus), only a single marker should be defined in the
sub-element **Locus** of the .rgen file, and the allele frequency sum
should equal 1.0.

[Home](index.html) [Haplotype Frequency Example File](data/PhasedPedigree.hap)
