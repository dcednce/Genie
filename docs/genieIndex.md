# Genie Home Page

* Genie

Genie is a general-purpose tool to analyze association and transmission disequilibrium
(TDT) between genetic markers and traits in studies of families and independent
individuals. It implements 3 analysis options. [PedGenie](PedGenieDetail.html) for
performing genetic association analysis in studies of families of arbitury sizes and
structure, and independent cases and controls. [hapMC](hapMCDetail.html) for performing
analysis on haplotype data. It uses Monte Carlo procedure and Expectation-Maximization
estimates of haplotypes to provide valid haplotype tests for various standard association
statistics. [hapConstructor](hapConstructor.html) for exploring multi-locus associations
in candidate genes and regions. It automatically builds multi-locus SNP sets to test for
association in a case-control framework.

[What's new in](whatsNew.html)

3.0.0.1

[PedGenie](PedGenieDetail.html)

PedGenie Descritpion and Functionality

[hapMC](hapMCDetail.html)

hapMC Description and Information

[hapConstructor](hapConstructor.html)

hapConstructor Description and Information

[Requirements](Requirements.html)

Java runtime, Genie.jar, .rgen parameter file and pedigree file(s)  
optional files : allele or haplotype frequency file; quantitative data file and linkage parameter file

How to execute

java -jar Genie.jar analysis\_option rgenfile\[.rgen\]

Output Report

Output Report written to  
\[.rgen file name\].report or \[.rgen file name\].summary file.

[.rgen Parameter File](rgenDescription.html)

Detailed description of .rgen XML file

[Example Files](Examples.html)

All PedGenie, hapMC, hapConstructor and QTDT example files

[QTDT Interface](QTDTinterface.html)

PedGenie / QTDT Interface Package

[download](download.html)

Download Genie and QTDT Interface packages

[To Reference Genie](ToReferenceGenie.html)

Reference information

Registration

Please send name and email address to [Steven Backus](mailto:backus@genepi.med.utah.edu)

Last update : 28 Aug 2020  
[Publications](Publications.html)    [References](References.html)    [Bugs](buglist.html)    [FAQ](FAQ.html)    [Disclaimer](http://www.utah.edu/disclaimer/index.html)    [Contact/Requests](contact.html)
