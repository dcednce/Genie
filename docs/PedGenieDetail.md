# PedGenie Description of Functionality

PedGenie is now part of our new [Genie](index.html) package. It functions as an analysis option of the new package.

**Description**  
When performing an association analysis using family data, one must account for the fact that individuals within a family are related; hence data are correlated. The computed test statistic will be correct; however, the variance will be underestimated and the significance overestimated due to correlations between individuals. PedGenie calculates an empirical p-value for a test statistic using a Monte Carlo approach. Genotypes or haplotypes of individuals within a pedigree are randomly simulated in proportion to a defined allele or haplotype frequency, while still maintaining the original pedigree structure.

**Functionality**  
PedGenie is capable of analyzing dichotomous and quantitative dependent variables. The statistics that PedGenie uses are the standard statistics (as provided by the links on the following websites) as would be used in a sample of independent individuals or independent nuclear families. PedGenie accounts for familial relationships by correcting the significance (p-value) of a statistic through an empirical approach, but leaves the statistic itself unchanged.  

The following tests can be performed:

**Dichotomous Outcome**

Genotype-based [Chi-squared test](http://mathworld.wolfram.com/Chi-SquaredTest.html)

Allele-based [Chi-square test](http://mathworld.wolfram.com/Chi-SquaredTest.html)

Genotype-based [Chi-square trend test](References.html#Zar)

Genotype-based with Multiple studies [CMH Chi-squared test](http://v8doc.sas.com/sashtml/stat/chap28/sect27.htm)

Genotype-based with Multiple studies [CMH Chi-squared trend test](http://v8doc.sas.com/sashtml/stat/chap28/sect27.htm)

Genotype-based [odds ratio](http://en.wikipedia.org/wiki/Odds_ratios)

Allele based [odds ratio](http://en.wikipedia.org/wiki/Odds_ratios)

Genotype-based [odds ratio with confidence intervals](http://en.wikipedia.org/wiki/Odds_ratios)

Allele based [odds ratio with confidence intervals](http://en.wikipedia.org/wiki/Odds_ratios)

Genotype-based with Multiple studies [meta odds ratio](References.html#Kirkwood)

Genotype-based with Multiple studies [meta odds ratio with confidence intervals](References.html#Kirkwood)

Genotype-based with Multiple studies - meta gene-gene 2x2 interactions odds ratio

Genotype-based with Multiple studies - meta gene-gene 2x2 interactions odds ratio with confidence intervals

Trio TDT [(Spielman et al 1993)](References.html#Spielman1993)

Sibs TDT [(Spielman and Ewens 1998)](References.html#Spielman1998)

Combined Trio/Sibs TDT [(Spielman and Ewens 1998)](References.html#Spielman1998)

Genotype-based [Hardy-Weinburg Equilibrium](http://en.wikipedia.org/wiki/Hardy-Weinberg_principle)

Allele-based [Hardy-Weinburg Equilibrium](http://en.wikipedia.org/wiki/Hardy-Weinberg_principle)

Genotype-based Q test for Odds Ratio

**Quantitative Outcome**

[Difference in means test](http://en.wikipedia.org/wiki/Z_test)

Analysis of Variance ( [ANOVA](http://mathworld.wolfram.com/ANOVA.html) )

Quantitative transmission / disequilibrium tests (TDT)  
[(Allison 1997) (TDT Q5)](References.html#Allison1997)  
[(Rabinowitz 1997)](References.html#Rabinowitz1997)  
[(Monks et al 1998)](References.html#Monks1998)  

How to execute

java -jar Genie.jar PedGenie rgenfile\[.rgen\]

[.rgen Parameter File](rgenDescription.html)

Detailed description of .rgen XML file

[PedGenie Examples](PedGenieExamples.html)

All PedGenie Example Files

[Home](index.html)    [hapMC](hapMCDetail.html)    [hapConstructor](hapConstructor.html)
