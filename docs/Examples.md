# Genie Examples

* PedGenie Examples

[Single Locus Example](SingleLocusExample)

This example analyses each marker as a single locus.

[Single Locus GeneCountAllele Example](SingleGeneCountExample)

Similar to the Single Locus Example, but the allele frequency estimation method GeneCounter from GeneCountAlleles program is used.

[Composite Genotype Example](PedGenieCompGenotypeExample)

This example examines the joint inheritance of two or more markers, without considering phase of the data.

[Haplotype Example](PedGenieHaplotypeExample)

This example examines the joint inheritance of two or more markers, using phased data.

[CMH / Meta Analysis Examples](MetaExample)

Example of CMH / meta test statistics for multiple studies.

[QTDT Interface Example](QTDTExample)

This is a PedGenie - QTDT Interface example.

[Weighted Index Example](WeightedIndexExample)

This example is similar to the Single Locus Example, but all founders are assigned with unique alleles, and gene-drop to the rest of the pedigree for use in the [weighting algorithm](WeightingAlgorithm).

* hapMC Examples

[Composite Genotype Example](hapMCCompGenotypeExample)

This example examines the joint inheritance of two or more markers, with GCHap phased data method.

[Haplotype Example](hapMCHaplotypeExample)

This example examines the joint inheritance of two or more markers, with GCHap phased data method.

* hapConstructor Example

[Single Gene hapConstructor 1.0 Example](l404#hapc1.0/hapConstructor1.0_example)

This example examine the contruction of haplotype analyses of a two loci using unphased data.

[Gene-Gene hapConstructor 2.0 Example](hapConstructor2.0_example)

This example examine the contruction of haplotype analyses of a two loci using unphased data.

QTDT Example

[QTDT Example](QTDTExample)

This example demonstrates Genie and QTDT Interface with phased data.

[Home](index)  [PedGenie](PedGenieDetail)  [hapMC](hapMCDetail)  [hapConstructor](hapConstructor)
