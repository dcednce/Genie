# hapMC Haplotype report

```
********** hapMC - production version 2.0 Report **********

Created: Mon Oct 09 11:17:17 MDT 2006
Specification from: Haplotype.rgen
Data from: ./GenotypeData.dat
Runtime global parameters: 
  ccstat1=ChiSquared
  ccstat2=ChiSquaredTrend
  ccstat3=OddsRatios
  ccstat4=Quantitative
  covar1=1
  drop=HapMCDropSim
  locus 1=SNP1
  locus 2=SNP2
  nsims=2000
  report=report
  rseed=777
  top=HapMCTopSim
  top-sample=all

________________________________________________________________ 
*** Analysis 1 ***   Loci : ALL MARKERS   Model : Hap 2 vs Hap 1   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(0)       2(1)      TOTAL

Case       :          0         32         32
Control    :         32         16         48
--
TOTAL      :         32         48         80


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(0)       2(1)  Row Total

CoVar-0    :   2.055484   2.197273   4.252757
--
Col TOTAL  :   2.055484   2.197273   4.252757


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 35.555556
   Empirical p-value : 0.0005
Odds Ratios, 2-Tailed Test : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: reference cell has 0 value, **test** has not been performed.
   Empirical p-value : -
   Empirical Confidence Intervals : (-, -)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 2v1:0.833911
   Empirical p-value : 2v1:0.4115

________________________________________________________________ 
*** Analysis 2 ***   Loci : ALL MARKERS   Model : Hap 3 vs Hap 1   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(0)       2(1)      TOTAL

Case       :          0         16         16
Control    :         48          0         48
--
TOTAL      :         48         16         64


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(0)       2(1)  Row Total

CoVar-0    :   2.071087   1.975385   4.046472
--
Col TOTAL  :   2.071087   1.975385   4.046472


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 64
   Empirical p-value : 0
Odds Ratios, 2-Tailed Test : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: reference cell has 0 value, **test** has not been performed.
   Empirical p-value : -
   Empirical Confidence Intervals : (-, -)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 2v1:-0.498144
   Empirical p-value : 2v1:0.615

________________________________________________________________ 
*** Analysis 3 ***   Loci : ALL MARKERS   Model : Hap 4 vs Hap 1   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(0)       2(1)      TOTAL

Case       :          0         32         32
Control    :         16         48         64
--
TOTAL      :         16         80         96


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(0)       2(1)  Row Total

CoVar-0    :   2.103333   2.108219   4.211553
--
Col TOTAL  :   2.103333   2.108219   4.211553


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 9.6
   Empirical p-value : 0.002
Odds Ratios, 2-Tailed Test : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: reference cell has 0 value, **test** has not been performed.
   Empirical p-value : -
   Empirical Confidence Intervals : (-, -)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 2v1:0.024777
   Empirical p-value : 2v1:0.9785

________________________________________________________________ 
*** Analysis 4 ***   Loci : ALL MARKERS   Model : Hap 2 vs rest REV   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(1)       2(0)      TOTAL

Case       :         32         16         48
Control    :         16         48         64
--
TOTAL      :         48         64        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(1)       2(0)  Row Total

CoVar-0    :   2.197273   2.085439   4.282711
--
Col TOTAL  :   2.197273   2.085439   4.282711


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 19.444444
   Empirical p-value : 0.001
Odds Ratios, 2-Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:6
   Empirical p-value : 1v2:0
   Empirical Confidence Intervals : (2.726267, 13.204868)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:0.749856
   Empirical p-value : 1v2:0.4515

________________________________________________________________ 
*** Analysis 5 ***   Loci : ALL MARKERS   Model : Hap 3 vs rest REV   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(1)       2(0)      TOTAL

Case       :         16         32         48
Control    :          0         64         64
--
TOTAL      :         16         96        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(1)       2(0)  Row Total

CoVar-0    :   1.975385   2.157614   4.132998
--
Col TOTAL  :   1.975385   2.157614   4.132998


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 24.888889
   Empirical p-value : 0.001
Odds Ratios, 2-Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:-
   Empirical p-value : 1v2:0.002
   Empirical Confidence Intervals : (-, -)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:-1.002694
   Empirical p-value : 1v2:0.324

________________________________________________________________ 
*** Analysis 6 ***   Loci : ALL MARKERS   Model : Hap 4 vs rest   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(1)       2(0)      TOTAL

Case       :         32         16         48
Control    :         48         16         64
--
TOTAL      :         80         32        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(1)       2(0)  Row Total

CoVar-0    :   2.108219   2.201786   4.310005
--
Col TOTAL  :   2.108219   2.201786   4.310005


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 0.933333
   Empirical p-value : 0.35
Odds Ratios, 2-Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:0.666667
   Empirical p-value : 1v2:0.351
   Empirical Confidence Intervals : (0.304403, 1.460055)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:-0.57367
   Empirical p-value : 1v2:0.567

________________________________________________________________ 
*** Analysis 7 ***   Loci : ALL MARKERS   Model : Hap 3 Dom REV   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(1)       2(0)      TOTAL

Case       :         16         32         48
Control    :          0         64         64
--
TOTAL      :         16         96        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(1)       2(0)  Row Total

CoVar-0    :   1.975385   2.157614   4.132998
--
Col TOTAL  :   1.975385   2.157614   4.132998


Chi-squared : 2000 / 2000 total statistics calculated
   Observed statistic : 24.888889
   Empirical p-value : 0.001
Odds Ratios, 2-Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:-
   Empirical p-value : 1v2:0.002
   Empirical Confidence Intervals : (-, -)
Quantitative, Two Tailed Test : 2000 / 2000 total statistics calculated
   Observed statistic : 1v2:-1.002694
   Empirical p-value : 1v2:0.324

________________________________________________________________ 
*** Analysis 8 ***   Loci : ALL MARKERS   Model : Hap 3 Rec REV   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(1)       2(0)      TOTAL

Case       :          0         48         48
Control    :          0         64         64
--
TOTAL      :          0        112        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(1)       2(0)  Row Total

CoVar-0    :          0   2.134158   2.134158
--
Col TOTAL  :          0   2.134158   2.134158


Chi-squared : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: row or column sum equals zero, **test** has not been performed.
   Empirical p-value : -
Odds Ratios, 2-Tailed Test : 1063 / 2000 total statistics calculated
   Observed statistic : 1v2:-
   Empirical p-value : 1v2:-
   Empirical Confidence Intervals : (-, -)
   Warning : 95% CI is uninformative, likely due to sparse data or insufficient simulations
Quantitative, Two Tailed Test : 316 / 2000 total statistics calculated
   Observed statistic : 1v2:-
   Empirical p-value : 1v2:-

________________________________________________________________ 
*** Analysis 9 ***   Loci : ALL MARKERS   Model : Hap3 Additive R   Type : Genotype

*** Study : Testing

================= Contingency Table =================

Pheno Type  ---------------- Col(wt) ----------------
                   1(2)       2(1)       3(0)      TOTAL

Case       :          0         16         32         48
Control    :          0          0         64         64
--
TOTAL      :          0         16         96        112


================= Quantitative Table =================

CoVar Mean  ---------------- Col(wt) ----------------
                   1(2)       2(1)       3(0)  Row Total

CoVar-0    :          0   1.975385   2.157614   4.132998
--
Col TOTAL  :          0   1.975385   2.157614   4.132998


Chi-squared : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: row or column sum equals zero, **test** has not been performed.
   Empirical p-value : -
Chi-squared Trend : 0 / 2000 total statistics calculated
   Observed statistic : WARNING: row or column sum equals zero, **test** has not been performed.
   Empirical p-value : -
Odds Ratios, 2-Tailed Test : 1063 / 2000, 2000 / 2000 total statistics calculated
   Observed statistic : 1v3:- 2v3:-
   Empirical p-value : 1v3:- 2v3:0.002
   Empirical Confidence Intervals : (-, -) (-, -)
   Warning : 95% CI is uninformative, likely due to sparse data or insufficient simulations
Quantitative, Two Tailed Test : 316 / 2000, 2000 / 2000 total statistics calculated
   Observed statistic : 1v3:- 2v3:-1.002694
   Observed Overall Statistic : -
   Empirical p-value : 1v3:- 2v3:0.3245
   Empirical Overall p-value : -

Elapse Time : 0 h : 4 m : 46 s
```