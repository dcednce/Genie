# Simulation Program Detail

* AlleleFreqTopSim

The Mendelian gene drop, choosing AlleleFreqTopSim selects alleles to be dropped from the founders in the pedigree structure.

* HapFreqTopSim

For the Mendelian gene drop, selecting HapFreqTopSim drops haplotypes from the founders in the pedigrees.

* HapMCTopTogether

This program uses Alun Thomas's [GCHap](http://balance.med.utah.edu/wiki/index.php/Access_programs_by_name) to find maxiumn likelihood estimates of the haplotypes and their frequencies from genotyped CASE and CONTROL individuals. We uses these estimated haplotypes and their frequencies for the gene-drop. Then all individual's haplotype data will be phased before testing.

* HapMCTopSeparate

This program is similar to HapMCTopTogether, except we separate individuals into CASE and CONTROL and uses [GCHap](http://balance.med.utah.edu/wiki/index.php/Access_programs_by_name) to estimate each group's haplotypes and their frequencies. Finally, we phased the two groups of data separately.

* GeneCounterTopSim

The Mendelian gene drop, using Alun Thomas's [_GeneCountAlleles_](References.html#Thomas2006) program to calculate allele frequencies based on all the genotyped members and the population allele frequencies in the linkage loci data file.

* IndivWtTopSim

Assigns two unique alleles to all founders and uses a Mendelian gene drop to assign these to the rest of the pedigree for use in [_weighting algorithm_](WeightingAlgorithm).

* ConditionalGeneTopSim

This program uses Alun Thomas's[ConditionalGeneDrop](http://www-genepi.med.utah.edu/~alun/index) program to preform the gene drop

[Home](index)   [.rgen Parameter File](rgenDescription)
