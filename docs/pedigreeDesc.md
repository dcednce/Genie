# Pedigree File Description

The pedigree file represents the genealogy of individuals in the study. It contains one header line and the family and genotype information for study subjects.

*  KIND

Unique kindred/family ID number

*  IND

Each individual has a unique number within a family

*  DAD

ID number of the father of a particular individual

*  MOM

ID number of the mother of a particular individual

*  SEX

Sex ID number, code as 0 = unknown, 1 = male, 2 = female

*  PHENO

Phenotype of individual coded as 0 = unknown, 1 = control, 2 = case

*  LIAB

Liability class

*  Marker data

Marker data should be entered as two separate columns of numbers separated by a space or a tab

[Home](index.html)   [Pedigree Example File](PhasedPedigree.dat)
