# Meta / Cochran-Mantel-Haenszel Analysis Example

This example demonstrates Cochran-Mantel-Haenzsel chi-squared, CMH chi-squared trend and meta odds ratio testing of 3 studies and one marker. If you have problems opening a file, at the error page try using toolbar _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Files required to run this example are :

[MetaPedigree1.dat](MetaPedigree1.dat) - Linkage pre-makeped pedigree file with a header line  
[MetaPedigree2.dat](MetaPedigree2.dat) - Linkage pre-makeped pedigree file with a header line  
[MetaPedigree3.dat](MetaPedigree3.dat) - Linkage pre-makeped pedigree file with a header line  
[Meta.rgen](Meta.rgen) - .rgen parameter file

Output file of this analysis is :

[Meta.report](Meta.report) - full detail report  
[Meta.summary](Meta.summary) - ASCII summary report

To run this analysis:

_java -jar Genie.jar PedGenie Meta.rgen_

**[MetaPedigree1.dat](MetaPedigree1.dat)**  
This example [pedigree file](pedigreeDesc.html) is composed of nuclear families consisting of 200 affected sib-pairs plus parents, 200 discordant sib-pairs plus parents, and 200 independent controls.

**[MetaPedigree2.dat](MetaPedigree2.dat)**  
This example [pedigree file](pedigreeDesc.html) is composed of four-generation families consisting of 53 members per pedigree (sibships of size three in all generations, all sibs marry in generations 2 and 3) and 500 independent controls.

**[MetaPedigree3.dat](MetaPedigree3.dat)**  
This example [pedigree file](pedigreeDesc.html) is composed of 1,000 independent cases and 1,000 independent controls.

**[Meta.rgen](Meta.rgen)**  
The set-up of Meta.rgen is similar to the set-up of the [SingleLocus.rgen](SingleLocus.rgen). The major differences between the two files are, there are 3 genotypedata files for this example and it uses meta / CMH statistical analysis. It also output both summary and full report.

**[Meta.report](Meta.report)** output file

This Meta.report file is similar to the [SingleLocus.report](SingleLocus.report) output. The exception is when CMH statistical tests are requested in a meta analysis of multiple data files, the individual study's contingency tables and their test results specified by the user appear first, following the CMH / meta analysis statistics.

**[Meta.summary](Meta.summary)** output file

This space-delimited file is a summary of all statistics, confidence intervals for odds ratios, and p-values for each individual study and CMH / meta analysis that appear in the Meta.report file. This allows the user to import results into other programs, such as Microsoft Excel. Each row of the summary file corresponds to each requested analysis. Format: markers, model type, random seed, individual study statistics followed by their corresponding p-values and odds ratio confidence intervals, CMH / meta statistics and their corresponding p-values and meta odds ratio confidence intervals.

[Home](index.html)   [PedGenie](PedGenieDetail.html)
