# hapMC Example

**Analysis example**

The hapMC example allows a user to test simulated multi-marker data. _View_, _Source_ or click on _Page_, _View Source_ to view the file.

Files required :

[hapmc\_asp.dat](hapmc_asp.dat) - Linkage pre-makeped pedigree file with a header line  
[hapmc\_asp.rgen](hapmc_asp.rgen) - .rgen Parameter File

Output files :

[hapmc\_asp.report](hapmc_asp.report)

To run this analysis:

_java -jar hapmc.jar hapMC hapmc\_asp.rgen_

**GenotypeData.dat**  
The example [Genotype Data file](pedigreeDesc.html) is composed of 500 affected sib-pairs. Each individual has full genotype data for 10 markers.

****Output pedigree phase configurations to file****

HapMC example for writing pedigree configurations to file.

Files required :

[asp.example.dat](asp.example.dat) - Linkage pre-makeped pedigree file with a header line  
[asp.example.rgen](asp.example.rgen) - .rgen Parameter File

Output files :

[ped\_configs.log](ped_configs.log)

To run this analysis:

_java -jar hapmc.jar hapMC asp.example.rgen_

[Home](index.html)   [hapMC](hapMCDetail.html)
