# hapMC Examples

[Composite Genotype Example](hapMCCompGenotypeExample.html)

This example examines the joint inheritance of two or more markers, without considering phase of the data.

[Haplotype Example](hapMCHaplotypeExample.html)

This example examines the joint inheritance of two or more markers.

[Home](index.html)  [hapMC](hapMCDetail.html)
