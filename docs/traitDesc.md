# Trait File Description

If the statistic class name Quantitative is included in the analysis, a trait file is required. The trait file contains any trait or covariate information to be analyzed. Missing trait/covariate data should be coded as 0.0. This file should be listed in the .rgen parameter file with attribute value = quantfile and the Trait file name entered as the variable. The number of traits and/or covariates are listed in the .rgen parameter file under covar1, covar2, etc.

* KIND

Unique kindred / family ID number

* IND

Each individual has a unique number within a family

* Trait

One of more traits may be entered

* Covariate

One or more covariates may be entered

[Home](index.html)   [QTDT Interface](QTDTinterface.html)   [Trait Example File](Trait.dat)
