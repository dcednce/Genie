# hapMC Haploypte .rgen example

<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE ge:rgen SYSTEM "hapMC-rgen.dtd">

<ge:rgen rseed="777" nsims="2000" >

  <ge:locus id="1" marker="SNP1"/>  
  <ge:locus id="2" marker="SNP2" dist="0.00"/>  
 
  <ge:datafile studyname="Testing" genotypedata="/export/home/jathine/webpages/hapMC2/GenotypeData.dat" quantitative="/export/home/jathine/webpages/hapMC2/Trait.dat"/>
 
  <!-- optional modules -->
  <ge:param name="ccstat1">ChiSquared</ge:param>
  <ge:param name="ccstat2">ChiSquaredTrend</ge:param>
  <ge:param name="ccstat3">OddsRatios</ge:param>
  <ge:param name="ccstat4">Quantitative</ge:param>
  <ge:param name="covar1">1</ge:param>

  <!-- other optional parameters -->
  <ge:param name="top-sample">all</ge:param>  <!-- options: all, founder, GeneCountAlleles -->
  <!-- the top-sample for allele frequencies is ignored.  -->

<!-- Haplotype Test Hap (1-2) vs. (1-1)-->

 <!-- Test against most common haplotype -->
 <ge:cctable stats="1 3 4" model="Hap 2 vs Hap 1">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
        <ge:a>1/1</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>1/2</ge:a>
        <ge:a>1/1</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>1/2</ge:a>
        <ge:a>1/2</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>2/1</ge:a>
        <ge:a>1/1</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>2/1</ge:a>
        <ge:a>2/1</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>1/1</ge:a>
        <ge:a>(./2)|(2/.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(2/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(2/2)</ge:a>
      </ge:g>
     <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(1/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(2/2)</ge:a>
      </ge:g>
   </ge:col>
  </ge:cctable>


<!-- Haplotype Test Hap (2-2) vs. (1-1)-->

 <ge:cctable stats="1 3 4" model="Hap 3 vs Hap 1">
    <ge:col wt="0">
      <ge:g>
        <ge:a>1/1</ge:a>
        <ge:a>(1/.)|(./1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(1/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(1/1)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(1/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(2/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(2/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(2/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/2)</ge:a>
        <ge:a>(2/.)|(./2)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

<!-- Haplotype Test Hap (2-1) vs. (1-1)-->

 <ge:cctable stats="1 3 4" model="Hap 4 vs Hap 1">
    <ge:col wt="0">
      <ge:g>
        <ge:a>(1/1)</ge:a>
        <ge:a>(1/.)|(./1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(1/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(2/1)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
      <ge:g>
        <ge:a>1/2</ge:a>
        <ge:a>(1/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(1/2)</ge:a>
        <ge:a>(2/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(1/1)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/1)</ge:a>
        <ge:a>(1/2)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(2/2)</ge:a>
        <ge:a>(1/.)|(./1)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

 
<!-- Haplotype 2 (1-2) -->
<!-- Note:  weight changed listing 'wt="1"' first to take advantage of './.' in 'wt="0"' -->
<!-- which excludes all patterns previously counted. Output will show reverse order -->
<!-- (we listed a reminder message: REV); reference group is still lowest weight.  -->


 <ge:cctable  stats="1 3 4" model="Hap 2 vs rest REV">
    <ge:col wt="1">
      <ge:g>
        <ge:a>(1/.)</ge:a>
        <ge:a>(2/.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(./1)</ge:a>
        <ge:a>(./2)</ge:a>
      </ge:g>
     </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>(./.)</ge:a>
        <ge:a>(./.)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Haplotype 3 (2-2) -->

 <ge:cctable  stats="1 3 4" model="Hap 3 vs rest REV">
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/.)</ge:a>
        <ge:a>(2/.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(./2)</ge:a>
        <ge:a>(./2)</ge:a>
      </ge:g>
     </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>(./.)</ge:a>
        <ge:a>(./.)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

<!-- Haplotype 4 (2-1) -->
 <ge:cctable  stats="1 3 4" model="Hap 4 vs rest">
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/.)</ge:a>
        <ge:a>(1/.)</ge:a>
      </ge:g>
      <ge:g>
        <ge:a>(./2)</ge:a>
        <ge:a>(./1)</ge:a>
      </ge:g>
     </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>(./.)</ge:a>
        <ge:a>(./.)</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Hap 3 exploration -->

 <!-- Dominant Model for Haplotype 3 (2-2) -->
 <ge:cctable stats="1 3 4" model="Hap 3 Dom REV">
    <ge:col wt="1">
      <ge:g>
        <ge:a>(2/.)</ge:a>
        <ge:a>(2/.)</ge:a>
       </ge:g>
       <ge:g>
        <ge:a>(./2)</ge:a>
        <ge:a>(./2)</ge:a>
      </ge:g>
  </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>./.</ge:a>
        <ge:a>./.</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Recessive Model for Haplotype 3 (2-2)-->
 <ge:cctable stats="1 3 4" model="Hap 3 Rec REV">
    <ge:col wt="1">
      <ge:g>
        <ge:a>2/2</ge:a>
        <ge:a>2/2</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>./.</ge:a>
        <ge:a>./.</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>


<!-- Additive test, weighting Haplotype 3 (2-1)-->
<!-- Note: no 'stats' listed, thus all statistics will be run, including the TDT tests -->
<!-- There will be three columns in this analysis, a prerequisite for the TDT tests.  -->

 <ge:cctable model="Hap3 Additive R">
    <ge:col wt="2">
      <ge:g>
        <ge:a>2/2</ge:a>
        <ge:a>2/2</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="1">
       <ge:g>
        <ge:a>(2/.)</ge:a>
        <ge:a>(2/.)</ge:a>
      </ge:g>
       <ge:g>
        <ge:a>(./2)</ge:a>
        <ge:a>(./2)</ge:a>
      </ge:g>
    </ge:col>
    <ge:col wt="0">
      <ge:g>
        <ge:a>./.</ge:a>
        <ge:a>./.</ge:a>
      </ge:g>
    </ge:col>
  </ge:cctable>

</ge:rgen>
