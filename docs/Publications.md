# Genie Publications

Allen-Brady K, Farnham JM, Weiler J, Camp NJ.

A cautionary note on the appropriateness of using a linkage resource for an association study. BMC Genetics 2003, 4 (Suppl 1): S89.

Allen-Brady, K. Wong J., Camp N.J

PedGenie: A General Analysis Tool for Genetic Association Testing in Extended Pedigrees and Genealogies of Arbitrary Size. BMC Bioinformatics 2006, 7:209.

Curtin K, Wong J, Allen-Brady K, Camp NJ

Pedgenie 2:0: Meta Genetic Association Testing in Mixed Family and Case-control Designs.  
Abstract, International Genetic Epidemiology Society 15th Annual Meeting. November 2006, St. Pete Beach, FL.

[Home](index.html)
